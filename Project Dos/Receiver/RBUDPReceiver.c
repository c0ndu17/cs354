/**
 * @author George Phillips, 16512561
 *
 */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<pthread.h>
#include<limits.h>
#include<ncurses.h>

/**
 * Definitions
 */
#define true 1
#define false 0
#define UDP_MAX 582
/**
 *  Datatypes
 *
 */

typedef struct udp_msg_t {
    int serial_number;
    char *data;
    struct udp_msg_t *next;
} udp_msg;

/**
 * Function Declarations
 */
void *server_send (void *sock);
void *server_recv (void *sock);
int add_udp_msg (char *packet);
void msg_check (char **buffers, int *packets_in, int num_sent, int total_num_pieces);
void close_windows();
int file_receiver(int *socket_desc);

/**
 * Global Variables
 */

int alive[] = {false, false};
char **msg_buffer;
FILE *new_file;
struct sockaddr_in udp_sock_desc;
struct sockaddr_in udp_server;
int udp_server_desc;
char* home_ip;
udp_msg *msg_list;
WINDOW *input_scr;
WINDOW *msg_list_scr;
WINDOW *user_list_scr;



int main(int argc, char *argv[])
{
    int socket_comm, alive_count, i;
    struct sockaddr_in server;
    pthread_t thread[2];

    if (argc!=2) {
        printf("usage: ./RBUDPReceiver <server I.P.> <your I.P> %d\n", argc);
        return;
    }

    initscr();
    echo();
    //raw();

    /* ncurses WINDOW Initialisation */
    if ( LINES < 10 || COLS < 21 ) {
        printf("Screen too small\nExiting...");
        return EXIT_FAILURE;
    }
    msg_list_scr    = newwin(LINES-3, COLS-21, 0, 0);
    user_list_scr   = newwin(LINES-3, 20, 0, COLS-21);
    input_scr       = newwin(3, COLS, LINES-3, 0);

    if(((input_scr ==NULL))||((msg_list_scr == NULL)||((user_list_scr == NULL)))||((msg_buffer = (char **) malloc(sizeof(char *) * LINES-5))== NULL)){
        printf("Failed to initialize all the screens\nExiting.\n");
        return EXIT_FAILURE;
    }
    wborder(input_scr, 0, 0, 0, 0, 0, 0, 0, 0);
    wborder(msg_list_scr, 0, 0, 0, 0, 0, 0, 0, 0);
    wborder(user_list_scr, 0, 0, 0, 0, 0, 0, 0, 0);
    mvwaddstr(user_list_scr, 1, 1, "RBUDPReciever.");
    wrefresh(input_scr);
    wrefresh(msg_list_scr);
    wrefresh(user_list_scr);
    if ((socket_comm = socket(AF_INET, SOCK_STREAM, 0))<0) {
        close_windows();
        endwin();
        printf("failed to open a socket\n");
        return EXIT_FAILURE;
    }
    if ((udp_server_desc = socket( AF_INET, SOCK_DGRAM, 0 )) <0){
        close_windows();
        endwin();
        printf("failed to open a socket\n");
        return EXIT_FAILURE;
    }

    //TODO:CHANGE TO ARGS
    server.sin_addr.s_addr = inet_addr("127.0.0.1");//argv[1];
    server.sin_family = AF_INET;
    server.sin_port = htons(3000);

    //TODO:CHANGE TO ARGS
    /*
    udp_sock_desc.sin_addr.s_addr = inet_addr("127.0.0.1");//argv[1];
    udp_sock_desc.sin_family = AF_INET;
    udp_sock_desc.sin_port = htons(3001);
    */

    //TODO:CHANGE TO ARGS
    udp_server.sin_addr.s_addr = INADDR_ANY;
    udp_server.sin_family = AF_INET;
    udp_server.sin_port = htons(3001);

    if(bind(udp_server_desc, (struct sockaddr *)&udp_server, sizeof(udp_server)) < 0 ){
        close_windows();
        endwin();
        printf("Failed to bind to socket\n");
        return EXIT_FAILURE;
    }

    home_ip = "127.0.0.1";//argv[2];

    if (connect(socket_comm, (struct sockaddr *)&server, sizeof(server))<0) {
        close_windows();
        endwin();
        printf("Failed to connect to socket\n");
        return EXIT_FAILURE;
    }
    mvwaddstr(user_list_scr, 3, 1, "Connected.");
    wrefresh(user_list_scr);
    werase(user_list_scr);
    wborder(user_list_scr, 0, 0, 0, 0, 0, 0, 0, 0); //ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
    mvwaddstr(user_list_scr, 1, 1, "Welcome.");
    wrefresh(user_list_scr);

    alive[0]=true;
    if (pthread_create(&(thread[0]), NULL, server_send, (void *)&socket_comm) < 0) {
        close_windows();
        endwin();
        printf("could not spawn child_thread(send) for client\n");
        alive[0]=false;
        return -1;
    }
    alive[1]=true;
    if ((pthread_create(&(thread[1]), NULL, server_recv, (void *)&socket_comm) < 0) || alive[0]==false) {
        alive[1]=false;
        alive[0] = false;
        close_windows();
        endwin();
        printf("could not spawn child_thread(recv) for client\n");
        return -1;
    }
    while (alive_count != 0) {
        alive_count =0;
        for(i =0; i<2; i++){
            alive_count += alive[i];
        }
    }
    pthread_join(thread[0], NULL);
    pthread_join(thread[1], NULL);
    close(socket_comm);
    close_windows();
    endwin();
    printf("Goodbye!\n");
    return EXIT_SUCCESS;
}

void close_windows(){
    delwin(user_list_scr);
    delwin(msg_list_scr);
    delwin(input_scr);
}

void *server_send (void *socket_desc) {
    char message_to_server[BUFSIZ];
    int socket_comm = *(int *)socket_desc;
    werase(input_scr);
    sprintf(message_to_server, "%s", home_ip);
    if (send(socket_comm, message_to_server, BUFSIZ, 0) < 0) {
        return;
    }
    bzero(message_to_server, BUFSIZ);
    while (alive[0]) {
        wborder(input_scr, 0, 0, 0, 0, 0, 0, 0, 0); //ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
        mvwaddstr(input_scr, 1, 1, "Enter file location: ");
        wrefresh(input_scr);
        wgetnstr(input_scr, message_to_server, getmaxx(input_scr)-20);
        if ( strlen(message_to_server) != 1 && (char)message_to_server[0] != '\n') {
            if ( strlen(message_to_server) > PATH_MAX-1 ) {
                mvwaddstr(input_scr, 1, 1, "Message too large, Maximum size is 536 characters");
                wrefresh(input_scr);
            }
            else if ( send(socket_comm, message_to_server, BUFSIZ, 0) < 0 ) {
                mvwaddstr(input_scr, 1, 1, "Send failed.");
                wrefresh(input_scr);
                alive[0] = false;
            }
        }
        if (message_to_server != NULL) {
            if (strcasecmp(message_to_server, ":exit")==0)
                alive[0]=false;
        }
        bzero(message_to_server, BUFSIZ);
        werase(input_scr);
    }
    wclear(input_scr);
}

void *server_recv (void *socket_desc) {

    char *message_from_server = malloc(sizeof(char)*BUFSIZ);
    int socket_comm = *(int *)socket_desc, i =1;
    while (alive[1]) {
        if (recv(socket_comm, message_from_server, BUFSIZ, 0)<0) {
            printf("Failed to receive from server.");
        }
        if (message_from_server != NULL) {
            mvwaddstr(msg_list_scr, i, 1, message_from_server);
            i++;
            if(strcasecmp(message_from_server, "1")==0){
                file_receiver((int *)socket_desc);
                mvwaddstr(msg_list_scr, i, 1, message_from_server);
            } else if(strcasecmp(message_from_server, "0")==0 ){
                bzero(message_from_server, strlen(message_from_server));
                strcat(message_from_server, "Error finding file on server\n");
                mvwaddstr(msg_list_scr, i, 1, message_from_server);
            } else if (strcasecmp(message_from_server, ":exit") == 0) {
                alive[1];
            } else {
                mvwaddstr(msg_list_scr, i, 1, message_from_server);
                i++;
            }
        }
        wrefresh(msg_list_scr);
        bzero(message_from_server, strlen(message_from_server));
    }
    alive[1] = false;
    free(message_from_server);
}

int file_receiver (int *socket_desc) {
    char buffer[BUFSIZ];
    char *file_data;
    int32_t received;
    int num_msgs_sent, sock_size, i, incount =2;
    int total_recv = 0,  protocol, packet_in, num_pieces;
    int *sent_packets;
    long long file_size;
    ssize_t this_read;
    char **buffers;

    if (recv(*(socket_desc), buffer, BUFSIZ, 0) < 0) {
        return 0;
    }
    new_file = fopen(buffer, "w");
    if (new_file == NULL){
        printf("File could not be opened\n");//TODO:Change to mvwaddstr
        return 0;
    }
    if (recv(*(socket_desc), buffer, BUFSIZ, 0) < 0) {
        return 0;
    }
    file_size = atoi(buffer);
    if (file_size <= 0) {
        return 0;
    }
    if (recv(*(socket_desc), buffer, BUFSIZ, 0) < 0) {
        return 0;
    }
    protocol = atoi(buffer);
    mvwaddstr(msg_list_scr, (LINES/2)-1, 1, buffer);
    wrefresh(msg_list_scr);
    bzero(buffer, BUFSIZ);
    switch (protocol){
        case 0:
            while (total_recv < file_size) {
                if((this_read = recv(*(socket_desc), buffer, BUFSIZ, 0)) < 0){
                    printf("failed to recv\n");
                }
                fwrite(buffer, sizeof(char), this_read, new_file);
                total_recv += this_read;
            }
            break;
        default:
            sock_size = sizeof(udp_sock_desc);
            send(*(socket_desc), &received, 0, 0);
            buffers = (char **)malloc(sizeof(char *)*((file_size/UDP_MAX)+1));
            while (total_recv < file_size) {
                received = htonl(0);
                while(recv(*(socket_desc), &received, sizeof(received), MSG_DONTWAIT)<0){
                    if((this_read = recvfrom(udp_server_desc, buffer, UDP_MAX, 0, (struct sockaddr *)&udp_sock_desc, &sock_size)) <= 0){
                        bzero(buffer, UDP_MAX);
                        sprintf(buffer, "%d", total_recv);
                        mvwaddstr(msg_list_scr, (LINES/2)-3 , 1, buffer);
                        wrefresh(msg_list_scr);
                        bzero(buffer, UDP_MAX);
                    } else {
                       packet_in= add_udp_msg(buffer);
                       if(buffers[packet_in] == NULL){
                           buffers[packet_in] = strdup(buffer);
                           mvwaddstr(msg_list_scr, (LINES/2) + incount, 1, buffer);
                           incount++;
                           wrefresh(msg_list_scr);
                           total_recv += this_read;
                       }
                    }
                }
                /*
                fwrite(buffer, sizeof(char), strlen(buffer), new_file);
                mvwaddstr(msg_list_scr, (LINES/2) + incount, 1, buffer);
                wrefresh(msg_list_scr);
                bzero(buffer, UDP_MAX);
                */
                //Send number of messages
                num_msgs_sent = atoi(buffer);
                if((this_read = recv(*(socket_desc), sent_packets, (sizeof(int)*num_msgs_sent), 0)) < 0){
                    return 0;
                }
                msg_check(buffers, sent_packets, num_msgs_sent,(int)((file_size/UDP_MAX)+1));
                if (send(*(socket_desc), sent_packets, (sizeof(int)*num_msgs_sent), 0) < 0){
                    return 0;
                }

                received = htonl(0);
                send(*(socket_desc), (void *)&received, sizeof(received), 0);
                sprintf(buffer, " %d", total_recv);
                mvwaddstr(msg_list_scr, (LINES/2)-3 , 1, buffer);
                wrefresh(msg_list_scr);
                bzero(buffer, UDP_MAX);

            }
            for(i =0; i<(int)((file_size/UDP_MAX)+1); i++){
                fwrite(buffers[i], sizeof(char), strlen(buffers[i]), new_file);
            }
            /*
            received = htonl(1);
            send(*(socket_desc), (void *)&received, sizeof(received), 0);
            */
            /*
            while (total_recv < file_size) {
                while (recv(*(socket_desc), buffer, BUFSIZ, MSG_DONTWAIT) < 0){
                    if((this_read = recvfrom(udp_server_desc, buffer, UDP_MAX, udp_sock_desc, sizeof(udp_sock_desc))) < 0){
                        printf("failed to recv\n");
                    }
                    add_udp_msg(buffer);

                }
                //Send number of messages
                if((this_read = recv(*(socket_desc), buffer, BUFSIZ, 0)) < 0){
                    return 0;
                }
                num_msgs_sent = atoi(buffer);
                //send list of serialnumbers as a string, delimated by spaces
                if((this_read = recv(*(socket_desc), buffer, BUFSIZ, 0)) < 0){
                    return 0;
                }
                msg_check(buffer);
                //TODO:write
                fwrite(buffer, sizeof(char), this_read, new_file);
                total_recv += this_read;
            }
            */
    }
    mvwaddstr(user_list_scr, (LINES/2), 1, "Received");
    wrefresh(user_list_scr);
    fclose(new_file);
}

int add_udp_msg(char *packet){
    udp_msg *new_msg;
    char *buffer;
    int serial_number;
    int i;
    /*
    if (msg_list == NULL) {
        msg_list = (udp_msg *)malloc(sizeof(udp_msg));
    } else {
        new_msg = (udp_msg *)malloc(sizeof(udp_msg));
        new_msg->next = msg_list;
        msg_list = new_msg;
    }
    if(msg_list==NULL)
        return;
        */
    if(packet == NULL)
        return -1;
    buffer = strtok(packet, " ");
    for (i = 0; i < strlen(buffer); i++) {
        if (!isdigit(buffer[i])) {
            printf("serial number was not a digit");
            return;
        }
    }
    serial_number = atoi(buffer);
    i = strlen(buffer) + 1;
    memmove(packet, packet + i, strlen(packet) - i + 1);
    return serial_number;
}

void msg_check (char **buffers, int *packets_in, int num_sent, int total_num_pieces) {
    int i;
    for(i=0; i < num_sent;i++){
        if(buffers[packets_in[i]]!=NULL){
            packets_in[i] = -1;
            mvwaddstr(user_list_scr, (LINES/2)+3, 1,buffers[packets_in[i]] );
            wrefresh(user_list_scr);
        }
    }
}

