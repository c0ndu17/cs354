/**
 * @author George Phillips, 16512561
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/stat.h>
#include<sys/sendfile.h>
#include<sys/types.h>
#include<libgen.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>
#include<errno.h>
#include<limits.h>

/**
 *  Definitions
 */
typedef int bool;
#define true 1
#define false 0
#define TCP_MAX 536
#define UDP_MAX 576

/**
 *  DataTypes
 */
typedef struct ClientThread_t{
    bool alive;
    int *tcp_sock_desc;
    struct sockaddr_in *udp_sock_desc;
    pthread_t thread_id;
    char *path;
    FILE *src_file;
    struct ClientThread_t *next;
} ClientThreadType;

struct udp_args {
    int serial;
    char *message;
};

/*
 *  Function Declarations
 */
FILE* file_opener(char *file_loc);
void *client_thread_start(void *client_sock_desc);
int server_logic(int client_sock, int client_count);
ClientThreadType *add_user (int socket_desc);
void udp_send (ClientThreadType *local_ref);
void tcp_send (ClientThreadType *local_ref);
void send_msg (int *socket_desc, char *message);
void trim (char* str_to_trim);
void left_trim(char* str_to_trim);
void right_trim(char* str_to_trim);

/**
 *  Global variables
 */
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t user_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t recv_lock = PTHREAD_MUTEX_INITIALIZER;
ClientThreadType* client_list;
int my_udp_desc;
int blast_num;
struct sockaddr_in my_udp;
struct sockaddr_in tcp_server, tcp_client;


/**
 *  Functions
 */
int main(int argc , char *argv[])
{
    int tcp_server_desc, client_sock, client_desc, optval, client_count = 1, backlog =10;
    ClientThreadType *tmp_client;

    if(argc!=3){
        printf("<usage>: ./RBUDPSender <your i.p> <blast size>\n");
        return;
    }
    blast_num = atoi(argv[2]);
    printf("RBUDPSender.");

    printf("Server starting\n");
    printf("Stimulating Transistors\n");

    if ((tcp_server_desc =socket(AF_INET , SOCK_STREAM , 0)) < 0) {
        printf("Failed to start tcp_server daemon socket\n");
        return EXIT_FAILURE;
    }
    if ((my_udp_desc =socket(AF_INET , SOCK_DGRAM , 0)) < 0) {
        printf("Failed to start tcp_server daemon socket\n");
        return EXIT_FAILURE;
    }
    printf("Server sockets created\n" );
    optval = 1;
    setsockopt(tcp_server_desc, SOL_SOCKET, SO_REUSEADDR,
            (const void *)&optval , sizeof(int));
    setsockopt(my_udp_desc, SOL_SOCKET, SO_REUSEADDR,
            (const void *)&optval , sizeof(int));

    /*Prepare the sockaddr_in structure*/
    tcp_server.sin_family = AF_INET;
    tcp_server.sin_addr.s_addr = INADDR_ANY;
    tcp_server.sin_port = htons( 3000 );

    my_udp.sin_family = AF_INET;
    my_udp.sin_addr.s_addr = inet_addr(argv[1]);
    my_udp.sin_port = htons( 3001 );

    if( bind(tcp_server_desc,(struct sockaddr *)&tcp_server , sizeof(tcp_server)) < 0)
    {
        /* print the error message */
        printf("Failed to bind to tcp socket\n");
        return EXIT_FAILURE;
    }
    printf("Bound to socket.\n");
    client_desc = sizeof(struct sockaddr_in);

    /* Listen */
    listen(tcp_server_desc, backlog);

    printf("Waiting for incoming connections...\n");
    client_desc = sizeof(struct sockaddr_in);
    while ( (client_sock = accept(tcp_server_desc, (struct sockaddr *)&tcp_client, (socklen_t*)&client_desc))) {
        if(server_logic(client_sock, client_count++)<0){
            client_sock = -2;
            break;
        }
    }
    printf("Exited Accept\n");
    switch (client_sock) {
        case -1:
            printf("accept failed");
            return EXIT_FAILURE;
        case -2:
            printf("Server Error... exiting");
            return EXIT_FAILURE;
        default:
            break;
    }
    while(client_list!=NULL){
        tmp_client = client_list;
        client_list = client_list->next;
        free(tmp_client->tcp_sock_desc);
        free(tmp_client);
    }
    return EXIT_SUCCESS;
}

int server_logic(int client_sock, int client_count) {
    ClientThreadType *tmp_client = add_user(client_sock);
    if (pthread_create(&(tmp_client->thread_id), NULL, client_thread_start, (void *)tmp_client->tcp_sock_desc) < 0) {
        printf("could not spawn child_thread for client %d\n", client_count);
        return -1;
    }
    pthread_detach(tmp_client->thread_id);
    printf("client %d's thread spawned\n", client_count);
    return 0;
}

void *client_thread_start(void *client_sock_desc){
    int protocol, *local_desc = (int *)client_sock_desc;
    bool finished = false;

    int client_message_len, socket_comm = *(int *)client_sock_desc, ret_check, i, j;
    //TODO: username malloc size
    char file_info[BUFSIZ], client_message[BUFSIZ], *username=malloc(1024*sizeof(char)), *msg_helper;
    ClientThreadType *local_ref;
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while ( local_ref != NULL) {
        if (*(int *)(client_sock_desc) == *(local_ref->tcp_sock_desc))
            break;
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    if (local_ref == NULL) {
        printf("socket could not be found in list\n Exiting");
        return;
    }

    /* Get file */
    while ( (client_message_len = recv_msg(local_ref->tcp_sock_desc, client_message)) < 0);
    trim(client_message);
    local_ref->udp_sock_desc = (struct sockaddr_in *)malloc(sizeof(struct sockaddr_in));
    local_ref->udp_sock_desc->sin_addr.s_addr = inet_addr(client_message);
    local_ref->udp_sock_desc->sin_family = AF_INET;
    local_ref->udp_sock_desc->sin_port = htons( 3001 );

    protocol=-1;
    bzero(client_message, BUFSIZ);
    while (strcasecmp(":exit", client_message) != 0) {
        if(strlen(client_message) > 0) {
            if (strncasecmp(client_message, ":tcp ", 4) == 0) {
                printf("TCP\n");
                protocol = 0;
                memmove(client_message, client_message + 5, strlen(client_message) - 5 + 1);
            } else {
                printf("UDP\n");
                protocol = 1;
            }
            if (client_message_len > 0) {
                ret_check = get_file_ptr(local_ref, client_message);
            } else {
                ret_check = -3;
            }
            bzero(client_message, BUFSIZ);
            bzero(file_info, BUFSIZ);
            switch (ret_check){
                case 1:
                    strcat(file_info, "1");
                    break;
                case 0:
                    strcat(file_info, "0");
                    break;
                case -2:
                    strcat(file_info, "-2");
                    break;
                case -3:
                    strcat(file_info, "-3");
                    break;
                default:
                    strcat(file_info, "-1");
            }
            if (protocol != -1) {
                send_msg(local_ref->tcp_sock_desc, file_info);
            }
            if (ret_check == 1)
                switch (protocol){
                    case 0:
                        tcp_send(local_ref);
                        break;
                    default:
                        udp_send(local_ref);
                }
        }
        while((client_message_len = recv_msg(local_ref->tcp_sock_desc, client_message))<0);
    }
    send_msg(local_ref->tcp_sock_desc, ":exit");
    trim(client_message);
    memset(file_info, 0, BUFSIZ);
}

/**
 *  send message via the UDP protocol.
 */

void udp_send (ClientThreadType *local_ref)
{
    char buffer[BUFSIZ], tmp_buffer[2];
    struct stat file_stat;
    off_t file_size;
    bool chunk_sent;
    socklen_t len;
    int32_t tmp_conf;
    struct sockaddr_in received_dg;
    int status, fd, send_size, recv_size, i, j, msg_recv_conf, data_sent = 0;
    char **buffers;
    int num_buffers;
    int orig_blast_num;
    int *packet_numbers;

    pthread_mutex_lock(&write_lock);

    if (local_ref->src_file == NULL) {
        pthread_mutex_unlock(&write_lock);
        return;
    }
    fd = fileno(local_ref->src_file);
    if (fd < 0) {
        pthread_mutex_unlock(&write_lock);
        return;
    }
    status = fstat(fd, &file_stat);
    if(status < 0){
        fprintf(stderr, "An error retrieving the file stats occurred\n");
        pthread_mutex_unlock(&write_lock);
        return;
    }
    file_size = file_stat.st_size;
    if (file_size <0) {
        fprintf(stderr, "An error retrieving the file size occurred\n");
        pthread_mutex_unlock(&write_lock);
        return;
    }
    printf("Sending file of size %d\n", (int) file_size);
    if(local_ref->path==NULL){
        fprintf(stderr, "File path not saved\n");
        pthread_mutex_unlock(&write_lock);
        return;
    }

    /**
     *  Send filename.
     */
    sprintf(buffer, "%s", basename(local_ref->path));
    printf("FILENAME\t%s\n",buffer);
    write(*(local_ref->tcp_sock_desc), buffer, sizeof(buffer));

    /**
     *  Send file size.
     */
    sprintf(buffer, "%d", (int)file_size);
    write(*(local_ref->tcp_sock_desc), buffer, sizeof(buffer));

    /**
     *  Send Protocol to use.
     */
    sprintf(buffer, "%d", 1);
    write(*(local_ref->tcp_sock_desc), buffer, sizeof(buffer));
    bzero(buffer, BUFSIZ);
    printf("Start..........\n");
    i=0;

    /**
     *  Client-side switch
     */

     /*
     *  Create Buffers for reading of file
     */
    buffers = (char **)malloc(sizeof(char *) * ((file_size/UDP_MAX)+1));

    /*
     *  Read file into buffers;
     */
    while (!feof(local_ref->src_file)){
        buffers[i] = (char *)malloc(sizeof(char)*UDP_MAX);
        fread(buffer, UDP_MAX-6 , 1, local_ref->src_file);
        sprintf(buffers[i],"%d ", i);
        strncat(buffers[i], buffer, strlen(buffer));
        i++;
    }
    num_buffers = i;
    //TODO:CHECK!!
    i--;
    if(blast_num == 0){
        pthread_mutex_unlock(&write_lock);
        return;
    }
    msg_recv_conf =1;
    if(num_buffers<blast_num)
        blast_num = num_buffers;
    if (local_ref->udp_sock_desc != NULL) {
        printf("%d \n", blast_num);
        printf("To Infinity.......... \n");
        fflush(stdout);
        i=0;
        while ( i < (int)((file_size/UDP_MAX)+1)) {
                packet_numbers = (int *)malloc(sizeof(int)*blast_num);
                j=i;
                while(j<blast_num){
                    packet_numbers[j] = j;
                    printf("%d\n", packet_numbers[j]);
                    j++;
                }
                fflush(stdout);
                chunk_sent = false;
                while (!chunk_sent) {
                    i = blast_num-i;
                    while (i < blast_num){
                        len = sizeof(*(local_ref->udp_sock_desc));
                        fflush(stdout);
                        if ((send_size = sendto(my_udp_desc, buffers[i], strlen(buffers[i]), 0, (struct sockaddr *)local_ref->udp_sock_desc, len))<0){
                            printf("failed to send datagram");
                            pthread_mutex_unlock(&write_lock);
                            return;
                        }
                        i++;
                    }
                    tmp_conf = htonl(orig_blast_num);
                    if(send(*(local_ref->tcp_sock_desc), (void *)&tmp_conf, sizeof(tmp_conf), 0)<0){
                        pthread_mutex_unlock(&write_lock);
                        return;
                    }
                    if (send(*(local_ref->tcp_sock_desc), packet_numbers, (sizeof(int)*blast_num), 0)< 0){
                        pthread_mutex_unlock(&write_lock);
                        return;
                    }
                    if (recv(*(local_ref->tcp_sock_desc), packet_numbers, (sizeof(int)*blast_num), 0)<0){
                        pthread_mutex_unlock(&write_lock);
                        return;
                    }
                    chunk_sent = true;
                    for(j = 0; j<blast_num; j++){
                        printf("%d ", packet_numbers[j]);
                        if (packet_numbers[j] != -1){
                            chunk_sent = false;
                        }

                    }
                }
                blast_num += blast_num;

                /*
                recv_size = recv(*(local_ref->tcp_sock_desc), &tmp_conf, sizeof(tmp_conf), 0);
                if (ntohl(tmp_conf)) {
                    msg_recv_conf = 0;
                    printf("msg_recv_conf %d\n", msg_recv_conf);
                    fflush(stdout);
                }*/
        }
        printf("File Sent\n");
    } else{
        printf("udp_sock_desc is NULL");

    }
    pthread_mutex_unlock(&write_lock);
    fclose(local_ref->src_file);
    local_ref->src_file == NULL;
    free(local_ref->path);
}
/**
 *  send message via the TCP protocol.
 */
void tcp_send (ClientThreadType *local_ref) {
    char buffer[BUFSIZ];
    struct stat file_stat;
    off_t file_size, curr_offset = 0;
    int status, fd, send_size, data_sent = 0;
    pthread_mutex_lock(&write_lock);
    printf("Sending over TCP\n");

    if (local_ref->src_file == NULL) {
        pthread_mutex_unlock(&write_lock);
        return;
    }
    fd = fileno(local_ref->src_file);
    if (fd < 0) {
        pthread_mutex_unlock(&write_lock);
        return;
    }
    status = fstat(fd, &file_stat);
    if(status < 0){
        fprintf(stderr, "An error retrieving the file stats occurred\n");
        pthread_mutex_unlock(&write_lock);
        return;
    }
    file_size = file_stat.st_size;
    if (file_size <0) {
        fprintf(stderr, "An error retrieving the file size occurred\n");
        pthread_mutex_unlock(&write_lock);
        return;
    }
    printf("Sending file of size %d\n", (int) file_size);
    if(local_ref->path==NULL){
        fprintf(stderr, "File path not saved\n");
        pthread_mutex_unlock(&write_lock);
        return;
    }

    /**
     *  Send Filename.
     */
    sprintf(buffer, "%s", basename(local_ref->path));
    printf("FILENAME\t%s\n", buffer);
    write(*(local_ref->tcp_sock_desc), buffer, sizeof(buffer));

    /**
     *  Send filesize
     */
    sprintf(buffer, "%d", (int)file_size);
    write(*(local_ref->tcp_sock_desc), buffer, sizeof(buffer));

    /**
     *  Send Protocol to use
     */
    sprintf(buffer, "%d", 0);
    write(*(local_ref->tcp_sock_desc), buffer, sizeof(buffer));

    while (((send_size = sendfile(*(local_ref->tcp_sock_desc), fd, &curr_offset, BUFSIZ)) > 0)&&(data_sent < file_size))
    {
        data_sent += send_size;
        printf("sent %d of %d\nBUFSIZE\t%d\n", data_sent, (int)file_size, BUFSIZ);
    }
    if(send_size == -1){
        printf("Sending error\n");
    }
    else printf("sent %d of %d\n", data_sent, (int)file_size);

    pthread_mutex_unlock(&write_lock);
    printf("File Sent\n");
    fclose(local_ref->src_file);
    local_ref->src_file == NULL;
    free(local_ref->path);
}

/**
 *
 */
void send_msg (int *socket_desc, char *message) {
    pthread_mutex_lock(&write_lock);
    write(*socket_desc, message, BUFSIZ);
    pthread_mutex_unlock(&write_lock);
}

/**
 *  Non-Blocking recv from the client
 */
int recv_msg(int *socket_desc, char *message){
    int ret_val;
    pthread_mutex_lock(&recv_lock);
    ret_val =recv(*socket_desc, message, PATH_MAX, MSG_DONTWAIT);
    pthread_mutex_unlock(&recv_lock);
    return ret_val;
}

/*
 *  add user to list of open transfers
 */

ClientThreadType *add_user (int socket_desc) {
    ClientThreadType *tmp_sock_thread;
    int *ret_val;
    tmp_sock_thread = (ClientThreadType *) malloc(sizeof(ClientThreadType));
    tmp_sock_thread->tcp_sock_desc = (int *)malloc(sizeof(int));
    *(tmp_sock_thread->tcp_sock_desc) = socket_desc;
    tmp_sock_thread->alive = true;
    pthread_mutex_lock(&user_lock);
    if (client_list==NULL) {
        client_list = tmp_sock_thread;
    } else {
        tmp_sock_thread->next = client_list;
        client_list = tmp_sock_thread;
    }
    pthread_mutex_unlock(&user_lock);
    return tmp_sock_thread;
}

/* checks if the file exits */
int get_file_ptr (ClientThreadType *local_ref, char *path) {
    ClientThreadType *looper;
    pthread_mutex_lock(&user_lock);
    pthread_mutex_lock(&write_lock);
    looper = client_list;
    if (local_ref == NULL) {
        pthread_mutex_unlock(&user_lock);
        pthread_mutex_unlock(&write_lock);
        return -2;
    }
    while (looper != NULL) {
        if (looper->path != NULL) {
            if ( strcasecmp(path, looper->path) == 0 ) {
                pthread_mutex_unlock(&user_lock);
                pthread_mutex_unlock(&write_lock);
                return -1;
            }
        }
        looper = looper->next;
    }
    local_ref->src_file = fopen(path, "r");
    pthread_mutex_unlock(&user_lock);
    pthread_mutex_unlock(&write_lock);
    if (local_ref->src_file == NULL){
        printf("Error opening file %s\n", path);
        return 0;
    } else {
        printf("transferring %s\n", path);
        local_ref->path = strdup(path);
        return 1;
    }
}
/* Contains functions for deleting whitespace from a */
void trim (char* str_to_trim) {
    left_trim(str_to_trim);
    right_trim(str_to_trim);
}

/* Trims trailing white space on the left side of a string */
void left_trim(char* str_to_trim){
    int n = 0;
    while (str_to_trim[n] != '\0' && isspace((unsigned char)str_to_trim[n])) {
        n++;
    }
    memmove(str_to_trim, str_to_trim + n, strlen(str_to_trim) - n + 1);
}

/* Trims trailing white space on the right side of a string */
void right_trim(char* str_to_trim){
    int n = strlen(str_to_trim);
    while (n > 0 && isspace(str_to_trim[n-1])) {
        n--;
    }
    str_to_trim[n] = '\0';
}

