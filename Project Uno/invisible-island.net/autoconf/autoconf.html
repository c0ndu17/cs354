<!--
  $Id: autoconf.html,v 1.41 2014/02/16 11:53:08 tom Exp $
  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for Linux (vers 25 March 2009), see www.w3.org">

  <title>AUTOCONF &ndash; Automatic Configuration</title>
  <link rev="MADE" href="mailto:dickey@invisible-island.net">
  <meta http-equiv="Content-Type" content=
  "text/html; charset=us-ascii">
  <link rel="SHORTCUT ICON" href="/img/icons/vile.ico" type=
  "image/x-icon">
  <link rel="stylesheet" href="/css/simplestyle.css" type=
  "text/css">
  <style type="text/css">
@import "/css/simplenavXX.css" all;
  </style>
</head>

<body>
  <hr>

  <p><a href="/">http://invisible-island.net/</a><br>
  Copyright &copy; 1997-2013,2014 by Thomas E. Dickey</p>
  <hr>

  <div class="nav">
    <ul>
      <li class="nav-top"><a href=
      "/autoconf/autoconf.html">(top)</a></li>

      <li><a href="#synopsis">Synopsis</a></li>

      <li><a href="#history">History</a></li>

      <li><a href="#patches">Patches</a></li>

      <li><a href="#changes">Changes</a></li>

      <li><a href="#download">Download</a></li>
    </ul>
  </div>

  <h2 id="synopsis-toc"><a name="synopsis" id=
  "synopsis">Synopsis</a></h2>

  <p><strong>autoconf</strong> is a package for creating Bourne
  shell scripts to configure source code packages using templates
  and an `m4' macro package.</p>

  <h2 id="history-toc"><a name="history" id=
  "history">History</a></h2>

  <p><strong>Autoconf</strong> was written chiefly by David
  Mackenzie, with input from many people, starting in 1991. When
  this page was started in 1997, the most recent releases (2.12 and
  2.10) were used in hundreds of programs. Like the Perl-based
  <strong>dist</strong>, <strong>autoconf</strong> is essentially a
  library of useful tests that a developer can put together to ask
  questions about a system to determine which features it has. Both
  are customizable, but <strong>dist</strong> is not as widely
  used. <strong>Autoconf</strong> is simpler, easier to learn.</p>

  <p>I have been using autoconf since June 1994, when Kevin
  Buettner started writing a configure script for <a href=
  "/vile/vile.html">vile</a>. I started a script for my directory
  editor <a href="/ded/ded.html">ded</a>. at that time, as well.
  Both of these scripts differ philosophically from autoconf's
  design. The designer of autoconf decided that there should be two
  ways to generate C-style definitions:</p>

  <ul>
    <li>as -D options in a makefile</li>

    <li>as substitions for #undef in a template header file (e.g.,
    config.h).</li>
  </ul>

  <p><strong>Autoconf</strong> is not design-neutral; sometimes
  this makes it awkward. Kevin chose a third alternative for vile,
  which we have kept because the first two options are unpalatable
  (the first because of tool limitations, and the second because it
  imposes unnecessary work on the maintainers, and maintaining the
  template is error-prone):</p>

  <ul>
    <li>generate the header file directly from the definitions that
    are found during the configure process.</li>
  </ul>

  <p>Since then, I've written simple configure scripts for several
  programs, as well as more complex scripts for a few (<a href=
  "/ncurses/ncurses.html">ncurses</a>, <a href=
  "/tin/tin.html">tin</a>, <a href="/lynx/lynx.html">lynx</a>,
  <a href="/xterm/xterm.html">xterm</a>).</p>

  <p>Though I have been writing macros for autoconf since 1994
  (prompted by Kevin Buettner's work on vile), it was not until
  August 1997 that I started maintaining the macros as a separate
  source archive. Until that point, I would simply cut/paste macros
  from a "good" copy into the program that I was working on. In
  1997, I wrote a pair of utilities (<code>acsplit</code> and
  <code>acmerge</code>) which I use to maintain the macros in their
  present form, resynchronizing them as needed as I work on each
  program. I refer to the common archive as
  "<em>my-autoconf</em>".</p>

  <p>At one point (early in 1998) I was discussing with Richard
  Stallman the possibility of becoming autoconf maintainer, There
  is an assignment on file which "Assigns past and future changes",
  but because the paperwork is incomplete (which the file does not
  note), that assignment has no effect. In particular, none of my
  autoconf-related patches are copyrighted by the Free Software
  Foundation.</p>

  <h2 id="patches-toc"><a name="patches" id=
  "patches">Patches</a></h2>

  <p>I find these patches useful:</p>

  <ul>
    <li>
      <a href=
      "ftp://invisible-island.net/autoconf/autoconf-2.12-971222.patch.gz">
      971222</a> simplify the way we generate the config.h file.
      This patch adds a parameter to AC_OUTPUT that tells it to use
      the definitions that it has built up to directly substitute
      into the template header, which can then be as simple as the
      single line
      <pre>
        @DEFS@
</pre>

      <p>The new parameter ("<em>sort</em>" or "<em>cat</em>")
      tells autoconf how to process the generated definitions. I
      prefer to sort the definitions since it eases comparison of
      headers for different platforms. Using "<em>cat</em>", will
      generate the definitions in the order that the configure
      script defines them.</p>
    </li>

    <li><a href=
    "ftp://invisible-island.net/autoconf/autoconf-2.12-971222-emx.patch.gz">
    971222-emx</a> a patch from <a href=
    "https://web.archive.org/web/20050212151723/http://www.arrakis.es/~worm/">
    Juan Jose Garcia Ripoll</a> &lt;worm@arrakis.es&gt;, originally
    for autoconf 2.10, for an OS/2 EMX-specific version of
    autoconf.</li>

    <li>
      <a href=
      "ftp://invisible-island.net/autoconf/autoconf-2.12-971230.patch.gz">
      971230</a> my own patch, for working around machines with
      limited environment space. AC_DIVERT_HELP splits long help
      messages into smaller pieces, which are emitted as a series
      of here-documents rather than attempt to keep their text in
      an environment variable. I use this macro to replace logic in
      autoconf's AC_ARG_ENABLE and AC_ARG_WITH, so it works without
      requiring you to change your configure.in or aclocal.m4
      files.

      <p>This introduces a minor incompatibility: generating the
      here-document directly causes escaped quotes in the help-text
      to be displayed with the spurious backslashes. Once you have
      converted to this macro, you will probably want to cleanup
      the help message text.</p>
    </li>

    <li>
      <a href=
      "ftp://invisible-island.net/autoconf/autoconf-2.13-20030927.patch.gz">
      autoconf 2.13 20030927</a> This is the most recent version of
      the autoconf 2.13 patch.

      <p>There is a more recent fully-patched tarball in the ftp
      area, which includes packaging scripts and minor updates.
      Since autoconf 2.13 does not support cross-compiling, most of
      my development uses the 2.52 versions.</p>
    </li>

    <li><a href=
    "ftp://invisible-island.net/autoconf/autoconf-2.13-20020210-emx.patch.gz">
    autoconf 2.13 20020210 EMX</a> This is the most recent version
    of the autoconf 2.13 patch for EMX.</li>

    <li>
      <a href=
      "ftp://invisible-island.net/autoconf/autoconf-2.52-20030208.patch.gz">
      autoconf 2.52 20030208</a> This is a resync of the patch (not
      EMX) against autoconf 2.52 (lightly tested: autoconf 2.5x
      introduces a number of incompatibilities with autoconf 2.13).
      The patch also includes some fixes for minor bugs which were
      reported/ignored on the autoconf mailing list. Finally, it
      repairs some of the intentional incompatibilites.

      <p>Applying a patch to autoconf is problematic, due to the
      minefield laid by its maintainers in the form of spurious
      build-dependencies upon automake and libtool. So I work with
      a cleaned-up source tree.</p>

      <p><a href="/autoconf/autoconf-252/ChangeLog">Here</a> is a
      list of changes for the source tree that I work with.</p>

      <p>A few developers still use the patched 2.13 version.
      <a href="/autoconf/autoconf-213/ChangeLog">Here</a> is a list
      of changes for the 2.13 tree.</p>
    </li>

    <li><a href=
    "ftp://invisible-island.net/autoconf/autoconf-2.57-20030810.patch.gz">
    autoconf 2.57 20030810</a> Even more lightly tested -- having
    noticed some new autoconf defects as I resync'd this patch, but
    unrelated to the patch.</li>
  </ul>

  <p>Occasionally someone asks why I use autoconf 2.13 (actually
  the question should be why I maintain configure scripts which are
  compatible with 2.13). The autoconf group, rather than focusing
  on making a reliable product, is using ongoing development as a
  basis for experimentation. With some care, it is possible to
  construct configure.in files which are compatible between 2.13
  and 2.5x. However, the standard response to reports of
  incompatibilities between the two is to blame 2.13 for "bugs"
  (ostensibly for quoting issues, but in fact on any issue without
  further analysis). Here is a recap of more recent autoconf
  versions and release dates:</p>

  <ul>
    <li>2.59 (2003-11-06) About nine months overdue (to repair
    2.57). Design flaws such as its interdependence with automake
    are still not addressed.</li>

    <li>2.58 (2003-11-04) Administrative blunder.</li>

    <li>2.57 (2002-12-03) Back up to beta quality! Within a few
    weeks a handful of serious bugs were reported, making it less
    useful than 2.52.</li>

    <li>2.56 (2002-11-15) Fixes an administrative blunder.</li>

    <li>2.55 Dead on arrival.</li>

    <li>2.54 (2002-09-13) Do not use: it was not tested with other
    "make" programs than GNU make.</li>

    <li>2.53 (2002-03-18) Too many initial bug reports to bother
    with. Features incomplete support for OS/2 EMX.</li>

    <li>2.52 (2001-07-18) Moderately stable (beta quality).</li>

    <li>2.51 (2001-07-17) Dead on arrival. The changelog around
    2.51 does not mention that the associated
    config.guess/config.sub had a syntax error (it wrote extra text
    to the output). This was silently fixed in the 2.52
    version.</li>

    <li>2.50 (2001-05-21) Do not use (alpha quality). It is
    compatible with 2.13 only for trivial configure.in's because
    the conventions for cached information were changed.</li>
  </ul>

  <p>Generally the changelog of autoconf does not reflect the
  issues (serious defects and incompatibilities) reported on their
  mailing list.</p>

  <p>As an example, consider "autoreconf". The reason for this tool
  is to repair the interfaces between autoconf and automake (which
  break on a regular basis) by regenerating the output from
  autoconf. Fortunately, I do not use automake (a monolithic perl
  script much larger than the <code>make</code> program). So I have
  no use for <code>autoreconf</code>.</p>

  <h2 id="changes-toc"><a name="changes" id=
  "changes">Changes</a></h2>

  <ul>
    <li><a href="/autoconf/my-autoconf/CHANGES">The
    "<em>my-autoconf</em>" macros</a></li>

    <li><a href="/autoconf/autoconf-213/ChangeLog">autoconf 2.13
    (patched)</a></li>

    <li><a href="/autoconf/autoconf-252/ChangeLog">autoconf 2.52
    (patched)</a></li>
  </ul>

  <h2 id="download-toc"><a name="download" id=
  "download">Download</a></h2>

  <ul>
    <li>The "<em>my-autoconf</em>" macros:

      <ul>
        <li><a href=
        "ftp://invisible-island.net/autoconf/my-autoconf.tar.gz">The
        macros (ftp)</a></li>

        <li><a href="/datafiles/release/my-autoconf.tar.gz">The
        macros (http)</a></li>
      </ul>
    </li>

    <li>Autoconf 2.52:

      <ul>
        <li><a href=
        "ftp://invisible-island.net/autoconf/autoconf.tar.gz">Current
        patched 2.52 (ftp)</a></li>

        <li><a href="/datafiles/release/autoconf.tar.gz">Current
        patched 2.52 (http)</a></li>
      </ul>
    </li>
  </ul>
</body>
</html>
