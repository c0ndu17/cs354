<!--
  $Id: diffstat.html,v 1.73 2014/05/05 19:47:39 tom Exp $
  -->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for Linux (vers 25 March 2009), see www.w3.org">

  <title>DIFFSTAT &ndash; make histogram from diff-output</title>
  <link rev="MADE" href="mailto:dickey@invisible-island.net">
  <meta http-equiv="Content-Type" content=
  "text/html; charset=us-ascii">
  <meta name="keywords" content="diff, patch, histogram">
  <meta name="description" content=
  "Thomas Dickey wrote and continues to develop diffstat, a tool which aids developers in viewing the impact of software patch. This page gives some background and pointers to diffstat resources.">
  <link rel="SHORTCUT ICON" href="/img/icons/vile.ico" type=
  "image/x-icon">
  <link rel="stylesheet" href="/css/simplestyle.css" type=
  "text/css">
  <style type="text/css">
@import "/css/simplenavXX.css" all;
  </style>
</head>

<body>
  <hr>

  <p><a href="/">http://invisible-island.net/</a><br>
  Copyright &copy; 1996-2013,2014 by Thomas E. Dickey</p>
  <hr>

  <div class="nav">
    <ul>
      <li class="nav-top"><a href=
      "/diffstat/diffstat.html">(top)</a></li>

      <li><a href="#synopsis">Synopsis</a></li>

      <li><a href="#history">History</a></li>

      <li><a href="#impact">Impact</a></li>

      <li>
        <a href="#nuisances">Nuisances</a>

        <ul>
          <li><a href="#bad_bashing">Bash-dependency</a></li>

          <li><a href="#bad_license">Licensing</a></li>
        </ul>
      </li>

      <li><a href="#documentation">Documentation</a></li>

      <li><a href="#download">Download</a></li>

      <li>
        <a href="#related">Related Links</a>

        <ul>
          <li><a href="#packages">Packages</a></li>

          <li><a href="#cm_tools">CM Tools</a></li>

          <li><a href="#other_uses">Other Uses</a></li>

          <li><a href="#derived">Other Implementations</a></li>
        </ul>
      </li>
    </ul>
  </div>

  <h1 class="no-header">DIFFSTAT &ndash; make histogram from
  diff-output</h1>

  <h2 id="synopsis-h2"><a name="synopsis" id=
  "synopsis">Synopsis</a></h2>

  <p><strong>diffstat</strong> reads the output of
  <strong>diff</strong> and displays a histogram of the insertions,
  deletions, and modifications per-file. It is useful for reviewing
  large, complex patch files.</p>

  <h2 id="history-h2"><a name="history" id=
  "history">History</a></h2>

  <p>I originally wrote this in <a href=
  "/personal/oldprogs.html#y1992">1992</a>, along with an
  associated utility <strong>rcshist</strong>, to trace the change
  history of collections of files. Since then, I've found it most
  useful for summarizing source patches.</p>

  <p>See the changelog for details:</p>

  <ul>
    <li><a href="/diffstat/CHANGES">plain text</a></li>

    <li><a href="/diffstat/CHANGES-index.html">release and patches
    by date</a></li>

    <li><a href="/diffstat/CHANGES.html">changes with
    links...</a></li>
  </ul>

  <h2 id="impact-h2"><a name="impact" id="impact">Impact</a></h2>

  <p>Initially, I used <strong>diff</strong> and
  <strong>diffstat</strong> in a script named
  <code>diff-patch</code>. In 1994, I started using <a href=
  "http://search.cpan.org/dist/makepatch/">makepatch</a> which gave
  more consistent results.</p>

  <p>It was not until early 1996 that there was much attention by
  others to the tool. At that point, developers on both XFree86 and
  ncurses mailing lists started using it.</p>

  <p>One of those developers (Tony Nugent) pointed it out to Linus
  Torvalds in July 1996, on <em>linux.dev.kernel</em>. Much later
  (in 2002), it was documented as part of the process for
  submitting Linux kernel patches for BitKeeper (BK) in Linux
  2.4.20. Linus <a href=
  "http://lkml.indiana.edu/hypermail/linux/kernel/0210.0/1811.html">
  commented</a> on the process:</p>

  <blockquote>
    <p>Ok, pulled. But _please_ do this the regular way next time.
    There's even a script to help you do it in
    linux/Documentation/BK-usage/bk-mak-sum, which does it all for
    you for BK patches.</p>

    <p>(many people end up doing their own thing, you don't have to
    use that particular script, of course. But the important thing
    I want is that the _email_ should contain enough information to
    make a good first pass judgement on what the patch does, and in
    particular it is important for me to see what a "bk pull" will
    actually change.)</p>

    <p>That's why the "diffstat" is important to me if I do a BK
    pull &ndash; and why I want to see the patches as plaintext if
    I apply stuff to generic files..</p>
  </blockquote>

  <p>Later, in 2005 Linus wrote <strong>git</strong>, which has the
  ability to <a href=
  "http://www.kernel.org/pub/software/scm/git/docs/git-diff.html">generate
  a diffstat</a>. There are some enhancements (git is able to track
  <a href=
  "http://lkml.indiana.edu/hypermail/linux/kernel/0609.3/0241.html">
  moves and renames</a> of files).</p>

  <h2 id="nuisances-h2"><a name="nuisances" id=
  "nuisances">Nuisances</a></h2>

  <h3><a name="bad_bashing" id=
  "bad_bashing">Bash-dependency</a></h3>

  <p>Ubuntu <a href=
  "https://bugs.launchpad.net/ubuntu/+source/diffstat/+bug/209537">#209537</a>).
  introduced a misfeature. Briefly, it checks if a
  <code>COLUMNS</code> environment variable is set, and uses
  whatever value <code>atoi</code> decodes to override the default
  of 80 columns for the report width. My advice was overruled (the
  bug report offers a disingenuous reason&mdash;see <a href=
  "http://bugs.debian.org/cgi-bin/pkgreport.cgi?ordering=normal;archive=0;src=mawk;repeatmerge">
  this</a> for the context in which the remarks were made).</p>

  <p>There is more than one reason why that is not a suitable
  change:</p>

  <ul>
    <li>
      <p>The change modifies existing behavior&mdash;silently.</p>
    </li>

    <li>
      <p>The change is redundant (the "<code>-w</code> option
      already provides the desired functionality). Assuming that
      <code>COLUMNS</code> were set reliably to a useful value, one
      could do</p>

      <blockquote>
        <pre>
diffstat -w$COLUMNS
</pre>
      </blockquote>I noted this in my initial response on the
      topic.
    </li>

    <li>
      <p>The change does no error-checking. If that variable
      happens to be set (even to an empty string) then it will use
      that value.</p>
    </li>

    <li>
      <p>The patch hardcodes <code>STDOUT</code> as a variable in
      the main function, rather than using
      <code>STDOUT_FILENO</code> (POSIX).</p>
    </li>

    <li>
      <p>The <code>COLUMNS</code> and <code>LINES</code>
      environment variables are set by only a few applications
      (<a href="/xterm/manpage/resize.html">resize</a> being one),
      and <strong>bash</strong> being another. A third does not
      come to mind (certainly not another shell).</p>

      <p>The <strong>resize</strong> program's environment
      variables are generally discarded (not applied to the shell);
      it is useful for making system calls to tell the the computer
      the actual size of the terminal window. On the other hand,
      <strong>bash</strong> does set the variables.</p>

      <p>In some configurations (Debian), <strong>bash</strong>
      sets a shell variable (which is not exported to
      subprocesses). In others (apparently the case with OpenSuSE),
      <strong>bash</strong> exports environment variables. This is
      without the complication of scripts which do an
      <code>export</code> of the shell variables.</p>
    </li>

    <li>
      <p>Some programs <em>use</em> these variables. In ncurses for
      instance, this is a standard legacy feature, useful for cases
      where the operating system cannot provide the required
      information (see <a href=
      "/ncurses/man/curs_util.3x.html">use_env</a>, compare with
      <strong>use_tioctl</strong>). Otherwise it is a nuisance
      because it interferes with programs that are able to obtain
      the screensize without this crutch (<a href=
      "/xterm/xterm.html">xterm</a> sets the variable only on a few
      very old platforms for this reason). A few other programs
      which do not use ncurses (such as <strong>ps</strong> as
      noted in Novell <a href=
      "https://bugzilla.novell.com/show_bug.cgi?id=793536">#793536</a>)
      can be overridden by <code>COLUMNS</code>.</p>
    </li>

    <li>
      <p>This behavior of <strong>bash</strong>'s has been seen as
      a nuisance, e.g., Novell <a href=
      "https://bugzilla.novell.com/show_bug.cgi?id=828877">#828877</a>,
      along with these threads from bug-bash in <a href=
      "https://lists.gnu.org/archive/html/bug-bash/2013-01/msg00060.html">
      2013-01</a>, <a href=
      "https://lists.gnu.org/archive/html/bug-bash/2013-07/msg00054.html">
      2013-07</a>. After reading several reports, e.g., ArchLinux
      <a href="https://bugs.archlinux.org/task/32821">#32821</a>,
      Debian <a href=
      "http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=628638">#628638</a>
      (<a href=
      "https://blogs.oracle.com/dp/entry/why_bash_doesn_t_work">and
      blogs</a>), a common thread emerges: <strong>bash</strong>
      has tied two behaviors together:</p>

      <ul>
        <li>It relies upon catching SIGWINCH so that it can itself
        display properly.</li>

        <li>It tells other applications to use the information by
        setting variables (which may directly or indirectly be
        exported).</li>
      </ul>

      <p>As long as the two behaviors (making <strong>bash</strong>
      work properly, and telling applications to use that
      information) are tied together, the feature is going to be a
      nuisance.</p>
    </li>

    <li>
      <p>Because there is no relevant standard (for the behavior of
      shell programs), users with scripts which happen to set the
      variable would be impacted. Debian for instance switched from
      <strong>bash</strong> to <strong>dash</strong> years ago. The
      latter does nothing with <code>COLUMNS</code>, so that
      scripts which work properly with <strong>dash</strong> would
      behave differently on a machine where <strong>bash</strong>
      was preferred.</p>
    </li>
  </ul>

  <p>The change was applied to the Debian package two years later,
  without discussion immediately after a change of package
  maintainers, (see Debian <a href=
  "http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=588876">#588876</a>).
  A user pointed out part of the problem with the change in Debian
  <a href=
  "http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=697696">#697696</a>,
  but made no headway with yet another maintainer.</p>

  <h3><a name="bad_license" id="bad_license">Licensing</a></h3>

  <p>I changed the copyright notice of <strong>diffstat</strong> to
  use MIT-X11 licensing at the beginning of 1998 (version 1.26).
  Before that, I had used the same wording as I did in other works
  distributed from 1994 onward, e.g., the <a href=
  "/ncurses/ncurses-license.html#ncurses_1_9_3">resizeterm</a>
  patch. The reason for this change was likely prompted by my work
  to <a href=
  "/ncurses/ncurses-license.html#issues_freer">relicense</a>
  ncurses, but also taking into account an old (October 1996)
  discussion with Joey Hess.</p>

  <p>The license is (of course) given in full as a comment at the
  top of the files which comprise the program. Nothwithstanding
  this, some packagers find it inconvenient to cite the license
  properly. Here are a few examples:</p>

  <ul>
    <li>
      <p>A bug report for <a href=
      "http://ports.haiku-files.org/ticket/329">Haiku</a> in 2010
      commented that the packager had trouble finding the license,
      and referred to it as &ldquo;DEC&rdquo;, apparently
      unfamiliar with MIT-X11. A followup patch for the package
      script referred to it as the &ldquo;diffstat&rdquo;
      license.</p>
    </li>

    <li>Some packagers get closer. OpenPkg labels
    <strong>diffstat</strong> as &ldquo;MIT-style&rdquo; which
    sounds better than &ldquo;DEC&rdquo; except for raising the
    question of what reference text they use.

      <p>On the other hand, OpenPkg labels <a href=
      "/luit/luit.html">luit</a> (also MIT-X11) as <a href=
      "http://download.openpkg.org/packages/current/source/EVAL/luit-20130217-20130218.src.rpm">
      GPL</a>. I notified them about that, as well as
      misattribution in their <a href=
      "http://download.openpkg.org/packages/current/source/EVAL/pdcurses-3.4-20080909.src.rpm">
      pdcurses</a> package in February 2014 without receiving a
      response.</p>
    </li>

    <li>
      <p>Next, (going down the scale), there are instances where
      the packager labels it &ldquo;gpl-like&rdquo; in the license
      field.<br>
      That is analogous to a pet-shop owner who puts a sign saying
      &ldquo;dog-like&rdquo; in front of a feline. Some people
      might object.</p>

      <p><a href=
      "http://distrowatch.com/table.php?distribution=sot">SOT
      Linux</a> did that with <a href=
      "http://ftp.sunet.se/pub/os/Linux/distributions/bestlinux/sotlinux-2003/SOTL_2003_SERVER/manuals/packages/diffstat-1.28-4.i386.html">
      diffstat 1.28</a> in 2003.</p>

      <p>Possibly related, <a href=
      "http://distrowatch.com/table.php?distribution=mageia">Mageia</a>
      does this with <a href=
      "http://mageia.madb.org/package/show/release/4/application/0/name/diffstat">
      diffstat 1.57</a> as of 2014. I notified them in February
      2014, received no response.</p>
    </li>

    <li>As of May, 2014, I know of no packages currently labeling
    <strong>diffstat</strong> as &ldquo;GPL&rdquo; (there have been
    instances which have been resolved).</li>
  </ul>

  <h2 id="documentation-id"><a name="documentation" id=
  "documentation">Documentation</a></h2>

  <ul>
    <li><a href="/diffstat/manpage/diffstat.html">diffstat
    program</a> (<a href="/diffstat/manpage/diffstat.pdf">pdf</a>)
    (<a href="/diffstat/manpage/diffstat.ps">postscript</a>)
    (<a href="/diffstat/manpage/diffstat.txt">plain text</a>)</li>
  </ul>
  <hr>

  <h2 id="download-h2"><a name="download" id=
  "download">Download</a></h2>

  <ul>
    <li><a href=
    "ftp://invisible-island.net/diffstat/diffstat.tar.gz">The
    source (ftp)</a></li>

    <li><a href="/datafiles/release/diffstat.tar.gz">The source
    (http)</a></li>
  </ul>

  <h2 id="related-h2"><a name="related" id="related">Related
  Links</a></h2>

  <h3><a name="packages" id="packages">Packages for
  diffstat</a></h3>

  <ul>
    <li><a href=
    "http://oswatershed.org/pkg/diffstat">OS<em>Watershed</em>.org</a></li>

    <li><a href=
    "http://www.perzl.org/aix/index.php?n=Main.Diffstat">AIX</a></li>

    <li><a href=
    "http://packages.debian.org/search?searchon=names&amp;keywords=diffstat">
    Debian</a></li>

    <li><a href=
    "https://www.freshports.org/textproc/diffstat/">FreeBSD</a></li>

    <li><a href=
    "http://hpux.connect.org.uk/hppd/hpux/Users/diffstat-1.54/man.html">
    HPUX</a></li>

    <li><a href=
    "http://trac.macports.org/browser/trunk/dports/devel/diffstat/Portfile">
    Mac<em>Ports</em></a></li>

    <li><a href=
    "http://ftp.netbsd.org/pub/pkgsrc/current/pkgsrc/textproc/diffstat/README.html">
    NetBSD pkgsrc/textproc</a></li>

    <li><a href="https://www.opencsw.org/package/diffstat/">OpenCSW
    (Solaris)</a></li>
  </ul>

  <h3><a name="cm_tools" id="cm_tools">Version control
  systems</a></h3>

  <p>Version control systems which have implemented diffstat's
  include</p>

  <ul>
    <li><a href=
    "http://doc.bazaar.canonical.com/plugins/en/diffstat-plugin.html">
    Bazaar</a> &ndash; The Adaptive Version Control System</li>

    <li>Git <a href=
    "https://www.kernel.org/pub/software/scm/git/docs/git-format-patch.html">
    format-patch</a> and <a href=
    "https://www.kernel.org/pub/software/scm/git/docs/git-diff.html">
    diff</a></li>

    <li><a href=
    "http://mercurial.selenic.com/wiki/DiffstatOfPulledChanges">Mercurial</a></li>

    <li><a href=
    "http://github.com/marschall/svn-diffstat">svn-diffstat (for
    Subversion)</a></li>

    <li><a href="http://drupal.org/node/979066">Drupal</a></li>
  </ul>

  <p>Some are slower:</p>

  <ul>
    <li><a href="http://bugs.darcs.net/issue1387">darcs whatsnew
    --summary: diffstat style</a> (March 2009)</li>

    <li><a href=
    "http://code.google.com/p/reviewboard/issues/detail?id=2036">reviewboard
    &ndash; Provide diff statistics (diffstat output) on review
    requests</a> (March 2011)</li>
  </ul>

  <p>A few tools extend one or more of the version control systems,
  enabling their diffstat features to be used via the tool:</p>

  <ul>
    <li>The <a href=
    "http://sourceforge.net/projects/scm-pepper/">scm-pepper</a>
    tool features &ldquo;Fast local <em>diffstat</em> and meta-data
    cache using zlib compression&rdquo; with Git, Mercurial or
    Subversion backends.</li>
  </ul>

  <h3><a name="other_uses" id="other_uses">Other Uses</a></h3>

  <p>Besides imitating <strong>diffstat</strong>, there are
  embedded uses of the original tool:</p>

  <ul>
    <li><a href=
    "http://www.eyrie.org/~eagle/software/cvslog/cvslog.html">CVS</a>
    (<a href=
    "http://www.eyrie.org/~eagle/software/cvslog/cvslog-changes.html">change-log</a>)</li>

    <li><a href=
    "http://lintian.debian.org/library-api/Lintian/Collect/Source.html">
    Debian <em>lintian</em> scripts</a></li>

    <li><a href=
    "https://gitorious.org/gpl-compliance-tools/gpl-compliance-scripts/source/c55553272557053396ae1d04e02a0068a99532f5:diffstat-total-compare.plx">
    GPL-compliance scripts</a></li>
  </ul>

  <h3><a name="derived" id="derived">Other implementations</a></h3>

  <ul>
    <li><a href=
    "http://www.emacswiki.org/emacs/Diffstat">Emacs</a></li>

    <li><a href="http://code.google.com/p/ndiffstat/">ndiffstat
    &ndash; diffstat-like program in .NET/C#</a></li>
  </ul><!--
From: LLa...@entel.cl (Lagos Munoz Leonardo Alberto)
Subject: What these numbers (in the kernel-announce) mean?
Date: 1998/05/08
Message-ID: <c=US%a=_%p=ENTEL_S.A.%l=ECCNT2SEXC01-980508140359Z-13473@nt.entel.cl>#1/1
X-Deja-AN: 351436221
Approved: g...@greenie.muc.de
Sender: muc.de!l-linux-kernel-owner
Newsgroups: muc.lists.linux-kernel
> (Original Message)
> From: kd...@linux.kernel.org [mailto:kd...@linux.kernel.org] 
> Subject: Linux kernel 2.1.100 released

Newsgroups: muc.lists.linux-kernel
From: tre...@jpj.net (Trevor Johnson)
Date: 1998/06/08
Subject: 2.1.105 spelling

From: tri...@samba.anu.edu.au (Andrew Tridgell)
Subject: Re: jitterbug
Date: 1998/10/01
Message-ID: <19981001024000Z12670492-25139+12810@samba.anu.edu.au>#1/1
X-Deja-AN: 396579856
Approved: g...@greenie.muc.de
Sender: muc.de!l-linux-kernel-owner
References: <Pine.LNX.4.05.9809301600360.20400-100000@ns.snowman.net>
Reply-To: tri...@samba.anu.edu.au
Newsgroups: muc.lists.linux-kernel

Newsgroups: linux.kernel
From: Linus Torvalds <torva...@transmeta.com>
Date: Thu, 27 Dec 2001 21:40:11 +0100
Local: Thurs, Dec 27 2001 4:40 pm 
Subject: Re: [PATCH] rlimit_nproc

Newsgroups: lucky.linux.kernel
From: torva...@transmeta.com (Linus Torvalds)
Date: Tue, 15 Jan 2002 18:07:56 +0000 (UTC)
Local: Tues, Jan 15 2002 2:07 pm 
Subject: Re: Why not "attach" patches?

2.4.21 2002-11-28
        (one of the first tarballs that uses version in name rather than just "linux")
2.4.37 2008-12-02
linux-2.4.37/Documentation/BK-usage
cset-to-linus
-->
</body>
</html>
