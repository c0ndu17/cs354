/**
 *  * @author George Phillips, 16512561
 *   *
 *    */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<pthread.h>
#include<ncurses.h>

/**
 *  * Definitions
 *   */
#define true 1
#define false 0
#define Client_MAX 1024
#define Server_MAX 2048

/**
 *  * Function Declarations
 *   */
void *server_send (void *sock);
void *server_recv (void *sock);
void close_windows();

/**
 *  * Global Variables
 *   */

int alive[] = {false, false};
char **msg_buffer;
WINDOW *input_scr;
WINDOW *msg_list_scr;
WINDOW *user_list_scr;

int main(int argc, char *argv[])
{
  int socket_comm, alive_count, i;
  struct sockaddr_in server;
  char *message_to_server = malloc(sizeof(char)*Client_MAX), *message_from_server = malloc(sizeof(char)*Server_MAX);
  pthread_t thread[2];

  initscr();
  echo();
  keypad(stdscr, TRUE);
  raw();

  if (argc <1) {
    endwin();
    printf("<usage>: ./LRCClient <ip_of_server>");
    return;
  }
  /* ncurses WINDOW Initialisation */
  if ( LINES < 10 || COLS < 21 ) {
    printf("Screen too small\nExiting...");
    return EXIT_FAILURE;
  }
  msg_list_scr    = newwin(LINES-3, COLS-21, 0, 0);
  user_list_scr   = newwin(LINES-3, 20, 0, COLS-21);
  input_scr       = newwin(3, COLS, LINES-3, 0);

  if(((input_scr ==NULL))||((msg_list_scr == NULL)||((user_list_scr == NULL)))||((msg_buffer = (char **) malloc(sizeof(char *) * LINES-5))== NULL)){
    printf("Failed to initialize all the screens\nExiting.\n");
    return EXIT_FAILURE;
  }
  wborder(input_scr, 0, 0, 0, 0, 0, 0, 0, 0);
  wborder(msg_list_scr, 0, 0, 0, 0, 0, 0, 0, 0);
  wborder(user_list_scr, 0, 0, 0, 0, 0, 0, 0, 0);
  mvwaddstr(user_list_scr, 1, 1, "LRCServer.");
  wrefresh(input_scr);
  wrefresh(msg_list_scr);
  wrefresh(user_list_scr);
  if ((socket_comm = socket(AF_INET, SOCK_STREAM, 0))<0) {
    close_windows();
    endwin();
    printf("failed to open a socket\n");
    return EXIT_FAILURE;
  }
  //TODO: Server Identity
  server.sin_addr.s_addr = inet_addr(argv[1]);
  server.sin_family = AF_INET;
  server.sin_port = htons(3000);

  if (connect(socket_comm, (struct sockaddr *)&server, sizeof(server))<0) {
    close_windows();
    endwin();
    printf("Failed to connect to socket\n");
    return EXIT_FAILURE;
  }
  mvwaddstr(user_list_scr, 3, 1, "Connected.");
  wrefresh(user_list_scr);
  while (message_from_server != "Logged In!") {
    bzero(message_from_server, Server_MAX);
    bzero(message_to_server, Client_MAX);
    wborder(input_scr, 0, 0, 0, 0, 0, 0, 0, 0); //ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
    mvwaddstr(input_scr, 1, 1, "Enter Username: ");
    wrefresh(input_scr);
    mvwgetnstr(input_scr, 1, 16, message_to_server, Client_MAX);
    if(strlen(message_to_server) < 17){
      if (send(socket_comm, message_to_server, Client_MAX, 0) < 0) {
        printf("Send failed.");
        break;
      }
    }
    if (recv(socket_comm, message_from_server, Server_MAX, 0)<0) {
      printf("Failed to receive from server.");
      break;
    }
    if (strcasecmp(message_from_server, "1")==0) {
      bzero(message_from_server, 1);
      strcpy(message_from_server, "The maximum username allowed is 16 characters");
    } else if (strcasecmp(message_from_server, "2") == 0) {
      bzero(message_from_server, 1);
      strcpy(message_from_server, "The username was either taken or did not only consist of alphanumeric characters");
    } else if(strcasecmp(message_from_server, "Logged In!") == 0) {
      break;
    }
    mvwaddnstr(msg_list_scr, 1, 1, message_from_server, LINES-23);
    wrefresh(msg_list_scr);
    werase(input_scr);
  }
  werase(user_list_scr);
  wborder(user_list_scr, 0, 0, 0, 0, 0, 0, 0, 0); //ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
  mvwaddstr(user_list_scr, 1, 1, "Welcome.");
  wrefresh(user_list_scr);

  if(message_from_server != NULL) {
    if (strcasecmp(message_from_server, "Logged In!") == 0) {
      alive[0]=true;
      if (pthread_create(&(thread[0]), NULL, server_send, (void *)&socket_comm) < 0) {
        close_windows();
        endwin();
        printf("could not spawn child_thread(send) for client\n");
        alive[0]=false;
        return -1;
      }
      alive[1]=true;
      if ((pthread_create(&(thread[1]), NULL, server_recv, (void *)&socket_comm) < 0) && alive[0]==false) {
        alive[1]=false;
        alive[0] = false;
        close_windows();
        endwin();
        printf("could not spawn child_thread(recv) for client\n");
        return -1;
      }
    }
  } else {
    wborder(user_list_scr, 0, 0, 0, 0, 0, 0, 0, 0); //ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
    wclear(user_list_scr);
    mvwaddstr(user_list_scr, 1, 1, "Failed to verify username\n");
    wrefresh(user_list_scr);
  }
  while (alive_count != 0) {
    alive_count =0;
    for(i =0; i<2; i++){
      alive_count += alive[i];
    }
  }
  free(message_from_server);
  free(message_to_server);
  pthread_join(thread[0], NULL);
  pthread_join(thread[1], NULL);
  close(socket_comm);
  close_windows();
  endwin();
  printf("Goodbye!\n");
  return EXIT_SUCCESS;
}

void close_windows(){
  delwin(user_list_scr);
  delwin(msg_list_scr);
  delwin(input_scr);
}

void *server_send (void *socket_desc) {
  char *message_to_server = malloc(sizeof(char)*Client_MAX);
  int socket_comm = *(int *)socket_desc;
  werase(input_scr);
  while (alive[0]) {
    wborder(input_scr, 0, 0, 0, 0, 0, 0, 0, 0); //ACS_VLINE, ACS_VLINE, ACS_HLINE, ACS_HLINE, ACS_ULCORNER, ACS_URCORNER, ACS_LLCORNER, ACS_LRCORNER);
    mvwaddstr(input_scr, 1, 1, "Enter Message: ");
    wrefresh(input_scr);
    wgetnstr(input_scr, message_to_server, getmaxx(input_scr)-18);
    if ( strlen(message_to_server) != 1 && (char)message_to_server[0] != '\n') {
      if ( strlen(message_to_server) > Client_MAX-1 ) {
        mvwaddstr(input_scr, 1, 1, "Message too large, Maximum size is 1023 characters");
        wrefresh(input_scr);
      }
      else if ( send(socket_comm, message_to_server, Client_MAX, 0) < 0 ) {
        mvwaddstr(input_scr, 1, 1, "Send failed.");
        wrefresh(input_scr);
        alive[0] = false;
      }
    }
    if (message_to_server != NULL) {
      if (strcasecmp(message_to_server, "/exit")==0)
        alive[0]=false;
    }
    bzero(message_to_server, strlen(message_to_server));
    werase(input_scr);
  }
  free(message_to_server);
  wclear(input_scr);
}

void *server_recv (void *socket_desc) {

  char *message_from_server = malloc(sizeof(char)*Server_MAX);
  int socket_comm = *(int *)socket_desc, i =1;
  while (alive[1]) {
    if (i==LINES-4) {
      i=1;
      werase(msg_list_scr);
    }
    if (recv(socket_comm, message_from_server, 2048, 0)<0) {
      printf("Failed to receive from server.");
    }
    if (message_from_server != NULL) {
      if (strcasecmp(message_from_server, "/exit") == 0) {
        alive[1] = false;
      } else {
        mvwaddstr(msg_list_scr, i, 1, message_from_server);
        i++;
      }
    }
    wrefresh(msg_list_scr);
    bzero(message_from_server, strlen(message_from_server));
  }
  alive[1] = false;
  free(message_from_server);
}

void print_commands(WINDOW *win, int line, int col, int start_y, int start_x){
  int i=start_y;
  //while ( i < getmaxy(win)) {
  mvwaddstr(win, start_y+i, 1, "Commands:");
  i++;
  mvwaddstr(win, start_y+i, 1, "/w <username> to whisper.");
  i++;
  mvwaddstr(win, start_y+i, 1, "/l to update user list");
  i++;
  mvwaddstr(win, start_y+i, 1, "/exit quit");
  //}

}

