import java.io.*;
import java.net.*;

public class LRCServer extends Thread {
    public static int port;

    public static void main(String[] args) {
        if(args.length <1){
            System.out.println("Please specify the port number");
            System.exit(1);
        } else if (Integer.parseInt(args[0])>65535 || Integer.parseInt(args[0])<0){
            System.out.println("Please specify a valid port number 0-65535");
            System.exit(1);
        }

        SSLServerSocket serverSocket = null;
        try{
            serverSocket = new SSLServerSocket(port);

        } catch (IOException e){
            System.out.println(e.getMessage());
            System.exit(1);
        }

    }
}
