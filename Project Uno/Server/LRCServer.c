/**
 * @author George Phillips, 16512561
 */
#include<stdio.h>
#include<string.h>
#include<regex.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h>   //threading
#include<errno.h>

/**
  Definitions
 */
typedef int bool;
#define true 1
#define false 0

#define Client_MAX 1024
#define Server_MAX 2048
/**
  DataTypes
 */

typedef struct ClientThread_t{
  bool alive;
  int *sock_desc;
  pthread_t thread_id;
  char *username;
  struct ClientThread_t *next;
} ClientThreadType;

/**
  Function Declarations
 */

int server_logic (int client_sock, int client_count);
void *client_thread_start (void *client_sock_desc);
void user_retrieve(char *ret_str, int placeholder);
void remove_user(int *socket_desc);
int recv_msg(int *socket_desc, char *message);
void send_to_user (char *username, char *message);
void send_msg(int *socket_desc, char *message);
int broadcast_msg (char *message_to_broadcast);
ClientThreadType *add_user (int socket_desc);
void add_username (char *username, ClientThreadType *local_ref);
int user_check (char *username);
int reg_check (char *username);
void trim (char* str_to_trim);
void left_trim (char* str_to_trim);
void right_trim (char* str_to_trim);

/**
  Global Variables
 */
ClientThreadType *client_list;
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t user_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t recv_lock = PTHREAD_MUTEX_INITIALIZER;

int main(int argc , char *argv[])
{
  int socket_desc, client_sock, client_desc, client_count =1, backlog =10, running_count=0;
  struct sockaddr_in server, client;
  ClientThreadType *tmp_client;
  /*
     pthread_mutex_init(&write_lock);
     pthread_mutex_init(&recv_lock);
   */
  printf("Server starting\n");
  printf("Stimulating Transistors\n");

  if ((socket_desc =socket(AF_INET , SOCK_STREAM , 0))<0) {
    printf("Failed to start server daemon socket\n");
    return EXIT_FAILURE;
  }
  printf("Server socket created\n" );

  //Prepare the sockaddr_in structure
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = INADDR_ANY;
  server.sin_port = htons( 3000 );

  if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
  {
    //print the error message
    printf("Failed to bind to socket\n");
    return EXIT_FAILURE;
  }
  printf("Bound to socket.\n");
  client_desc = sizeof(struct sockaddr_in);

  //Listen
  listen(socket_desc, backlog);


  printf("Waiting for incoming connections...\n");
  client_desc = sizeof(struct sockaddr_in);
  while ( (client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&client_desc))) {
    if(server_logic(client_sock, client_count++)<0){
      client_sock=-2;
      break;
    }
  }
  printf("Exited Accept\n");
  switch (client_sock) {
    case -1:
      printf("accept failed");
      return EXIT_FAILURE;
    case -2:
      printf("Server Error... exiting");
      return EXIT_FAILURE;
    default:
      break;
  }
  while(client_list!=NULL){
    tmp_client = client_list;
    client_list = client_list->next;
    free(tmp_client->sock_desc);
    free(tmp_client->username);
    free(tmp_client);
  }
  return EXIT_SUCCESS;
}

/*
 * Creates and detaches a client specific thread
 */

int server_logic(int client_sock, int client_count) {
  ClientThreadType *tmp_client = add_user(client_sock);
  if (pthread_create(&(tmp_client->thread_id), NULL, client_thread_start, (void *)tmp_client->sock_desc) < 0) {
    printf("could not spawn child_thread for client %d\n", client_count);
    return -1;
  }
  pthread_detach(tmp_client->thread_id);
  printf("client %d's thread spawned\n", client_count);
  return 0;
}

/*
   Client thread handler: Spawned pthread process
 */
void *client_thread_start(void *client_sock_desc) {
  int client_message_len, socket_comm = *(int *)client_sock_desc, ret_check, i, j;
  //TODO: username malloc size
  char message[Server_MAX], client_message[Client_MAX], *username=malloc(1024*sizeof(char)), *msg_helper;
  ClientThreadType *local_ref;
  pthread_mutex_lock(&user_lock);
  local_ref = client_list;
  while ( local_ref != NULL) {
    if (*(int *)(client_sock_desc) == *(local_ref->sock_desc))
      break;
    local_ref = local_ref->next;
  }
  pthread_mutex_unlock(&user_lock);
  if (local_ref == NULL) {
    printf("socket could not be found in list\n Exiting");
    return;
  }

  /* User Check */
  while ( (client_message_len = recv_msg(local_ref->sock_desc, client_message)) < 0);
  if ( (client_message_len == EWOULDBLOCK) || (client_message_len == EAGAIN) || (int)client_message[0]==0) {
    printf("Unauthenticated Client Disconnected\n");
    return;
  }
  trim(client_message);
  while ((ret_check=user_check(client_message)) != 1) {
    bzero(client_message, Client_MAX);
    bzero(message, Server_MAX);
    switch (ret_check){
      case -1:
        strcat(message, "1");
        break;

      default:
        strcat(message, "2");
    }
    send_msg(local_ref->sock_desc, message);
    while((client_message_len = recv_msg(local_ref->sock_desc, client_message))<0);
    if ( (client_message_len == EWOULDBLOCK) || (client_message_len == EAGAIN) || (int)client_message[0] == 0) {
      printf("Unauthenticated Client Disconnected\n");
      fflush(stdout);
      return;
    }
  }
  trim(client_message);
  memset(message, 0, Server_MAX);

  if (client_message_len > 0) {
    add_username(client_message, local_ref);
    bzero(message, Server_MAX);
    strcat(message, "Logged In!");
    send_msg(local_ref->sock_desc, message);
    bzero(client_message, Client_MAX);

    /* Client Loop: While Thread is alive loop*/
    while (local_ref->alive) {
      if ((client_message_len = recv_msg(local_ref->sock_desc, client_message)) > 0) {
        trim(client_message);
        printf("message from client: %s\t%s\n", local_ref->username, client_message);
        fflush(stdout);
        bzero(message, Server_MAX);
        if (strncasecmp(client_message, "/exit", 5)==0) {
          send_msg(local_ref->sock_desc, "/exit");
          local_ref->alive = false;
          msg_helper = strdup(local_ref->username);
          remove_user(local_ref->sock_desc);
          //TODO: Broadcast disconnect
          strcat(message, msg_helper);
          strcat(message, " disconnected");
          broadcast_msg(message);
          free(msg_helper);
          return;
        } else if ( strncasecmp(client_message, "/w ", 3) == 0 ) {
          //TODO: destruct message, construct whisper;
          memmove(client_message, client_message + 3, client_message_len - 3 + 1);
          msg_helper = strchr(client_message, (int)' ');
          strcat(message, "[Whisper] ");
          if (msg_helper !=NULL){
            bzero(username, 1024);
            i = 0;
            while(!isspace((unsigned char) client_message[i])){
              username[i] = client_message[i];
              i++;
            }
            memmove(client_message, client_message+i, Client_MAX -i+1 );
            strcat(message, local_ref->username);
            strcat(message, ": ");
            strcat(message, client_message);
            send_to_user(username, message);
          } else {
            send_msg(local_ref->sock_desc, "Invalid whisper command!\n");
          }
        } else if (strcasecmp(client_message, "/l") == 0){
          send_msg(local_ref->sock_desc, "User List:");
          i=0;
          pthread_mutex_lock(&user_lock);
          msg_helper = malloc(sizeof(char)*16);
          bzero(msg_helper, 16);
          user_retrieve(msg_helper, i++);
          while (strlen(msg_helper) != 0) {
            send_msg(local_ref->sock_desc, msg_helper);
            bzero(msg_helper, 16);
            user_retrieve(msg_helper, i++);
          }
          pthread_mutex_unlock(&user_lock);
          free(msg_helper);
          send_msg(local_ref->sock_desc, "0");

        } else {
          /* construct string to broadcast*/
          memset(message, 0, Server_MAX);
          strcat(message, local_ref->username);
          strcat(message, ": ");
          strcat(message, client_message);
          broadcast_msg(message);
        }
        memset(client_message, 0, Client_MAX);
      }
    }
  }
  if (client_message_len == 0){
    local_ref->alive=false;
    printf("Client Disconnected\n");
    fflush(stdout);
  } else if (client_message_len == -1) {
    printf("recv failed\n");
  }
}

/*
 *  Retrieves a list of users, taking a malloced char array as input and a place holder for the last user removed
 */
void user_retrieve(char *ret_str, int placeholder) {
  int i =0;
  ClientThreadType *local_ref;
  local_ref = client_list;
  while (local_ref != NULL) {
    if (local_ref->username != NULL) {
      if(i >=placeholder){
        strcpy(ret_str, local_ref->username);
        return;
      } else i++;
    }

    local_ref = local_ref->next;
  }
}



/**
 *  Non-Blocking recv from the client
 */
int recv_msg(int *socket_desc, char *message){
  int ret_val;
  pthread_mutex_lock(&recv_lock);
  ret_val =recv(*socket_desc, message, Client_MAX, MSG_DONTWAIT);
  pthread_mutex_unlock(&recv_lock);
  return ret_val;
}

/* */
void send_to_user (char *username, char *message) {
  ClientThreadType *local_ref;
  pthread_mutex_lock(&write_lock);
  pthread_mutex_lock(&user_lock);
  local_ref = client_list;
  while (local_ref != NULL) {
    if (local_ref->username != NULL) {
      if (strcasecmp(username, local_ref->username) == 0) {
        write(*(local_ref->sock_desc), message, Server_MAX);
        pthread_mutex_unlock(&user_lock);
        pthread_mutex_unlock(&write_lock);
        return;
      }
    }
    local_ref = local_ref->next;
  }
  pthread_mutex_unlock(&user_lock);
  pthread_mutex_unlock(&write_lock);
}

/* */
void send_msg (int *socket_desc, char *message) {
  pthread_mutex_lock(&write_lock);
  write(*socket_desc, message, Server_MAX);
  pthread_mutex_unlock(&write_lock);
}

int broadcast_msg(char *message_to_broadcast){
  ClientThreadType *client_looper;
  int sock_holder;
  pthread_mutex_lock(&user_lock);
  pthread_mutex_lock(&write_lock);
  client_looper = client_list;
  while (client_looper != NULL) {
    if (client_looper->sock_desc != NULL && client_looper->alive) {
      write( *(client_looper->sock_desc), message_to_broadcast, Server_MAX);
    }
    client_looper = client_looper->next;
  }
  pthread_mutex_unlock(&write_lock);
  pthread_mutex_unlock(&user_lock);
}

/*
 *
 */

void add_username (char *username, ClientThreadType *local_ref) {
  pthread_mutex_lock(&user_lock);
  local_ref->username = strdup(username);
  printf("user: %s added to database\n", local_ref->username);
  pthread_mutex_unlock(&user_lock);
}

/*
 * Called on the /exit to command to delete the user from the database
 */
void remove_user(int *socket_desc){
  ClientThreadType *tmp_sock_thread, *remove_sock_thread;
  pthread_mutex_lock(&user_lock);
  tmp_sock_thread =client_list;
  if(tmp_sock_thread == NULL ){
    pthread_mutex_unlock(&user_lock);
    return;
  }
  if(*(tmp_sock_thread->sock_desc)==*(socket_desc)){
    remove_sock_thread = client_list;
    client_list = client_list->next;
    if(remove_sock_thread->username != NULL)free(remove_sock_thread->username);
    free(remove_sock_thread->sock_desc);
    free(remove_sock_thread);
    pthread_mutex_unlock(&user_lock);
    return;
  } else {
    while (tmp_sock_thread->next != NULL) {
      if(*(tmp_sock_thread->next->sock_desc)==*(socket_desc)){
        remove_sock_thread = tmp_sock_thread->next;
        tmp_sock_thread->next = remove_sock_thread->next;
        if(remove_sock_thread->username != NULL)free(remove_sock_thread->username);
        free(remove_sock_thread->sock_desc);
        free(remove_sock_thread);
        pthread_mutex_unlock(&user_lock);
        return;
      }
      tmp_sock_thread = tmp_sock_thread->next;
    }
  }
  pthread_mutex_unlock(&user_lock);
}

/*
 *
 */

ClientThreadType *add_user (int socket_desc) {
  ClientThreadType *tmp_sock_thread;
  int *ret_val;
  tmp_sock_thread = (ClientThreadType *) malloc(sizeof(ClientThreadType));
  tmp_sock_thread->sock_desc = (int *)malloc(sizeof(int));
  *(tmp_sock_thread->sock_desc) = socket_desc;
  tmp_sock_thread->username = (char *)malloc(1024*sizeof(char));
  tmp_sock_thread->alive = true;
  pthread_mutex_lock(&user_lock);
  if (client_list==NULL) {
    client_list = tmp_sock_thread;
  } else {
    tmp_sock_thread->next = client_list;
    client_list = tmp_sock_thread;
  }
  pthread_mutex_unlock(&user_lock);
  return tmp_sock_thread;
}

/* checks if the user is in the database */
int user_check (char *username) {
  ClientThreadType *local_ref;
  if(strlen(username)>16) return -1;
  if(!reg_check(username)) return 0;
  pthread_mutex_lock(&user_lock);
  local_ref = client_list;
  while (local_ref!= NULL) {
    if (local_ref->username != NULL) {
      if (strcasecmp(username, local_ref->username) == 0) {
        pthread_mutex_unlock(&user_lock);
        return 0;
      }
    }
    local_ref = local_ref->next;
  }
  pthread_mutex_unlock(&user_lock);
  return 1;
}

/* Compares a username, to check if its valid via a regex expresion. Only passing if its an alpha numeric string*/
int reg_check (char *username) {
  regex_t regex;
  char *space_check;
  int regcode;

  regcode = regcomp(&regex, "^[[:alnum:]]", REG_ICASE);
  if(regcode){
    printf("regex could not be compiled for user %s\n", username);
    regfree(&regex);
    return 0;
  }
  regcode = regexec(&regex, username, 0, NULL, 0);
  if (!regcode) {
    printf("valid regex for %s\n", username);
    regfree(&regex);
    if((space_check = strchr(username, (int)' '))!=NULL);
    return 1;
  }
  printf("invalid regex for %s\n", username);
  printf("%d\n", (int) username[0]);
  regfree(&regex);
  return 0;
}

/* Contains functions for deleting whitespace from a */
void trim (char* str_to_trim) {
  left_trim(str_to_trim);
  right_trim(str_to_trim);
}

/* Trims trailing white space on the left side of a string */
void left_trim(char* str_to_trim){
  int n = 0;
  while (str_to_trim[n] != '\0' && isspace((unsigned char)str_to_trim[n])) {
    n++;
  }
  memmove(str_to_trim, str_to_trim + n, strlen(str_to_trim) - n + 1);
}

/* Trims trailing white space on the right side of a string */
void right_trim(char* str_to_trim){
  int n = strlen(str_to_trim);
  while (n > 0 && isspace(str_to_trim[n-1])) {
    n--;
  }
  str_to_trim[n] = '\0';
}


