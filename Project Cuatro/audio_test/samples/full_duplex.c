/**
 *  @author George Philips
 *  @file full_duplex.c
 */


#include <alsa/asoundlib.h>
/**
 *  Function Declarations
 */
int open_handle(snd_pcm_t *handle, int i);
void configure_audio(snd_pcm_t *handle, snd_pcm_hw_params_t *params);

/**
 *  Definitions
 */
#define Message_Max 2048

/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API

/**
 *  Global Variables
 */
snd_pcm_t *capture_handle;
snd_pcm_t *playback_handle;
snd_pcm_hw_params_t *capture_params;
snd_pcm_hw_params_t *playback_params;

int main() {
    Communicator();
}
/**
 *  Communicator
 */
int Communicator(){
    long loops;
    int init_val;
    int size;
    unsigned int val;
    int dir;
    snd_pcm_uframes_t frames;
    char *playback_buffer;

    /* Open PCM device for playback. 0 = playback / 1 = capture */
    open_handle(playback_handle, 0);
    open_handle(capture_handle, 1);

    /**
     *  Initialize playback variables
     */
    configure_audio(playback_handle, playback_params);
    configure_audio(capture_handle, capture_params);



    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(playback_params, &frames, &dir);
    printf("Frames:\t%d\tBuffer_size:\t%d\tDir:\t%d\n", (int)frames, (int)frames* 4, dir);

    size = frames * 4; /* 2 bytes/sample, 2 channels */
    playback_buffer = (char *) malloc(size);

    /* We want to loop for 5 seconds */
    snd_pcm_hw_params_get_period_time(playback_params, &val, &dir);
    printf("Period Time: %d\n", val);
    /* 5 seconds in microseconds divided by
     * period time */
    loops = 5000000 / val;

    while ((init_val = read(0, playback_buffer, size)) != 0) {
        if (init_val == 0) {
            fprintf(stderr, "end of file on input\n");
            break;
        } else if (init_val != size) {
            fprintf(stderr,
                    "short read: read %d bytes\n", init_val);
        }
        init_val = snd_pcm_writei(playback_handle, playback_buffer, frames);
        if (init_val == -EPIPE) {
            /* EPIPE means underrun */
            fprintf(stderr, "underrun occurred\n");
            snd_pcm_prepare(playback_handle);
        } else if (init_val < 0) {
            fprintf(stderr, "error from writei: %s\n", snd_strerror(init_val));
        }  else if (init_val != (int)frames) {
            fprintf(stderr, "short write, write %d frames\n", init_val);
        }
    }

    snd_pcm_drain(playback_handle);
    snd_pcm_close(playback_handle);
    free(playback_buffer);
    return 0;
}


int open_handle(snd_pcm_t *handle, int i) {
    int ret_val;
    if (i > 1) {
        return -1;
    }
    if (i == 0) {
        ret_val = snd_pcm_open(&handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    } else ret_val = snd_pcm_open(&handle, "default", SND_PCM_STREAM_CAPTURE, 0);
    if (ret_val < 0) {
        fprintf(stderr, "unable to open pcm device: %s\n", snd_strerror(ret_val));
        exit(1);
    }
}

void configure_audio(snd_pcm_t *handle, snd_pcm_hw_params_t *params){
    int val, init_val, dir;
    snd_pcm_uframes_t frames;

    /* Set Sample size and period size*/
    val = 44100;
    frames = 32;

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(handle, params);

    /* Set the desired hardware parameters. */
    /* Interleaved mode */
    snd_pcm_hw_params_set_access(handle, params, SND_PCM_ACCESS_RW_INTERLEAVED);
    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(handle, params, SND_PCM_FORMAT_S16_LE);
    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(handle, params, 2);
    /* 44100 bits/second sampling rate (CD quality) */
    snd_pcm_hw_params_set_rate_near(handle, params, &val, &dir);
    /* Set period size to 32 frames. */
    snd_pcm_hw_params_set_period_size_near(handle, params, &frames, &dir);

    /* Write the parameters to the driver */
    init_val = snd_pcm_hw_params(handle, params);
    if (init_val < 0) {
        printf("Set hw parameters:\t\t\t\t[FAILED!!!] %s\n", snd_strerror(init_val));
        exit(1);
    }
}
