/**
 *  @author George Phillips, 16512561
 *
 */
#include<stdio.h>
#include<string.h>
#include<regex.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h>   //threading
#include<errno.h>

/**
 *  Definitions
 */
typedef int bool;
#define true 1
#define false 0

#define Message_MAX 2048

/**
 *  Data Types
 */
typedef enum {NO_CALL, IN_CALL, END_CALL}call_status_e;

typedef struct MessageType_t {
    int flag;
    char buffer[Message_MAX];
} MessageType;

typedef struct CallGroup_t {
    int *cli_desc;
    struct CallGroup_t *next;
} CallGroup;

typedef struct ClientThreadType_t{
    bool alive;
    pthread_mutex_t in_call;
    char *client_ip;
    int *sock_desc;
    pthread_t thread_id;
    call_status_e call_status;
    char *username;
    struct ClientThreadType_t *next;
} ClientThreadType;


/**
 * Function Declarations
 */
int server_logic (int client_sock, int client_count);
void *client_thread_start (void *client_sock_desc);
int call_protocol (ClientThreadType *src_user, char *buffer);
void user_retrieve(char *ret_str, int placeholder);
void remove_user(int *socket_desc);
int recv_msg(int *socket_desc, MessageType *message);
int send_to_user (char *username, MessageType *message);
int send_msg(int *socket_desc, MessageType *message);
int broadcast_msg (MessageType *message_to_broadcast, int *local_desc);
ClientThreadType *add_user (int socket_desc);
void add_username (char *username, ClientThreadType *local_ref);
ClientThreadType *user_to_ref (char *username);
int user_check (char *username);
int reg_check (char *username);

/**
 * Global Variables
 */
ClientThreadType *client_list;
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t user_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t recv_lock = PTHREAD_MUTEX_INITIALIZER;

int main(int argc , char *argv[])
{
    int tcp_sock_desc, client_sock, client_desc, client_count, backlog;
    struct sockaddr_in tcp_server, client;
    ClientThreadType *tmp_client;
    printf("Server starting\n");
    printf("Stimulating Transistors\n");

    if ((tcp_sock_desc =socket(AF_INET , SOCK_STREAM , 0)) < 0) {
        printf("Failed to start server daemon socket\n");
        return EXIT_FAILURE;
    }
    printf("TCP server socket created\n" );

    //Prepare the sockaddr_in structure
    tcp_server.sin_family = AF_INET;
    tcp_server.sin_addr.s_addr = INADDR_ANY;
    tcp_server.sin_port = htons( 3000 );

    if( bind(tcp_sock_desc,(struct sockaddr *)&tcp_server , sizeof(tcp_server)) < 0)
    {
        //print the error message
        printf("Failed to bind to socket\n");
        return EXIT_FAILURE;
    }
    printf("Bound to TCP socket.\n");

    //Listen
    backlog = 10;
    listen(tcp_sock_desc, backlog);

    client_desc = sizeof(struct sockaddr_in);
    client_count =1;

    printf("Waiting for incoming connections...\n");
    while ( (client_sock = accept(tcp_sock_desc, (struct sockaddr *)&client, (socklen_t*)&client_desc))) {
        if(server_logic(client_sock, client_count++)<0){
            client_sock=-2;
            break;
        }
    }
    printf("Exited Accept\n");
    switch (client_sock) {
        case -1:
            printf("accept failed");
            return EXIT_FAILURE;
        case -2:
            printf("Server Error... exiting");
            return EXIT_FAILURE;
        default:
            break;
    }
    while(client_list!=NULL){
        tmp_client = client_list;
        client_list = client_list->next;
        free(tmp_client->sock_desc);
        free(tmp_client->username);
        free(tmp_client);
    }
    return EXIT_SUCCESS;
}

/*
 * Creates and detaches a client specific thread
 */

int server_logic(int client_sock, int client_count) {
    ClientThreadType *tmp_client = add_user(client_sock);
    if (pthread_create(&(tmp_client->thread_id), NULL, client_thread_start, (void *)tmp_client) < 0) {
        printf("could not spawn child_thread for client %d\n", client_count);
        return -1;
    }
    pthread_detach(tmp_client->thread_id);
    printf("client %d's thread spawned\n", client_count);
    return 0;
}

/**
 *  Client thread handler: Spawned pthread process
 */
void *client_thread_start(void *client_sock_desc) {
    int client_message_len, i, j;
    //TODO: username malloc size
    MessageType message_from_client, message_to_client;
    ClientThreadType *local_ref;
    local_ref = (ClientThreadType *) client_sock_desc;

    if (local_ref == NULL) {
        printf("socket could not be found in list\n Exiting");
        return;
    }

    /* User Check */
    message_from_client.flag = -1;
    bzero(message_from_client.buffer, Message_MAX);
    while ((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client))) < 0);
    if (client_message_len == 0) {
        printf("Unauthenticated Client Disconnected\n");
        return;
    }
    while ((message_to_client.flag = user_check(message_from_client.buffer)) != 1) {
        if(!send_msg(local_ref->sock_desc, &(message_to_client)))
            return;
        message_to_client.flag = -1;
        bzero(message_from_client.buffer, Message_MAX);
        while((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client)))<0);
        if ( client_message_len == 0) {
            printf("Unauthenticated Client Disconnected\n");
            fflush(stdout);
            return;
        }
    }
    add_username(message_from_client.buffer, local_ref);
    fflush(stdout);
    if(!send_msg(local_ref->sock_desc, &(message_to_client)))
        return;
    message_to_client.flag = -1;
    bzero(message_to_client.buffer, Message_MAX);

    while ((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client))) < 0) {
    }
    if (client_message_len == 0){
        printf("Failed to recieve I.P\n");
        return;
    }
    local_ref->client_ip = malloc(strlen(message_from_client.buffer)*sizeof(char));
    strcpy(local_ref->client_ip, message_from_client.buffer);

    /* Client Loop: While Thread is alive loop*/
    while (local_ref->alive) {
      message_from_client.flag = -1;
      bzero(message_from_client.buffer, Message_MAX);
      if ((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client))) > 0) {
        printf("message from client: %s\t%d\t%s\n", local_ref->username, message_from_client.flag, message_from_client.buffer);
        fflush(stdout);
        switch (message_from_client.flag) {
          case 0:
            message_to_client.flag = 0;
            sprintf(message_to_client.buffer, "%s", message_from_client.buffer);
            broadcast_msg(&(message_to_client), local_ref->sock_desc);
            message_to_client.flag = -1;
            bzero(message_to_client.buffer, Message_MAX);
            break;
          case 1:
            if(!send_msg(local_ref->sock_desc, &(message_to_client)))
              return;
            local_ref->alive = false;
            sprintf(message_to_client.buffer, "%s disconnected", local_ref->username);
            remove_user(local_ref->sock_desc);
            broadcast_msg(&(message_to_client), local_ref->sock_desc);
            printf("%s\n",message_to_client.buffer);
            message_to_client.flag = -1;
            bzero(message_to_client.buffer, Message_MAX);
            return;
          case 2:
            if (strcasecmp(local_ref->username, message_from_client.buffer)!=0) {
                local_ref->call_status = IN_CALL;
                call_protocol(local_ref, message_from_client.buffer);
            }
            break;
          case 3:
            local_ref->call_status = NO_CALL;
            break;
        }

      } else if (client_message_len == 0) {
        local_ref->alive=false;
        printf("Client Disconnected\n");
        fflush(stdout);
        return;
      } else if (client_message_len == -1) {
          if (errno != EWOULDBLOCK && errno != EAGAIN){
              local_ref->alive=false;
              printf("Client Disconnected\n");
              fflush(stdout);
              return;
          }
      }
    }
    if (client_message_len == 0){
        local_ref->alive=false;
        printf("Client Disconnected\n");
        fflush(stdout);
        return;
    } else if (client_message_len == -1) {
        printf("recv failed\n");
        return;
    }
}

/**
 *  Calls the given user
 */
int call_protocol (ClientThreadType *src_user, char *buffer) {
    int client_message_len, ret_val;
    pthread_t call_thread;
    MessageType message_to_client, message_from_client, message_to_others;
    char *addr_buffer;
    ClientThreadType *client_check;
    /*
    message_to_client.flag = 4;
    sprintf(message_to_client.buffer, "Call From %s, Would you like to pick it up?(y/n)\n", src_user->username);
    */
    addr_buffer = strtok(buffer, " ");
    if (addr_buffer == NULL) {
        return -1;
    }
    client_check = user_to_ref(addr_buffer);
    if (client_check != NULL) {
        message_to_client.flag = 3;
        sprintf(message_to_client.buffer, "%s ", client_check->client_ip);
    }
    while ((addr_buffer = strtok(NULL, " ")) != NULL) {
        client_check = user_to_ref(addr_buffer);
        if (client_check != NULL) {
          if (client_check->call_status != IN_CALL) {
            printf("Adding User: %s\n", client_check->username);
            message_to_client.flag = 3;
            strcat(message_to_client.buffer, client_check->client_ip);
            strcat(message_to_client.buffer, " ");
          }
        }
    }

    printf("Client_ip's: %s\n", message_to_client.buffer);
    ret_val = send_msg(src_user->sock_desc, &message_to_client);
    if (ret_val <= 0) {
        return -1;
    }
    message_to_others.flag = 2;
    sprintf(message_to_others.buffer, "%s", src_user->client_ip);
    addr_buffer = strtok(buffer, " ");
    if (addr_buffer == NULL) {
        return -1;
    }
    client_check = user_to_ref(addr_buffer);
    if (client_check != NULL) {
        ret_val = send_msg(client_check->sock_desc, &message_to_others);
        if (ret_val <= 0) {
            return -1;
        }
    }
    while ((addr_buffer = strtok(NULL, " ")) != NULL) {
        client_check = user_to_ref(addr_buffer);
        if (client_check != NULL) {
            client_check->call_status == IN_CALL;
            if (!send_msg(client_check->sock_desc, &message_to_others)) {
              return -1;
            }
        }
    }
    printf("Call Started...");
    return 1;
}

/**
 *  Retrieves a list of users, taking a malloced char array as input and a place holder for the last user removed
 */
void user_retrieve(char *ret_str, int placeholder) {
    int i =0;
    ClientThreadType *local_ref;
    local_ref = client_list;
    while (local_ref != NULL) {
        if (local_ref->username != NULL) {
            if (i >= placeholder) {
                strcpy(ret_str, local_ref->username);
                return;
            } else i++;
        }
        local_ref = local_ref->next;
    }
}



/**
 *  Non-Blocking read from the client
 */
int recv_msg(int *socket_desc, MessageType *message){
    int ret_val;
    pthread_mutex_lock(&recv_lock);
    ret_val = recv(*(socket_desc), message, sizeof(MessageType), MSG_DONTWAIT);
    pthread_mutex_unlock(&recv_lock);
    return ret_val;
}

/**
 *  Blocking write to the client
 */
int send_msg (int *socket_desc, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&write_lock);
    ret_val = write(*socket_desc, message, sizeof(MessageType));
    pthread_mutex_unlock(&write_lock);
    return ret_val;
}

/**
 *
 */
int send_to_user (char *username, MessageType *message) {
    ClientThreadType *local_ref;
    pthread_mutex_lock(&write_lock);
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while (local_ref != NULL) {
        if (local_ref->username != NULL) {
            if (strcasecmp(username, local_ref->username) == 0) {
                write(*(local_ref->sock_desc), (void *)message, sizeof(MessageType));
                pthread_mutex_unlock(&user_lock);
                pthread_mutex_unlock(&write_lock);
                return;
            }
        }
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    pthread_mutex_unlock(&write_lock);
}

/*
 * Broadcast message to users.
 */
int broadcast_msg(MessageType *message_to_broadcast, int *local_desc){
    ClientThreadType *client_looper;
    int sock_holder;
    pthread_mutex_lock(&user_lock);
    pthread_mutex_lock(&write_lock);
    client_looper = client_list;
    while (client_looper != NULL) {
        if ((client_looper->sock_desc != NULL )&& client_looper->alive && *(local_desc) != *(client_looper->sock_desc)) {
            write(*(client_looper->sock_desc), message_to_broadcast, Message_MAX);
        }
        client_looper = client_looper->next;
    }
    pthread_mutex_unlock(&write_lock);
    pthread_mutex_unlock(&user_lock);
}

/*
 *
 */
void add_username (char *username, ClientThreadType *local_ref) {
    int i;
    pthread_mutex_lock(&user_lock);
    local_ref->username = (char *) malloc(sizeof(char)*strlen(username));
    for ( i = 0; i< strlen(username); i++){
        local_ref->username[i] =username[i];
    }
    printf("user: %s added to database\n", local_ref->username);
    pthread_mutex_unlock(&user_lock);
}

/*
 * Called on the /exit to command to delete the user from the database
 */
void remove_user(int *socket_desc){
    ClientThreadType *tmp_sock_thread, *remove_sock_thread;
    pthread_mutex_lock(&user_lock);
    tmp_sock_thread = client_list;
    if(tmp_sock_thread == NULL ){
        pthread_mutex_unlock(&user_lock);
        return;
    }
    if(*(tmp_sock_thread->sock_desc)==*(socket_desc)){
        remove_sock_thread = client_list;
        client_list = client_list->next;
        if(remove_sock_thread->username != NULL)free(remove_sock_thread->username);
        free(remove_sock_thread->sock_desc);
        free(remove_sock_thread);
        pthread_mutex_unlock(&user_lock);
        return;
    } else {
        while (tmp_sock_thread->next != NULL) {
            if(*(tmp_sock_thread->next->sock_desc)==*(socket_desc)){
                remove_sock_thread = tmp_sock_thread->next;
                tmp_sock_thread->next = remove_sock_thread->next;
                if(remove_sock_thread->username != NULL)free(remove_sock_thread->username);
                free(remove_sock_thread->sock_desc);
                free(remove_sock_thread);
                pthread_mutex_unlock(&user_lock);
                return;
            }
            tmp_sock_thread = tmp_sock_thread->next;
        }
    }
    pthread_mutex_unlock(&user_lock);
}

/*
 * Add User to list
 */
ClientThreadType *add_user (int socket_desc) {
    ClientThreadType *tmp_sock_thread;
    int *ret_val;
    tmp_sock_thread = (ClientThreadType *) malloc(sizeof(ClientThreadType));
    tmp_sock_thread->sock_desc = (int *)malloc(sizeof(int));
    *(tmp_sock_thread->sock_desc) = socket_desc;
    tmp_sock_thread->alive = true;
    tmp_sock_thread->call_status = NO_CALL;
    pthread_mutex_lock(&user_lock);
    if (client_list==NULL) {
        client_list = tmp_sock_thread;
    } else {
        tmp_sock_thread->next = client_list;
        client_list = tmp_sock_thread;
    }
    pthread_mutex_unlock(&user_lock);
    return tmp_sock_thread;
}


/*
 * Returns a refence to client struct if the user is in the database.
 */
ClientThreadType *user_to_ref (char username[Message_MAX]) {
    ClientThreadType *local_ref;
    //if(!reg_check(username)) return 2;
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while (local_ref!= NULL) {
        if (local_ref->username != NULL) {
            if (strcasecmp(username, local_ref->username) == 0) {
                pthread_mutex_unlock(&user_lock);
                return local_ref;
            }
        }
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    return NULL;
}

/*
 * Checks if the user is in the database
 */
int user_check (char username[Message_MAX]) {
    ClientThreadType *local_ref;
    if(strlen(username)>16) return 0;
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while (local_ref!= NULL) {
        if (local_ref->username != NULL) {
            if (strcasecmp(username, local_ref->username) == 0) {
                pthread_mutex_unlock(&user_lock);
                return 3;
            }
        }
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    return 1;
}
/*if (strcasecmp(client_message, "/l") == 0){
  send_msg(local_ref->sock_desc, "User List:");
  i=0;
  pthread_mutex_lock(&user_lock);
  msg_helper = malloc(sizeof(char)*16);
  bzero(msg_helper, 16);
  user_retrieve(msg_helper, i++);
  while (strlen(msg_helper) != 0) {
  send_msg(local_ref->sock_desc, msg_helper);
  bzero(msg_helper, 16);
  user_retrieve(msg_helper, i++);
  }
  pthread_mutex_unlock(&user_lock);
  free(msg_helper);
  send_msg(local_ref->sock_desc, "0");

  } else {
  memset(message, 0, Message_MAX);
  strcat(message, local_ref->username);
  strcat(message, ": ");
  strcat(message, client_message);
  broadcast_msg(message);
  }*/
