/**
 * E:q:
 *  @author George Phillips, 16512561
 *  @file Client.c
 */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h>
#include<ifaddrs.h>
#include<pthread.h>
#include<alsa/asoundlib.h>


/**
 *  Definitions
 */
#define true 1
#define false 0
#define Message_MAX 2048

/* Use the newer ALSA API */
#define ALSA_PCM_NEW_HW_PARAMS_API

typedef struct MessageType_t {
    int flag;
    char buffer[Message_MAX];
} MessageType;

typedef struct ClientCallerType_t {
    int num_callers;
    struct sockaddr_in *caller;
} ClientCallerType;


typedef enum {NO_CALL, IN_CALL, END_CALL}call_status_e;
/**
 * Function Declarations
 */
void *server_send();
void *server_recv();
void close_windows();
int write_msg(int sockfd, MessageType *message);
int read_msg(int sockfd, MessageType *message);
void safe_get(char *response);
void *rec_n_send (void *void_list);
void *recv_n_play (void *void_list);
int communicator (char *inet_buffer, int is_server);
void *client_handler(void *socket_comm);
int conn_call_server(char *inet_buffer);
void *udp_serv(void *inet_buffer);
void *udp_cli(void *inet_buffer);

/**
 * Global Variables
 */
int alive[] = {false, false, false};
int tcp_socket_comm;
int udp_socket_comm;
int my_socket_comm;
in_addr_t local_ip;
struct sockaddr_in udp_server;
call_status_e call_status;
ClientCallerType *caller_list;
int *caller_sockfd;
pthread_t thread[4];
pthread_mutex_t write_lock;
pthread_mutex_t read_lock;
pthread_mutex_t get_lock;
pthread_mutex_t thread_lock;

int main(int argc, char *argv[])
{
    int alive_count, i, client_message_len;
    struct sockaddr_in server;
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    MessageType message_to_server, message_from_server;


    if (argc <2) {
        printf("<usage>: ./Client <ip_of_server>");
        return;
    }

    if ((tcp_socket_comm = socket(AF_INET, SOCK_STREAM, 0))<0) {
        close_windows();
        printf("failed to open a socket\n");
        return EXIT_FAILURE;
    }
    //TODO: Server Identity
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons(3000);

    if (connect(tcp_socket_comm, (struct sockaddr *)&server, sizeof(server))<0) {
        close_windows();
        printf("Failed to connect to socket\n");
        return EXIT_FAILURE;
    }
    while (message_from_server.flag != 1) {
        message_to_server.flag = -1;
        bzero(message_to_server.buffer, Message_MAX);
        printf("Enter Username: ");
        safe_get(message_to_server.buffer);
        if (strlen(message_to_server.buffer) < 17) {
            if ((client_message_len = write_msg(tcp_socket_comm, &(message_to_server))) < 0) {
                printf("Send failed.\n");
                break;
            }
            if (client_message_len == 0) {
                printf("Disconnected from Server\n");
                return;
            }
            if ((client_message_len = read_msg(tcp_socket_comm, &(message_from_server))) < 0) {
                printf("Failed to receive from server.\n <---");
                break;
            }
            if (client_message_len == 0) {
                printf("Disconnected from Server\n");
                return;
            }
            printf("flag: %d\n", message_from_server.flag);
            switch (message_from_server.flag) {
                case 0:
                    printf("The maximum username allowed is 16 characters\n");
                    break;
                case 1:
                    printf("Logged In!\n");
                    break;
                case 2:
                    printf("The username did not consist of only alphanumeric characters\n");
                    break;
                case 3:
                    printf("The username was taken\n");
                    break;
            }
        }
    }

    /*Determine IP*/

    bzero(message_to_server.buffer, sizeof(message_to_server));

    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr->sa_family==AF_INET) {
            if(strcasecmp(ifa->ifa_name, "eth0") == 0) {
                sa = (struct sockaddr_in *) ifa->ifa_addr;
                sprintf(message_to_server.buffer, "%s", inet_ntoa(sa->sin_addr));
                local_ip = inet_addr(message_to_server.buffer);
                printf("Interface: %s\tAddress: %s\n", ifa->ifa_name, inet_ntoa(sa->sin_addr));
                break;
            }
        }
    }

    if ((client_message_len = write_msg(tcp_socket_comm, &(message_to_server))) < 0) {
        printf("Send failed.\n");
        return;
    }
    freeifaddrs(ifap);

    alive[0] = true;
    if (pthread_create(&(thread[0]), NULL, server_send, NULL) < 0) {
        close_windows();
        printf("could not spawn child_thread(send) for client\n");
        alive[0]=false;
        return -1;
    }
    alive[1]=true;
    if (pthread_create(&(thread[1]), NULL, server_recv, NULL) < 0) {
        alive[1]=false;
        alive[0] = false;
        close_windows();
        printf("could not spawn child_thread(recv) for client\n");
        return -1;
    }
    while (true);
    while (alive_count != 0) {
        alive_count =0;
        for(i =0; i<2; i++){
            alive_count += alive[i];
        }
    }
    close(tcp_socket_comm);
    close_windows();
    printf("Goodbye!\n");
    return EXIT_SUCCESS;
}


/**
 * Sends an message to a client
 */
int write_msg (int sockfd, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&write_lock);
    ret_val = write(sockfd, message, sizeof(MessageType));
    pthread_mutex_unlock(&write_lock);
    return ret_val;
}

int read_msg (int sockfd, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&read_lock);
    ret_val = read(sockfd, (void *)message, sizeof(MessageType));
    pthread_mutex_unlock(&read_lock);
    return ret_val;
}

void safe_get(char *response) {
    pthread_mutex_lock(&get_lock);
    gets(response);
    pthread_mutex_unlock(&get_lock);
}

void *server_send () {
    MessageType message_to_server;
    int client_message_len;
    message_to_server.flag = -1;
    while (alive[0]) {
        if (call_status == NO_CALL) {
            message_to_server.flag = -1;
            bzero(message_to_server.buffer, Message_MAX);
            safe_get(message_to_server.buffer);
            if (message_to_server.buffer[0] != 0) {
                if (strncasecmp(message_to_server.buffer, "/exit", 5) == 0){
                    message_to_server.flag = 1;
                    printf("Exiting...\n");
                    alive[0]=false;
                    alive[1]=false;
                } else if (strncasecmp(message_to_server.buffer, "/call ", 6) == 0) {
                    memmove(message_to_server.buffer, message_to_server.buffer+6, strlen(message_to_server.buffer)-6+1);
                    message_to_server.flag = 2;
                } else message_to_server.flag = 0;
                if ((client_message_len = write_msg(tcp_socket_comm, &(message_to_server))) < 0 ) {
                    alive[0]=false;
                    alive[1]=false;
                }
                if (client_message_len == 0) {
                    printf("Disconnected from Server\n");
                    return;
                }
            }
        }
    }
    alive[1] = false;
    alive[0] = false;
}

void *server_recv () {
    char response[3];
    MessageType message_from_server, message_to_server;
    int i, client_message_len;
    i = 1;
    while (alive[1]) {
        message_from_server.flag = -1;
        bzero(message_from_server.buffer, Message_MAX);
        if ((client_message_len = read_msg(tcp_socket_comm, &(message_from_server))) < 0) {
            alive[0] = false;
            alive[1] = false;
            return;
        }
        if (client_message_len == 0) {
            printf("Disconnected from Server\n");
            alive[0] = false;
            alive[1] = false;
            return;
        }
        if (message_from_server.flag == 0) {
            printf("%s\n", message_from_server.buffer);
            i++;
        } else if (message_from_server.flag == 1) { //Exit
            alive[0] = false;
            alive[1] = false;
        } else if (message_from_server.flag == 2) { //PICKUP CALL
            printf("%s", message_from_server.buffer);
            call_status = IN_CALL;
            if (conn_call_server(message_from_server.buffer) == 0) {
                call_status = NO_CALL;
            } else printf("Communication threads initiated.\n");
        } else if (message_from_server.flag == 3) { //PICKUP CALL
            call_status = IN_CALL;
            if (start_call_server(message_from_server.buffer) <= 0) {
                call_status = NO_CALL;
            } else printf("Communication Server initiated.\n");
        }
    }
    alive[1] = false;
    alive[0] = false;
}


void *rec_n_send (void *void_list) {
    unsigned int val;
    int sockfd, ret_val, size, dir, client_message_len, i;
    char sendline[1364], *buffer;
    struct sockaddr_in *send_addr, cliaddr;
    FILE *src_file;
    snd_pcm_t *capture_handle;
    snd_pcm_hw_params_t *capture_params;
    snd_pcm_uframes_t frames;
    socklen_t len;

    val = 16000;
    frames = 32;
    printf("Hello from rec n send\n");
    /* Open PCM device for playback. */
    ret_val = snd_pcm_open(&capture_handle, "default", SND_PCM_STREAM_CAPTURE, 0);
    if (ret_val < 0) {
        printf("unable to open pcm device: %s\n", snd_strerror(ret_val));
        exit(1);
    }


    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&capture_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(capture_handle, capture_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(capture_handle, capture_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(capture_handle, capture_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(capture_handle, capture_params, 2);

    /* 44100 bits/second sampling rate (CD quality) */
    snd_pcm_hw_params_set_rate_near(capture_handle, capture_params, &val, &dir);

    /* Set period size to 32 frames. */
    snd_pcm_hw_params_set_period_size_near(capture_handle, capture_params, &frames, &dir);

    /* Write the parameters to the driver */
    ret_val = snd_pcm_hw_params(capture_handle, capture_params);
    if (ret_val < 0) {
        printf("unable to set hw parameters: %s\n", snd_strerror(ret_val));
        exit(1);
    }

    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(capture_params, &frames, &dir);
    size = frames; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);

    len = sizeof(struct sockaddr_in);
    while (alive[2])
    {
        ret_val = snd_pcm_readi(capture_handle, buffer, frames);
        if (ret_val != size) {
            printf("short read: read %d bytes\n", ret_val);
        }
        if (ret_val == -EPIPE) {
            fprintf(stderr, "capture underrun occurred\n");
            snd_pcm_prepare(capture_handle);
        } else if (ret_val < 0) {
            fprintf(stderr, "error from readi: %s\n", snd_strerror(ret_val));
        }  else if (ret_val != (int) frames) {
            fprintf(stderr, "short read, read %d frames\n", ret_val);
        }
        bzero(sendline, sizeof(sendline));
        sprintf(sendline, "%s", buffer);
        if (caller_list == NULL) {
            sendto(udp_socket_comm, sendline, sizeof(sendline), 0, (struct sockaddr *)&udp_server, len);
        } else {
            for (i = 0; i < caller_list->num_callers; i++) {
                sendto(udp_socket_comm, sendline, sizeof(sendline), 0, (struct sockaddr *)&(caller_list->caller[i]), len);
            }
        }
    }
    printf("Capture ending.\n");
    snd_pcm_drain(capture_handle);
    snd_pcm_close(capture_handle);
    free(buffer);
    return 0;
}

void *recv_n_play (void *void_list) {
    unsigned int val;
    int sockfd, i, ret_val, size, dir, client_message_len;
    char recvline[1364], *buffer;
    struct sockaddr_in cliaddr;
    snd_pcm_t *playback_handle;
    snd_pcm_hw_params_t *playback_params;
    snd_pcm_uframes_t frames;
    socklen_t len;

    val = 16000;
    frames = 32;


    printf("Hello from recv n play\n");
    /* Open PCM device for playback. */
    ret_val = snd_pcm_open(&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (ret_val < 0) {
        printf("unable to open pcm device: %s\n", snd_strerror(ret_val));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&playback_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(playback_handle, playback_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(playback_handle, playback_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(playback_handle, playback_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(playback_handle, playback_params, 2);

    /* 44100 bits/second sampling rate (CD quality) */
    snd_pcm_hw_params_set_rate_near(playback_handle, playback_params, &val, &dir);

    /* Set period size to 32 frames. */
    snd_pcm_hw_params_set_period_size_near(playback_handle, playback_params, &frames, &dir);

    /* Write the parameters to the driver */
    ret_val = snd_pcm_hw_params(playback_handle, playback_params);
    if (ret_val < 0) {
        fprintf(stderr, "unable to set hw parameters: %s\n", snd_strerror(ret_val));
        exit(1);
    }

    snd_pcm_hw_params_get_period_size(playback_params, &frames, &dir);
    size = frames; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);

    len = sizeof(struct sockaddr_in);
    while (alive[2])
    {
        if (caller_list == NULL) {
            client_message_len = recvfrom(udp_socket_comm, recvline, sizeof(recvline), 0, (struct sockaddr *)&udp_server, &len);
            printf("%d client_message_len\n", client_message_len);
            ret_val = snd_pcm_writei(playback_handle, recvline, frames);
            if (ret_val == -EPIPE) {
                fprintf(stderr, "playback underrun occurred\n");
                snd_pcm_prepare(playback_handle);
            } else if (ret_val < 0) {
                fprintf(stderr, "error from writei: %s\n", snd_strerror(ret_val));
            }  else if (ret_val != (int)frames) {
                fprintf(stderr, "short write, write %d frames\n", ret_val);
            }
        } else {
            for ( i = 0; i < caller_list->num_callers; i++) {
                if (caller_sockfd[i] != -1) {
                    client_message_len = recvfrom(udp_socket_comm, recvline, sizeof(recvline),0,(struct sockaddr *)&caller_list[i], &len);

                    if (client_message_len > 0) {
                        ret_val = snd_pcm_writei(playback_handle, recvline, frames);
                        if (ret_val == -EPIPE) {
                            fprintf(stderr, "underrun occurred\n");
                            snd_pcm_prepare(playback_handle);
                        } else if (ret_val < 0) {
                            fprintf(stderr, "error from writei: %s\n", snd_strerror(ret_val));
                        }  else if (ret_val != (int)frames) {
                            fprintf(stderr, "short write, write %d frames\n", ret_val);
                        }
                    }
                }
            }
        }
    }
    printf("Playback ending.\n");
    snd_pcm_drain(playback_handle);
    snd_pcm_close(playback_handle);
    free(buffer);
    return 0;

}

/**
 * Creates a server socket, and starts the udp transfer.
 */
int start_call_server (char *inet_buffer) {
    unsigned int val;
    int ret_val, size, dir, backlog, client_sock, client_len, i;
    char *buffer, *addr_token, *tmp_buf;
    in_addr_t *client_addresses;
    pthread_t *thread_block;
    struct sockaddr_in my_tcp_server, client;
    MessageType message_to_server;

    if(inet_buffer == NULL) {
        return 0;
    }
    printf("inet_buffer: %s\n", inet_buffer);
    caller_list = (ClientCallerType *) malloc(sizeof(ClientCallerType));
    caller_list->caller = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
    if ((addr_token = strtok(inet_buffer, " ")) == NULL) {
        printf("That Client cold not be contacted\n");
        call_status == NO_CALL;
        return 0;
    }
    tmp_buf = strdup(addr_token);
    bzero(&(caller_list->caller[caller_list->num_callers]), sizeof(struct sockaddr_in));
    caller_list->caller[caller_list->num_callers].sin_family = AF_INET;
    caller_list->caller[caller_list->num_callers].sin_addr.s_addr = inet_addr(addr_token);
    caller_list->caller[caller_list->num_callers].sin_port = htons(3001);
    caller_list->num_callers = 1;
    while ((addr_token = strtok(NULL, " ")) != NULL) {
        if ((realloc(caller_list->caller, (caller_list->num_callers+1) * sizeof(struct sockaddr_in))) == NULL){
            call_status == NO_CALL;
            return 0;
        }
        bzero(&(caller_list->caller[caller_list->num_callers]), sizeof(struct sockaddr_in));
        caller_list->caller[caller_list->num_callers].sin_family = AF_INET;
        caller_list->caller[caller_list->num_callers].sin_addr.s_addr = inet_addr(addr_token);
        caller_list->caller[caller_list->num_callers].sin_port = htons(3001);
        caller_list->num_callers += 1;
    }

    if ((my_socket_comm = socket(AF_INET, SOCK_STREAM , 0)) < 0) {
        printf("Failed to start server daemon socket\n");
        return 0;
    }
    my_tcp_server.sin_family = AF_INET;
    my_tcp_server.sin_addr.s_addr = INADDR_ANY;
    my_tcp_server.sin_port = htons( 3002 );

    printf("TCP server socket created\n" );
    if ( bind(my_socket_comm, (struct sockaddr *)&my_tcp_server, sizeof(my_tcp_server)) < 0) {
        //print the error message
        printf("Failed to bind to socket\n");
        return EXIT_FAILURE;
    }
    printf("Bound to TCP socket.\n");

    //Listen
    backlog = 10;
    listen(my_socket_comm, backlog);


    thread_block = (pthread_t *) malloc(sizeof(pthread_t)*(caller_list->num_callers));
    caller_sockfd = (int *) malloc(sizeof(int) * (caller_list->num_callers));
    for ( i=0; i < caller_list->num_callers; i++) {
        caller_sockfd[i] = -1;
    }
    i = 0;
    while ( i < caller_list->num_callers) {
        client_sock = accept(my_socket_comm, (struct sockaddr *)&client, (socklen_t*)&client_len);
        if (pthread_create(&(thread_block[i]), NULL, client_handler, (void *)&(client_sock)) < 0) {
            return 0;
        }
        caller_sockfd[i] = client_sock;
        i++;
    }
    alive[2] = true;
    if (pthread_create(&(thread[2]), NULL, udp_serv, (void *)tmp_buf ) < 0) {
        return 0;
    }
    for (i; i > 0; i--) {
        pthread_join(thread_block[i-1], NULL);
    }
    pthread_join(thread[2], NULL);
    printf("Joined\n");
    free(caller_list);
}

void *client_handler(void *sock_comm) {
    int client_message_len, sockfd, i;
    MessageType message_from_client, message_to_client;
    sockfd = *(int *)sock_comm;
    if ((client_message_len = read_msg(sockfd, &message_from_client)) < 0) {
        return;
    }
    if (client_message_len == 0) {
        printf("Client Disconnected\n");
        return;
    }
    if (message_from_client.flag == 0) {
        printf("Client denied the call.\n");
        for ( i =0; i < caller_list->num_callers; i++){
            if (caller_sockfd[i] == sockfd)
                caller_sockfd[i] = -1;
        }
        return;
    }
    while (true) {
        if ((client_message_len = read_msg(sockfd, &message_from_client)) < 0) {
            return;
        }
        if (client_message_len == 0) {
            printf("Client Disconnected");
            return;
        }
        if (message_from_client.flag == 0){
            printf("Call Connection closed.\n");
            return;
        } else if (message_from_client.flag == 1) {
            //broadcast_msg(message_to_client);
        } else if (message_from_client.flag == 2) {
        } else if (message_from_client.flag == 3) {
        }
    }
}

int conn_call_server(char *inet_buffer) {
    int alive_count, i, client_message_len, call_socket_comm;
    struct sockaddr_in call_server;
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    MessageType message_to_server, message_from_server;
    char response[2];

    if(inet_buffer == NULL) {
        return 0;
    }
    if ((call_socket_comm = socket(AF_INET, SOCK_STREAM, 0))<0) {
        close_windows();
        //endwin();
        printf("failed to open call socket\n");
        return EXIT_FAILURE;
    }
    //TODO: Server Identity
    printf("%s\n",inet_buffer);
    call_server.sin_family = AF_INET;
    call_server.sin_addr.s_addr = inet_addr(inet_buffer);
    call_server.sin_port = htons(3002);
    sleep(1);
    if (connect(call_socket_comm, (struct sockaddr *)&call_server, sizeof(call_server)) < 0) {
        close_windows();
        //endwin();
        printf("Failed to connect to socket, %s\n", snd_strerror(errno));
        return EXIT_FAILURE;
    }

    printf("You are being called, would you like to pick up? (y||Enter)\n");
    safe_get(response);
    if (response[0] == 'y' || response[0] == 0) {
        message_to_server.flag = 1;
        if (!write_msg(call_socket_comm, &message_to_server)) {
            return 0;
        }
    } else {
        message_to_server.flag = 0;
        write_msg(call_socket_comm, &message_to_server);
        message_to_server.flag = 3;
        write_msg(tcp_socket_comm, &message_to_server);
        return 0;

    }
    alive[2] = true;
    printf("%s\n", inet_buffer);
    if (pthread_create(&(thread[2]), NULL, udp_cli, (void *)inet_buffer) < 0) {
        return 0;
    }
    while (true) {
      safe_get(response);
      if (response[0] == 'x'){
        message_to_server.flag = 0;
        if ((client_message_len = write_msg(call_socket_comm, &message_to_server)) < 0) {
            break;
        }
        message_to_server.flag = 3;
        if ((client_message_len = write_msg(tcp_socket_comm, &message_to_server)) < 0) {
            break;
        }
        if (client_message_len == 0 ) {
            printf("Disconnected.\n");
            break;
        }
        printf("Close.\n");
        break;
      }
    }
    message_to_server.flag = 3;
    if ((client_message_len = write_msg(tcp_socket_comm, &message_to_server)) < 0) {
        return;
    }
    if (client_message_len == 0 ) {
        printf("Disconnected.\n");
        exit(0);
    }
}

void *udp_serv(void *inet_buffer){
    int sockfd, rc, n, size, dir;
    unsigned int val;
    char *buffer;
    char sendline[1364];
    char recvline[1364];
    struct sockaddr_in servaddr, cliaddr;
    snd_pcm_uframes_t frames;
    socklen_t len;
    snd_pcm_t *playback_handle, *capture_handle;
    snd_pcm_hw_params_t *playback_params, *capture_params;

    buffer = (char *)inet_buffer;
    printf("Starting UDP Transmission\n%s\n", buffer);
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;//inet_addr(buffer);
    servaddr.sin_port = htons(3001);
    bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr));

    /* Open PCM device for playback. */
    rc = snd_pcm_open(&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (rc < 0) {
        printf("unable to open pcm device: %s\n", snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&playback_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(playback_handle, playback_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(playback_handle, playback_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(playback_handle, playback_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(playback_handle, playback_params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 16000;
    snd_pcm_hw_params_set_rate_near(playback_handle, playback_params, &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(playback_handle, playback_params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(playback_handle, playback_params);
    if (rc < 0) {
        printf("unable to set hw parameters: %s\n", snd_strerror(rc));
        exit(1);
    }

    rc = snd_pcm_open(&capture_handle, "default", SND_PCM_STREAM_CAPTURE, 0);
    if (rc < 0) {
        printf("unable to open pcm device: %s\n", snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&capture_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(capture_handle, capture_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(capture_handle, capture_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(capture_handle, capture_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(capture_handle, capture_params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 16000;
    snd_pcm_hw_params_set_rate_near(capture_handle, capture_params, &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(capture_handle, capture_params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(capture_handle, capture_params);
    if (rc < 0) {
        printf("unable to set hw parameters: %s\n", snd_strerror(rc));
        exit(1);
    }


    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(playback_params, &frames, &dir);
    size = frames; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);

    while (true)
    {
        len = sizeof(cliaddr);
        rc = snd_pcm_readi(capture_handle, sendline, frames);
        if (rc == -EPIPE) {
            printf("capture underrun occurred\n");
            snd_pcm_prepare(capture_handle);
        } else if (rc < 0) {
            printf("error from readi: %s\n", snd_strerror(rc));
        }  else if (rc != (int)frames) {
            printf("short read, read %d frames\n", rc);
        }
        if (rc > 0) {
            n = recvfrom(sockfd, recvline, sizeof(recvline), 0, (struct sockaddr *)&cliaddr, &len);
            rc = snd_pcm_writei(playback_handle, recvline, frames);
            if (rc == -EPIPE) {
                snd_pcm_prepare(playback_handle);
            } else if (rc < 0) {
                printf("error from writei: %s\n", snd_strerror(rc));
            }  else if (rc != (int)frames) {
                printf("short write, write %d frames\n", rc);
            }
        }
        sendto(sockfd,sendline,sizeof(sendline),0,(struct sockaddr *)&cliaddr,sizeof(cliaddr));
    }
    snd_pcm_drain(playback_handle);
    snd_pcm_close(playback_handle);
    snd_pcm_drain(capture_handle);
    snd_pcm_close(capture_handle);
    free(buffer);
    return 0;
}
/**
 *  Starts udp Client
 */
void *udp_cli(void *inet_buffer) {
    unsigned int val;
    int sockfd, n, rc, size;
    snd_pcm_t *playback_handle, *capture_handle;
    snd_pcm_hw_params_t *playback_params, *capture_params;
    snd_pcm_sframes_t num_frames;
    int dir;
    snd_pcm_uframes_t frames;
    char *buffer, *tmp_buf;
    struct sockaddr_in servaddr, cliaddr;
    char sendline[1364];
    char recvline[1364];
    socklen_t len;

    sockfd=socket(AF_INET,SOCK_DGRAM,0);
    tmp_buf = (char *)inet_buffer;

    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(tmp_buf);
    servaddr.sin_port = htons(3001);

    /* Open PCM device for playback. */
    rc = snd_pcm_open(&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (rc < 0) {
        printf("unable to open pcm device: %s\n", snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&playback_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(playback_handle, playback_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(playback_handle, playback_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(playback_handle, playback_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(playback_handle, playback_params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 16000;
    snd_pcm_hw_params_set_rate_near(playback_handle, playback_params, &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(playback_handle, playback_params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(playback_handle, playback_params);
    if (rc < 0) {
        printf("unable to set hw parameters: %s\n", snd_strerror(rc));
        exit(1);
    }

    rc = snd_pcm_open(&capture_handle, "default", SND_PCM_STREAM_CAPTURE, 0);
    if (rc < 0) {
        printf("unable to open pcm device: %s\n", snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&capture_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(capture_handle, capture_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(capture_handle, capture_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(capture_handle, capture_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(capture_handle, capture_params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 16000;
    snd_pcm_hw_params_set_rate_near(capture_handle, capture_params, &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(capture_handle, capture_params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(capture_handle, capture_params);
    if (rc < 0) {
        printf("unable to set hw parameters: %s\n", snd_strerror(rc));
        exit(1);
    }


    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(playback_params, &frames, &dir);
    size = frames; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);

    printf("Entering Loop:\n");
    while (true)
    {
        len = sizeof(servaddr);
        rc = snd_pcm_readi(capture_handle, sendline, frames);
        printf("rc %d\n", rc);
        if (rc == -EPIPE) {
            printf("capture underrun occurred\n");
            snd_pcm_prepare(capture_handle);
        } else if (rc < 0) {
            printf("error from readi: %s\n", snd_strerror(rc));
        }  else if (rc != (int)frames) {
            printf("short read, read %d frames\n", rc);
        }
        if (rc != size) {
            printf("short read: read %d bytes\n", rc);
        }
        sendto(sockfd, sendline, sizeof(sendline),0, (struct sockaddr *)&servaddr, sizeof(servaddr));
        n=recvfrom(sockfd, recvline, sizeof(recvline),0,(struct sockaddr *)&servaddr, &len);
        rc = snd_pcm_writei(playback_handle, recvline, frames);
        if (rc == -EPIPE) {
            snd_pcm_prepare(playback_handle);
        } else if (rc < 0) {
            printf("error from writei: %s\n", snd_strerror(rc));
        }  else if (rc != (int)frames) {
            printf("short write, write %d frames\n", rc);
        }
    }
    snd_pcm_drain(playback_handle);
    snd_pcm_close(playback_handle);
    snd_pcm_drain(capture_handle);
    snd_pcm_close(capture_handle);
    free(buffer);
    return 0;
}

