#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <alsa/asoundlib.h>

/**
 *  Definitions
 */
#define ALSA_PCM_NEW_HW_PARAMS_API


int main(int argc, char**argv)
{
    int sockfd,n;
    long loops;
    int rc;
    int size;
    snd_pcm_t *playback_handle, *capture_handle;
    snd_pcm_hw_params_t *playback_params, *capture_params;
    unsigned int val;
    int dir;
    snd_pcm_uframes_t frames;
    char *buffer;
    struct sockaddr_in servaddr,cliaddr;
    char sendline[1364];
    char recvline[1364];
    socklen_t len;

    if (argc != 2)
    {
        printf("usage: ./udp_audio_client  <IP address>\n");
        exit(1);
    }

    sockfd=socket(AF_INET,SOCK_DGRAM,0);

    bzero(&servaddr,sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr=inet_addr(argv[1]);
    servaddr.sin_port=htons(3001);

    /* Open PCM device for playback. */
    rc = snd_pcm_open(&playback_handle, "default", SND_PCM_STREAM_PLAYBACK, 0);
    if (rc < 0) {
        fprintf(stderr,
                "unable to open pcm device: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&playback_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(playback_handle, playback_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(playback_handle, playback_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(playback_handle, playback_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(playback_handle, playback_params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 16000;
    snd_pcm_hw_params_set_rate_near(playback_handle, playback_params, &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(playback_handle, playback_params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(playback_handle, playback_params);
    if (rc < 0) {
        fprintf(stderr, "unable to set hw parameters: %s\n", snd_strerror(rc));
        exit(1);
    }

    rc = snd_pcm_open(&capture_handle, "default", SND_PCM_STREAM_CAPTURE, 0);
    if (rc < 0) {
        fprintf(stderr,
                "unable to open pcm device: %s\n",
                snd_strerror(rc));
        exit(1);
    }

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca(&capture_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any(capture_handle, capture_params);

    /* Set the desired hardware parameters. */

    /* Interleaved mode */
    snd_pcm_hw_params_set_access(capture_handle, capture_params, SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Signed 16-bit little-endian format */
    snd_pcm_hw_params_set_format(capture_handle, capture_params, SND_PCM_FORMAT_S16_LE);

    /* Two channels (stereo) */
    snd_pcm_hw_params_set_channels(capture_handle, capture_params, 1);

    /* 44100 bits/second sampling rate (CD quality) */
    val = 16000;
    snd_pcm_hw_params_set_rate_near(capture_handle, capture_params, &val, &dir);

    /* Set period size to 32 frames. */
    frames = 32;
    snd_pcm_hw_params_set_period_size_near(capture_handle, capture_params, &frames, &dir);

    /* Write the parameters to the driver */
    rc = snd_pcm_hw_params(capture_handle, capture_params);
    if (rc < 0) {
        printf( "unable to set hw parameters: %s\n", snd_strerror(rc));
        exit(1);
    }


    /* Use a buffer large enough to hold one period */
    snd_pcm_hw_params_get_period_size(playback_params, &frames, &dir);

    size = frames * 4; /* 2 bytes/sample, 2 channels */
    buffer = (char *) malloc(size);

    /* We want to loop for 5 seconds */
    snd_pcm_hw_params_get_period_time(playback_params, &val, &dir);
      /* EPIPE means overrun */
    /*
  while (true) {
    if (rc == -EPIPE) {
      fprintf(stderr, "overrun occurred\n");
      snd_pcm_prepare(handle);
    } else if (rc < 0) {
      fprintf(stderr,
              "error from read: %s\n",
              snd_strerror(rc));
    } else if (rc != (int)frames) {
      fprintf(stderr, "short read, read %d frames\n", rc);
    }
    rc = write(1, buffer, size);
    if (rc != size)
      fprintf(stderr,
              "short write: wrote %d bytes\n", rc);
  }
  */

    printf("Entering Loop:\n");
    printf("Buffer Size:\t%d\tFrames:\t%d\n", size, (int)frames);
    len = sizeof(servaddr);
    while ((rc = read(0, sendline, sizeof(sendline)))!=0)
    {
        if (rc != size) {
            printf("short read: read %d bytes\n", rc);
        }
        sendto(sockfd, sendline, sizeof(sendline),0, (struct sockaddr *)&servaddr, sizeof(servaddr));
        n=recvfrom(sockfd, recvline, sizeof(recvline),0,(struct sockaddr *)&servaddr, &len);
        /* EPIPE means underrun */
        /*
        if (strlen(buffer) >= size) {
            printf("Received %d bytes:\t%s\n", (int)strlen(buffer), buffer);
        } else {
            strcat(buffer, recvline);
        }
    */
        rc = snd_pcm_writei(playback_handle, recvline, frames);
        if (rc == -EPIPE) {
            fprintf(stderr, "underrun occurred\n");
            snd_pcm_prepare(playback_handle);
        } else if (rc < 0) {
            fprintf(stderr, "error from writei: %s\n", snd_strerror(rc));
        }  else if (rc != (int)frames) {
            fprintf(stderr, "short write, write %d frames\n", rc);
        }
        rc = snd_pcm_readi(capture_handle, buffer, frames);
        if (rc == -EPIPE) {
            fprintf(stderr, "capture underrun occurred\n");
            snd_pcm_prepare(capture_handle);
        } else if (rc < 0) {
            fprintf(stderr, "error from readi: %s\n", snd_strerror(rc));
        }  else if (rc != (int)frames) {
            fprintf(stderr, "short read, read %d frames\n", rc);
        }
    }
    printf("end of file on input\n");
    snd_pcm_drain(playback_handle);
    snd_pcm_close(playback_handle);
    snd_pcm_drain(capture_handle);
    snd_pcm_close(capture_handle);
    free(buffer);
    return 0;
}
