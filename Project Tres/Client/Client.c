/**
 * @author George Phillips, 16512561
 */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<pthread.h>
#include<ncurses.h>
#include<time.h>
#include<errno.h>

/**
 * Definitions
 */
#define true 1
#define false 0
#define CLIENT_MAX 1024
#define SERVER_MAX 2048
enum prototype {ICMP, DHCP};

/**
 * DataTypes
 */
typedef struct DHCPType_t {
  uint8_t flag;
  uint8_t CIADDR[4];
  uint8_t SIADDR[4];
  uint8_t CHADDR[6];
} DHCPType;

typedef struct ICMPType_t {
  uint8_t ttl;
  uint8_t type;
  uint8_t code;
  uint8_t src_addr[4];
  uint8_t dest_addr[4];
  char payload[SERVER_MAX];
} ICMPType;

typedef struct EthernetType_t{
  uint8_t src_mac[6];
  uint8_t dest_mac[6];
  enum prototype protocol;
  union {
    DHCPType dhcp_payload;
    ICMPType icmp_payload;
  } ether_payload;
} EthernetType;

/**
 * Function Declarations
 */
void close_windows();
void *server_send();
void *server_recv();
void process_frame(EthernetType *frame);
int send_frame (EthernetType *frame_out);
uint8_t *address_format(const char *char_addr);
void mac_gen(uint8_t mac_ref[6]);

/**
 * Global Variables
 */
uint8_t mac_addr[6];
int alive[] = {false, false};
int alive_count;
uint8_t *ip_addr, set_internal;
time_t time_seed;
int socket_comm;
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
struct sockaddr_in server;

int main(int argc, char *argv[])
{
  int send_message_len, i;
  uint8_t *internal_arg, *server_ip;
  char msg_helper[SERVER_MAX];
  pthread_t thread[2];
  EthernetType *lease_paquet;


  if (argc <2) {
    printf("<usage>: ./Client <Server I.P.> <Interal = 0/External = 1>\n");
    return;
  }
  srand((unsigned) time(&time_seed));

  if ((socket_comm = socket(AF_INET, SOCK_STREAM, 0))<0) {
    printf("failed to open a socket\n");
    return EXIT_FAILURE;
  }
  /*
   *  Server Initialization.
   */
  server.sin_addr.s_addr = inet_addr(argv[1]);
  server.sin_family = AF_INET;
  server.sin_port = htons(3000);

  if (connect(socket_comm, (struct sockaddr *)&server, sizeof(server))<0) {
    printf("Failed to connect to socket\n");
    return EXIT_FAILURE;
  }
  printf("Connected.\n");

  set_internal = atoi(argv[2]);
  if (!set_internal){
    printf("Retrieving I.P.:\n");
  }

  server_ip = address_format(argv[1]);
  mac_gen(mac_addr);

  /**
   * DHCP Request
   */


  lease_paquet = (EthernetType *) malloc(sizeof(EthernetType));
  lease_paquet->protocol = DHCP;
  lease_paquet->ether_payload.dhcp_payload.flag = set_internal;
  for(i = 0; i < 6; i++){
    lease_paquet->ether_payload.dhcp_payload.CHADDR[i] = mac_addr[i];
  }

  if(!set_internal)
      printf("DHCP discovery\n");
  if (send(socket_comm, (void *)lease_paquet, sizeof(EthernetType), 0) <= 0) {
    printf("Send failed.");
    return;
  }
  if(!set_internal)
      printf("DHCP Offer\n");

  if (recv(socket_comm, (void *)lease_paquet, sizeof(EthernetType), 0) <= 0) {
    printf("Failed to receive from server.");
    return;
  }
  ip_addr = (uint8_t *) malloc(sizeof(uint8_t)*4);
  for( i = 0; i < 4; i++){
    ip_addr[i] = lease_paquet->ether_payload.dhcp_payload.CIADDR[i];
  }
  printf("\n");
  lease_paquet->ether_payload.dhcp_payload.flag = 2;
  if(!set_internal)
      printf("DHCP Request\n");
  if (send(socket_comm, (void *)lease_paquet, sizeof(EthernetType), 0) <= 0) {
    printf("Send failed.");
    return;
  }

  if(!set_internal)
  printf("DHCP Acknowledgment\n");
  if (recv(socket_comm, lease_paquet, sizeof(EthernetType), 0) <= 0) {
    printf("Failed to receive from server.");
    return;
  }
  if (lease_paquet->ether_payload.dhcp_payload.flag == 3) {
    if(!set_internal){
      printf("DHCP complete\n");
    }
    printf("My I.P.:\t");
    for(i = 0; i < 3; i++){
        printf("%d.", ip_addr[i]);
    }
    printf("%d\n", ip_addr[4]);

    printf("My M.A.C:\t");
    for(i = 0; i < 5; i++){
        printf("%2x:", mac_addr[i]);
    }
    printf("%2x\n", mac_addr[i]);
  } else{
    printf("DHCP Failed\n");
    return;
  }

  /**
   *  Create 2 threads to handle send and receive of paquets
   */
  alive[0]=true;
  alive[1]=true;
  if (pthread_create(&(thread[0]), NULL, server_send, NULL) < 0) {
    alive[1] = false;
    alive[0] = false;
    printf("could not spawn child_thread(send) for client\n");
    alive[0]=false;
    return -1;
  }
  if ((pthread_create(&(thread[1]), NULL, server_recv, NULL) < 0) || alive[0] == false) {
    alive[1] = false;
    alive[0] = false;
    printf("could not spawn child_thread(recv) for client\n");
    return -1;
  }
  while (alive_count != 0) {
    alive_count = 0;
    for(i =0; i<2; i++){
      alive_count += alive[i];
    }
  }

  pthread_join(thread[0], NULL);
  pthread_join(thread[1], NULL);
  close(socket_comm);
  printf("Goodbye!\n");
  return EXIT_SUCCESS;
}


void *server_send () {
  char message_to_server[CLIENT_MAX];
  EthernetType *frame_out;
  uint8_t *int_dest;
  int i, j=0;
  while (alive[0]) {
    printf("Enter I.P.:");
    fgets (message_to_server, CLIENT_MAX, stdin);
    if (message_to_server != NULL) {
      if (strcasecmp(message_to_server, "/exit") == 0) {
        alive[0] = false;
      }
      int_dest = address_format(message_to_server);
      if (int_dest != NULL) {
        frame_out = (EthernetType *) malloc(sizeof(EthernetType));
        frame_out->protocol = ICMP;
        frame_out->ether_payload.icmp_payload.type = 8;
        frame_out->ether_payload.icmp_payload.code = 0;
        frame_out->ether_payload.icmp_payload.ttl = 16;
        for (i =0; i < 6; i++) {
          printf("mac_addr[%d]: %d\n", i, mac_addr[i]);
          frame_out->src_mac[i] = mac_addr[i];
        }
        frame_out->protocol = ICMP;
        for (i =0; i < 4; i++) {
          frame_out->ether_payload.icmp_payload.src_addr[i] = ip_addr[i];
          frame_out->ether_payload.icmp_payload.dest_addr[i] = int_dest[i];
        }
        printf("Enter a payload [a-z]\n");
        bzero(message_to_server, strlen(message_to_server));
        fgets(message_to_server, CLIENT_MAX, stdin);
        sprintf(frame_out->ether_payload.icmp_payload.payload,"%s",  message_to_server);
        if(strlen(message_to_server) > 0){
          printf("%s\n",frame_out->ether_payload.icmp_payload.payload);
        }
        if ( send(socket_comm, frame_out, sizeof(EthernetType), 0) < 0 ) {
          alive[0] = false;
        }
        j++;
        free(frame_out);
      } else {
        j++;

      }
    }
    bzero(message_to_server, strlen(message_to_server));
  }
}

void *server_recv () {
  int scr_line_num, client_message_len, i;
  char message_for_scr[COLS-21];
  EthernetType *frame_in;

  scr_line_num =1;
  while (alive[1]) {
    frame_in = (EthernetType *) malloc(sizeof(EthernetType));
    if (scr_line_num==LINES-4) {
      scr_line_num = 1;
    }
    bzero(message_for_scr, strlen(message_for_scr));
    if ((client_message_len = recv(socket_comm, (void *)frame_in, sizeof(EthernetType), 0)) < 0) {
      alive[0] = false;
      alive[1] = false;
      sprintf(message_for_scr, "Failed to recv %d", errno);
      free(frame_in);
      return;
    }
    if (client_message_len == 0) {
      alive[0]=false;

      alive[1]=false;
      free(frame_in);
      return;
    } else if (client_message_len == sizeof(EthernetType)) {
      if(frame_in->protocol == ICMP){
        process_frame(frame_in);
      }
      scr_line_num++;
    }
    bzero(message_for_scr, strlen(message_for_scr));
    free(frame_in);
  }
  alive[1] = false;
}

void process_frame(EthernetType *frame){
  int type, code, ttl, i;
  uint8_t int_swp;
  type = frame->ether_payload.icmp_payload.type;
  code = frame->ether_payload.icmp_payload.code;
  ttl = frame->ether_payload.icmp_payload.ttl;
  switch(type){
    case 3:
      if (code == 1) {
        printf("\n------------------ Destination host unreachable ------------------\n");
      } else if (code == 7) {
        printf("\n------------------ Destination host unknown ------------------\n");
      }
      break;
    case 8:
      if (code == 0) {
        printf("\n------------------ PING ------------------\n");
      }
      break;
    case 0:
      if (code == 0) {
        printf("\n------------------ PONG ------------------\n");
      }
      break;

  }
  printf("Type:\t%d\nCode:\t%d\nTime to live:\t%d\n", type, code, ttl);
  printf("Src M.A.C.: ");
  for(i = 0; i<5; i++){
    printf("%2x:", frame->src_mac[i]);
  }
  printf("%x\n", frame->src_mac[5]);
  printf("Src I.P.: ");
  for (i = 0; i < 3; i++) {
    printf("%d.", frame->ether_payload.icmp_payload.src_addr[i]);
  }
  printf("%d\n", frame->ether_payload.icmp_payload.src_addr[3]);
  printf("Dest M.A.C.: ");
  for(i = 0; i<5; i++){
    printf("%2x:", frame->dest_mac[i]);
  }
  printf("%x\n", frame->dest_mac[5]);
  printf("Dest I.P.: ");
  for (i = 0; i < 3; i++) {
    printf("%d.", frame->ether_payload.icmp_payload.dest_addr[i]);
  }
  printf("%d\n", frame->ether_payload.icmp_payload.dest_addr[3]);
  printf("\nPayload:\n");
  printf("%s\n", frame->ether_payload.icmp_payload.payload);
  if(type==8 && code == 0){
      frame->ether_payload.icmp_payload.type = 0;
      frame->ether_payload.icmp_payload.code = 0;
      for ( i = 0; i< 4; i++) {
          int_swp = frame->ether_payload.icmp_payload.src_addr[i];
          frame->ether_payload.icmp_payload.src_addr[i] = frame->ether_payload.icmp_payload.dest_addr[i];
          frame->ether_payload.icmp_payload.dest_addr[i] = int_swp;

          int_swp = frame->src_mac[i];
          frame->src_mac[i] = frame->dest_mac[i];
          frame->dest_mac[i] = int_swp;
      }
      for (i; i<6; i++) {
          int_swp = frame->src_mac[i];
          frame->src_mac[i] = frame->dest_mac[i];
          frame->dest_mac[i] = int_swp;
      }
      send_frame(frame);
  }
}

/**
 * Sends an ethernet frame to a client
 */
int send_frame (EthernetType *frame_out) {
  int ret_val;
  pthread_mutex_lock(&write_lock);
  ret_val = send(socket_comm, frame_out, sizeof(EthernetType), 0);
  pthread_mutex_unlock(&write_lock);
  return ret_val;
}

uint8_t *address_format(const char *char_addr) {
  uint8_t *ret_addr, i;
  char *token, *addr_in;
  const char delim[2] = ".";
  ret_addr = (uint8_t *) malloc(sizeof(uint8_t)*4);
  addr_in = strdup(char_addr);
  token = strtok(addr_in, delim);
  ret_addr[0] = atoi(token);
  for(i = 1 ; i<4; i++){
    token = strtok(NULL, delim);
    if(token != NULL){
      ret_addr[i] = atoi(token);
    } else {
      return NULL;
    }
  }
  return ret_addr;
}

void mac_gen(uint8_t mac_ref[6]) {
    int i;
    for(i = 0; i < 6; i++){
        mac_ref[i] = (rand() % 256);
    }
}
