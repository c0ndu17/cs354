/**
 * @author George Phillips, 16512561
 */
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h>   //threading
#include<errno.h>
#include<time.h>

/**
 * Definitions
 */
#define true 1
#define false 0
#define CLIENT_MAX 1024
#define SERVER_MAX 2048
enum prototype {ICMP, DHCP};

/**
 * DataTypes
 */

typedef struct ClientThread_t {
    uint8_t alive;
    uint8_t internal;
    int *sock_desc;
    pthread_t thread_id;
    uint8_t client_ip_addr[4];
    uint8_t client_mac_addr[6];
    struct ClientThread_t *next;
} ClientThreadType;

typedef struct NatElement_t {
    uint8_t referral_addr[4];
    time_t open_time;
} NatElement;

typedef struct ListNode_t {
    int8_t level;
    int16_t start;
    int16_t end;
    long int size;
    ClientThreadType *list_ref;
    NatElement *open_connection;
    struct ListNode_t *subdomain;
} ListNode;

typedef struct DHCPType_t {
    uint8_t flag; /* Designates Internality */
    uint8_t CIADDR[4];
    uint8_t SIADDR[4];
    uint8_t CHADDR[6];
} DHCPType;


typedef struct ICMPType_t {
    uint8_t ttl;
    uint8_t type;
    uint8_t code;
    uint8_t src_addr[4];
    uint8_t dest_addr[4];
    char payload[SERVER_MAX];
} ICMPType;


typedef struct EthernetType_t{
    uint8_t src_mac[6];
    uint8_t dest_mac[6];
    enum prototype protocol;
    union {
        DHCPType dhcp_payload;
        ICMPType icmp_payload;
    } ether_payload;
} EthernetType;


/**
  Function Declarations
  */

int server_logic (int client_sock, int client_count);
void *client_thread_start (void *client_sock_desc);
void process_frame (EthernetType *frame_in);
void send_error (ClientThreadType *local_ref, EthernetType *frame);
int send_frame (ClientThreadType *local_ref, EthernetType *frame_in);
ListNode *find_in_tree (uint8_t ip_addr[4]);
ClientThreadType *create_client (int socket_desc);
void add_ip (ClientThreadType *local_ref, uint8_t *internal_status);
void add_internal (ClientThreadType *local_ref);
void add_external (ClientThreadType *local_ref);
void add_to_domain(ClientThreadType *local_ref, ListNode *tmp_domain, uint8_t *give_domain);
int subnet_full(ListNode *list_in);
uint8_t random_subnet(ListNode *list_in);
int is_internal_addr(uint8_t *ip_addr);
uint8_t *address_format(const char *char_addr);
void mac_gen(uint8_t mac_ref[6]);
int cmp(uint8_t a[4], uint8_t b[4]);

/**
  Global Variables
  */

uint8_t host_ip[4];
int network_class;
time_t time_seed;
uint8_t host_mac[6];

ListNode *internal_root, *external_root;
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t recv_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t internal_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t external_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rand_lock = PTHREAD_MUTEX_INITIALIZER;
/**
 * Starts the server
 */
int main(int argc , char *argv[])
{
    int socket_desc, client_sock, client_desc, i, client_count = 1, backlog = 10, running_count = 0;
    uint8_t *tmp_ip;
    struct sockaddr_in server, client;
    ClientThreadType *tmp_client;
    ListNode tmp_node;
    /*
       pthread_mutex_init(&write_lock);
       pthread_mutex_init(&recv_lock);
       */
    if (argc <2) {
        printf("<usage>: ./Server <Host I.P.> <network class>\n\tNetwork classes:\n\t\tA = class A\n\t\tB = class B\n\t\tC = class C\n");
        return;
    }
    /* Sets and Saves the local */
    tmp_ip = address_format(argv[1]);
    for(i = 0; i < 4; i++){
        host_ip[i] = tmp_ip[i];
    }
    free(tmp_ip);
    if(strcasecmp(argv[2], "A") != 0 && strcasecmp(argv[2], "B") != 0 && strcasecmp(argv[2], "C") != 0) {
        printf("<usage>: ./Server <Host I.P.> <network class>\n\tNetwork classes:\n\t\tA = class A\n\t\tB = class B\n\t\tC = class C\n");
        return;
    }
    printf("Server starting\n");
    printf("Stimulating Transistors\n");
    printf("Set server I.P. address as %d.%d.%d.%d\n", host_ip[0], host_ip[1], host_ip[2], host_ip[3]);

    if ((socket_desc = socket(AF_INET , SOCK_STREAM , 0))<0) {
        printf("Failed to start server daemon socket\n");
        return EXIT_FAILURE;
    }
    printf("Server socket created\n" );

    /* Initialize random generator */
    srand((unsigned) time(&time_seed));

    /*Assign Local Mac*/
    mac_gen(host_mac);

    /*Set GV for network class */
    network_class = *(argv[2]) -65;
    printf("Network class %d\n", network_class);

    /*Root Node Initialization*/

    external_root = (ListNode *) malloc(sizeof(ListNode));
    internal_root = (ListNode *) malloc(sizeof(ListNode));
    if (external_root == NULL) {
        printf("Failed to allocate space for the external root node.\nExiting.");
        return EXIT_FAILURE;
    }
    external_root->subdomain = (ListNode *) malloc(sizeof(ListNode) * 256);
    external_root->size = 0;
    external_root->level = -2;
    external_root->start = 0;
    external_root->end = 255;

    if (internal_root == NULL){
        printf("Failed to allocate space for the internal root node.\nExiting.");
        return EXIT_FAILURE;
    }
    internal_root->subdomain = (ListNode *) malloc(sizeof(ListNode) * 256);
    internal_root->size = 0;
    internal_root->level = -2;
    internal_root->start = 0;
    internal_root->end = 255;

    /* Soul Eater*/

    for (i = 0; i < 256; i++) {
        external_root->subdomain[i].level = -1;
        external_root->subdomain[i].start = -1;
        external_root->subdomain[i].size = -1;
        external_root->subdomain[i].end = -1;
        internal_root->subdomain[i].level = -1;
        internal_root->subdomain[i].start = -1;
        internal_root->subdomain[i].size = -1;
        internal_root->subdomain[i].end = -1;
    }
    switch (network_class) {
        case 0:
            internal_root->subdomain[10].subdomain = (ListNode *) malloc(sizeof(ListNode) * 256);
            internal_root->subdomain[10].level = 0;
            internal_root->subdomain[10].size = 0;
            internal_root->subdomain[10].start = 0;
            internal_root->subdomain[10].end = 255;
            for (i = 0; i < 256; i++) {
                internal_root->subdomain[10].subdomain[i].level = -1;
                internal_root->subdomain[10].subdomain[i].size = -1;
                internal_root->subdomain[10].subdomain[i].start = -1;
                internal_root->subdomain[10].subdomain[i].end = -1;
            }
            break;

        case 1:

            internal_root->subdomain[172].subdomain = (ListNode *) malloc(sizeof(ListNode) * 256);
            internal_root->subdomain[172].level = 0;
            internal_root->subdomain[172].start = 16;
            internal_root->subdomain[172].end = 31;
            for ( i = 16; i< 32; i++){
                internal_root->subdomain[172].subdomain[i].level  = -1;
                internal_root->subdomain[172].subdomain[i].size    = -1;
                internal_root->subdomain[172].subdomain[i].start  = -1;
                internal_root->subdomain[172].subdomain[i].end    = -1;
            }
            break;

        case 2:

            internal_root->subdomain[192].subdomain = (ListNode *)malloc(sizeof(ListNode) * 256);
            for (i = 0; i < 256; i++) {
                internal_root->subdomain[192].subdomain[i].level  = -1;
                internal_root->subdomain[192].subdomain[i].start  = -1;
                internal_root->subdomain[192].subdomain[i].end    = -1;
            }
            internal_root->subdomain[192].level = 0;
            internal_root->subdomain[192].start = 168;
            internal_root->subdomain[192].end = 168;

            internal_root->subdomain[192].subdomain[168].subdomain = (ListNode *)malloc(sizeof(ListNode) * 256);
            for (i = 0; i < 256; i++) {
                internal_root->subdomain[192].subdomain[168].subdomain[i].level = -1;
                internal_root->subdomain[192].subdomain[168].subdomain[i].start = -1;
                internal_root->subdomain[192].subdomain[168].subdomain[i].end   = -1;
            }
            internal_root->subdomain[192].subdomain[168].level  = 1;
            internal_root->subdomain[192].subdomain[168].start  = 0;
            internal_root->subdomain[192].subdomain[168].end    = 255;
            break;
    }

    /* Prepare the sockaddr_in structure */
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 3000 );

    if( bind(socket_desc, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        printf("Failed to bind to socket\n");
        return EXIT_FAILURE;
    }
    printf("Bound to socket.\n");
    client_desc = sizeof(struct sockaddr_in);

    /* Listen */
    listen(socket_desc, backlog);


    printf("Waiting for incoming connections...\n");
    client_desc = sizeof(struct sockaddr_in);
    while ((client_sock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&client_desc))) {
        if (server_logic(client_sock, client_count++) < 0) {
            client_sock=-2;
            break;
        }
    }
    printf("Exited Accept\n");
    switch (client_sock) {
        case -1:
            printf("accept failed");
            return EXIT_FAILURE;
        case -2:
            printf("Server Error... exiting");
            return EXIT_FAILURE;
        default:
            break;
    }
    //TODO:Frees
    return EXIT_SUCCESS;
}

/**
 * Creates and detaches a client specific thread
 */

int server_logic(int client_sock, int client_count) {
    ClientThreadType *tmp_client = create_client(client_sock);
    if (pthread_create((void *)&(tmp_client), NULL, client_thread_start, (void *)tmp_client) < 0) {
        printf("could not spawn child_thread for client %d\n", client_count);
        return -1;
    }
    pthread_detach(tmp_client->thread_id);
    printf("client %d's thread spawned\n", client_count);
    return 0;
}


/**
 * Client thread handler: Spawned pthread process
 */
void *client_thread_start(void *client_desc) {
    int client_message_len, ret_check, i, j;
    uint8_t *tmp_ip, internal;
    time_t curr_time;
    char message[SERVER_MAX], client_message[CLIENT_MAX],*msg_helper;
    EthernetType *frame_in, *frame_out;
    ClientThreadType *local_ref = (ClientThreadType *)client_desc;
    ListNode *nat_check;

    if (local_ref == NULL) {
        printf("socket could not be found in list\n Exiting");
        return;
    }
    frame_in = (EthernetType *) malloc(sizeof(EthernetType));

    if ( (client_message_len = recv(*(local_ref->sock_desc), frame_in, sizeof(EthernetType), 0)) <= 0){
        return;
    }
    if (!internal) {
        printf("DHCPDiscover message\nLeasing...\n");
    }
    internal = frame_in->ether_payload.dhcp_payload.flag;
    for (i = 0; i < 6; i++){
        local_ref->client_mac_addr[i] = frame_in->ether_payload.dhcp_payload.CHADDR[i];
    }
    local_ref->internal = internal;

    add_ip(local_ref, &(internal));
    if (local_ref->client_ip_addr == NULL) {
        printf("No client IP\nExiting.\n");
        return;
    }
    for (i = 0; i < 4; i++) {
        frame_in->ether_payload.dhcp_payload.CIADDR[i] = local_ref->client_ip_addr[i];
    }
    printf("Allocated %d.%d.%d.%d\n", local_ref->client_ip_addr[0], local_ref->client_ip_addr[1], local_ref->client_ip_addr[2], local_ref->client_ip_addr[3]);
    if (send(*(local_ref->sock_desc), (void *)frame_in, sizeof(EthernetType), 0) <= 0) {
        printf("failed to send client their i.p closing the thread and connection to the client\n");
        return;
    }

    if ( (client_message_len = recv(*(local_ref->sock_desc), frame_in, sizeof(EthernetType), 0)) <= 0){
        return;
    }
    if(frame_in->ether_payload.dhcp_payload.flag ==2){
        frame_in->ether_payload.dhcp_payload.flag= 3;
    } else{
        printf("DHCP Failed\n");
        return;
    }
    if (send(*(local_ref->sock_desc), (void *)frame_in, sizeof(EthernetType), 0) <= 0) {
        printf("failed to send client their i.p closing the thread and connection to the client\n");
        return;
    }

    if (local_ref->client_ip_addr != NULL) {
        /* Client Loop: While Thread is alive loop*/
        while (local_ref->alive) {
            frame_in = (EthernetType *) malloc(sizeof(EthernetType));
            while ((client_message_len = recv(*(local_ref->sock_desc), frame_in, sizeof(EthernetType), 0)) < 0);
            if (client_message_len == 0){
                local_ref->alive = false;
                break;
            } if (client_message_len ==  sizeof(EthernetType)) {
                if(frame_in->protocol==ICMP){
                    nat_check = find_in_tree(local_ref->client_ip_addr);
                    if (nat_check != NULL) {
                        if (nat_check->open_connection != NULL){
                            curr_time = time(NULL);
                            if ( (curr_time - nat_check->open_connection->open_time) >= 600) {
                                free(nat_check->open_connection);
                            }
                        }
                    }
                    process_frame(frame_in);
                }
            }
            free(frame_in);
        }
    }
    if (client_message_len == 0){
        local_ref->alive=false;
        printf("Client Disconnected\n");
        fflush(stdout);
    } else if (client_message_len == -1) {
        printf("recv failed\n");
    }
}

ClientThreadType *create_client (int socket_desc) {
    int i;
    ClientThreadType *tmp_sock_thread;
    tmp_sock_thread = (ClientThreadType *) malloc(sizeof(ClientThreadType));
    tmp_sock_thread->sock_desc = (int *) malloc(sizeof(int));
    *(tmp_sock_thread->sock_desc) = socket_desc;
    for (i = 0; i < 4; i++) {
        tmp_sock_thread->client_ip_addr[i] = -1;
    }
    tmp_sock_thread->alive = true;
    return tmp_sock_thread;
}
/**
 * Sends an ethernet frame to a client
 */
void process_frame (EthernetType *frame_in) {
    uint8_t addr_in[4], addr_out[4], in_status, out_status, i, cmp_val;
    uint8_t external_send, internal_send;
    ListNode *src_client, *dest_client;
    EthernetType *frame_out;
    NatElement *nat_entry;

    frame_out = (EthernetType *) malloc(sizeof(EthernetType));

    for (i = 0; i < 4; i++) {
        addr_in[i] = frame_in->ether_payload.icmp_payload.src_addr[i];
        addr_out[i] = frame_in->ether_payload.icmp_payload.dest_addr[i];
        printf("addr_in[%d]\t%d\taddr_out[%d]\t%d\n", i, addr_in[i], i, addr_out[i],  in_status);
    }
    src_client = find_in_tree(addr_in);
    if (src_client == NULL) {
        printf("Client does not exist.\n");
        free(frame_out);
        return;
    }
    if (src_client->list_ref == NULL) {
        printf("No Client Details at I.P\n");
        free(frame_out);
        return;
    }
    cmp_val = cmp(addr_out, host_ip);
    if(!cmp_val){
        dest_client = find_in_tree(addr_out);
        if (dest_client == NULL) {
            out_status = 1;
        } else if (dest_client->list_ref == NULL) {
            frame_in->ether_payload.icmp_payload.type = 3;
            frame_in->ether_payload.icmp_payload.code = 1;

            /* Send new frame */
            external_send = send_frame(src_client->list_ref, frame_in);
            free(frame_out);
            return;
        } else out_status = dest_client->list_ref->internal;
    } else {
        out_status = 0;
    }
    in_status = src_client->list_ref->internal;

    if (frame_out->ether_payload.icmp_payload.ttl = frame_in->ether_payload.icmp_payload.ttl == 0 ) {
        printf("Time to live for packet diminished\n");
        free(frame_out);
        return;
    }

    switch (in_status) {
        case 0:
            if (out_status == 0) {
                //SEND TO CLIENT WITHOUT MODIFCIATION
                /* Packet creation routine*/
                printf("Internal->Internal\n");
                for (i =0; i < 6; i++ ){
                    frame_out->src_mac[i] = frame_in->src_mac[i];
                    frame_out->dest_mac[i] = dest_client->list_ref->client_mac_addr[i];
                }
                frame_out->protocol = frame_in->protocol;
                frame_out->ether_payload.icmp_payload.ttl = frame_in->ether_payload.icmp_payload.ttl-1;
                frame_out->ether_payload.icmp_payload.type = frame_in->ether_payload.icmp_payload.type;
                frame_out->ether_payload.icmp_payload.code = frame_in->ether_payload.icmp_payload.code;

                for (i=0; i < 4;i++){
                    frame_out->ether_payload.icmp_payload.src_addr[i] = addr_in[i];
                    frame_out->ether_payload.icmp_payload.dest_addr[i] = addr_out[i];
                }
                sprintf(frame_out->ether_payload.icmp_payload.payload, "%s", frame_in->ether_payload.icmp_payload.payload);
                external_send = send_frame(dest_client->list_ref, frame_out);

                if (external_send == -1) {
                    //TODO: SEND Error Paquet
                    frame_in->ether_payload.icmp_payload.type = 3;
                    frame_in->ether_payload.icmp_payload.code = 1;

                    /* Send new frame */
                    external_send = send_frame(src_client->list_ref, frame_in);
                }
            } else {
                /* MODIFY, ADD TO NAT TABLE && SEND */
                printf("Internal->External\n");

                /* Add Nat Entry to Tree*/
                nat_entry = (NatElement *) malloc(sizeof(NatElement));
                for (i = 0; i < 4; i++) {
                    nat_entry->referral_addr[i] = addr_in[i];
                }
                nat_entry->open_time = time(NULL);

                /* Overwrite existing entry if present*/
                if (dest_client->open_connection != NULL) {
                    free(dest_client->open_connection);
                }
                dest_client->open_connection = nat_entry;

                /* Packet Creation && Modification routines*/

                for (i = 0; i < 5; i++){
                    printf("%x:", host_mac[i]);
                }
                printf("%x\n", frame_out->src_mac[5]);
                for (i = 0; i < 6; i++){
                    frame_out->src_mac[i] = host_mac[i];
                    frame_out->dest_mac[i] = dest_client->list_ref->client_mac_addr[i];
                }
                for (i = 0; i < 5; i++){
                    printf("%x:", frame_out->src_mac[5]);
                }
                printf("%x\n", host_mac[i]);
                frame_out->protocol = frame_in->protocol;
                frame_out->ether_payload.icmp_payload.ttl = frame_in->ether_payload.icmp_payload.ttl-1;
                frame_out->ether_payload.icmp_payload.type = frame_in->ether_payload.icmp_payload.type;
                frame_out->ether_payload.icmp_payload.code = frame_in->ether_payload.icmp_payload.code;
                sprintf(frame_out->ether_payload.icmp_payload.payload, "%s", frame_in->ether_payload.icmp_payload.payload);
                for (i = 0; i < 4; i++) {
                    frame_out->ether_payload.icmp_payload.src_addr[i] = host_ip[i];
                    frame_out->ether_payload.icmp_payload.dest_addr[i] = addr_out[i];
                }

                /* Send new frame */
                external_send = send_frame(dest_client->list_ref, frame_out);
                if (external_send == -1) {
                    frame_in->ether_payload.icmp_payload.type = 3;
                    frame_in->ether_payload.icmp_payload.code = 1;
                    frame_in->ether_payload.icmp_payload.ttl = frame_in->ether_payload.icmp_payload.ttl-1;

                    /* Send new frame */
                    external_send = send_frame(src_client->list_ref, frame_in);
                    return;
                }
            }
            break;
        case 1:
            //CHECK NAT TABLE AND SEND
            switch (out_status) {
                case 0:
                    printf("External->Internal\n");
                    if (src_client->open_connection == NULL) {
                        frame_in->ether_payload.icmp_payload.type = 3;
                        frame_in->ether_payload.icmp_payload.code = 1;

                        /* Send new frame */
                        external_send = send_frame(src_client->list_ref, frame_in);
                    } else {
                        /* Packet Creation && Modification routines*/
                        dest_client = find_in_tree(src_client->open_connection->referral_addr);
                        for (i = 0; i < 6; i++ ) {
                            frame_out->src_mac[i] = frame_in->src_mac[i];
                            frame_out->dest_mac[i] = dest_client->list_ref->client_mac_addr[i];
                        }
                        frame_out->protocol = frame_in->protocol;
                        frame_out->ether_payload.icmp_payload.ttl = (frame_in->ether_payload.icmp_payload.ttl)-1;
                        frame_out->ether_payload.icmp_payload.type = frame_in->ether_payload.icmp_payload.type;
                        frame_out->ether_payload.icmp_payload.code = frame_in->ether_payload.icmp_payload.code;

                        if (frame_in->ether_payload.icmp_payload.payload[0] != 0) {
                            sprintf(frame_out->ether_payload.icmp_payload.payload, "%s", frame_in->ether_payload.icmp_payload.payload);
                        }
                        for (i = 0; i < 4; i++) {
                            frame_out->ether_payload.icmp_payload.src_addr[i]   = frame_in->ether_payload.icmp_payload.src_addr[i];
                            frame_out->ether_payload.icmp_payload.dest_addr[i]  = dest_client->list_ref->client_ip_addr[i];
                        }

                        /* Send new frame */
                        external_send = send_frame(dest_client->list_ref, frame_out);
                        if (external_send == -1) {
                            frame_in->ether_payload.icmp_payload.type = 3;
                            frame_in->ether_payload.icmp_payload.code = 1;

                            /* Send new frame */
                            external_send = send_frame(src_client->list_ref, frame_in);
                            return;
                        }
                    }
                    break;

                case 1:
                    printf("External->External\n");
                    frame_in->ether_payload.icmp_payload.type = 3;
                    frame_in->ether_payload.icmp_payload.code = 7;

                    /* Send new frame */
                    external_send = send_frame(src_client->list_ref, frame_in);
                    break;
            }
    }
    if(frame_out != NULL)
        free(frame_out);
}

/**
 * Sends an ethernet frame to a client
 */
int send_frame (ClientThreadType *local_ref, EthernetType *frame_out) {
    int ret_val;
    pthread_mutex_lock(&write_lock);
    ret_val = send(*(local_ref->sock_desc), frame_out, sizeof(EthernetType), 0);
    pthread_mutex_unlock(&write_lock);
    return ret_val;
}

/**
 * Find a reference of an ip in the internal or external list of clients
 */
ListNode *find_in_tree(uint8_t ref_ip[4]) {
    int i, in_status;
    ListNode *tmp_domain;
    in_status = is_internal_addr(ref_ip);
    switch (in_status){
        case 0:
            pthread_mutex_lock(&external_lock);
            if(external_root == NULL || ref_ip == NULL){
                pthread_mutex_unlock(&external_lock);
                return NULL;
            }
            tmp_domain = external_root;
            if (tmp_domain->level == -1){
                pthread_mutex_unlock(&external_lock);
                return NULL;
            }
            i = 0;
            while (tmp_domain->level != 3) {
                if (&(tmp_domain->subdomain[ref_ip[i]]) == NULL) {
                    printf("subdomain uninitialized\n");
                    return NULL;
                }
                if (tmp_domain->subdomain[ref_ip[i]].level == -1) {
                    printf("level not set for subdomain %d\n", ref_ip[i]);
                    return NULL;
                }
                tmp_domain = &(tmp_domain->subdomain[ref_ip[i]]);
                i++;
            }
            if (tmp_domain->list_ref == NULL){
                pthread_mutex_unlock(&external_lock);
                return NULL;
            }
            pthread_mutex_unlock(&external_lock);
            break;
        default:
            pthread_mutex_lock(&internal_lock);
            if (internal_root == NULL || ref_ip == NULL) {
                pthread_mutex_unlock(&internal_lock);
                return NULL;
            }
            tmp_domain = internal_root;
            i = 0;
            while (tmp_domain->level != 3) {
                if (&(tmp_domain->subdomain[ref_ip[i]]) == NULL) {
                    printf("subdomain uninitialized\n");
                    pthread_mutex_unlock(&internal_lock);
                    return NULL;
                }
                if (tmp_domain->subdomain[ref_ip[i]].level == -1) {
                    printf("level not set for subdomain %d\n", ref_ip[i]);
                    pthread_mutex_unlock(&internal_lock);
                    return NULL;
                }
                tmp_domain = &(tmp_domain->subdomain[ref_ip[i]]);
                i++;
            }
            if (tmp_domain->list_ref == NULL){
                pthread_mutex_unlock(&internal_lock);
                return NULL;
            }
            pthread_mutex_unlock(&internal_lock);
            break;
    }
    return tmp_domain;
}


/*
 * Add a Client to the list of designated I.P.'s
 */
void add_ip (ClientThreadType *local_ref, uint8_t *internal_status) {
    int internal_switch;
    internal_switch = *(internal_status);
    switch (internal_switch){
        case false:
            add_internal(local_ref);
            break;
        case true:
            add_external(local_ref);
            break;
        default:
            printf("Could not determine internal status of client.\nExiting.\n");
            exit(0);
    }
    return;
}

/*
 * Add a Client to the list of internal clients
 */

void add_internal (ClientThreadType *local_ref) {
    ListNode *tmp_domain;
    pthread_mutex_lock(&internal_lock);
    switch (network_class) {
        case 0:
            local_ref->client_ip_addr[0] = 10;
            add_to_domain(local_ref, internal_root, &(local_ref->client_ip_addr[0]));
            break;
        case 1:
            local_ref->client_ip_addr[0] = 172;
            add_to_domain(local_ref, internal_root, &(local_ref->client_ip_addr[0]));
            break;
        case 2:
            local_ref->client_ip_addr[0] = 192;
            local_ref->client_ip_addr[1] = 168;
            tmp_domain = &(internal_root->subdomain[192]);
            add_to_domain(local_ref, tmp_domain, &(local_ref->client_ip_addr[1]));
            break;
        default:
            printf("network class not set.\n Exiting.\n");
            exit(0);
    }
    pthread_mutex_unlock(&internal_lock);
}

/**
 *  Add a client to the list of external clients.
 */

void add_external (ClientThreadType *local_ref) {
    ListNode *tmp_domain;
    pthread_mutex_lock(&external_lock);
    tmp_domain = external_root;
    if( tmp_domain == NULL ) {
        printf("External list unintialized");
        exit(0);
    }
    add_to_domain(local_ref, tmp_domain, NULL);
    pthread_mutex_unlock(&external_lock);
}

/**
 *  finds an I.P for the client;
 */
void add_to_domain(ClientThreadType *local_ref, ListNode *tmp_domain, uint8_t *given_domain) {
    int net_count, i;
    uint8_t rand_net;
    int valid;
    net_count = 0;
    valid = false;
    if( tmp_domain == NULL ) {
        printf("subnet unintialized");
        exit(0);
    } else if (tmp_domain->level == 3) {
        if (tmp_domain->list_ref != NULL) {
            printf("I.P conflict.\n");
            return;
        } else {
            tmp_domain->list_ref = local_ref;
        }
        return;
    }
    /**
     *  Find a subnet
     */
    if (given_domain!=NULL) {
        rand_net = *(given_domain);
    } else {
        while (!valid) {
            rand_net = random_subnet(tmp_domain);
            valid = false;
            if (rand_net != -1) {
                valid = true;
                if (local_ref->internal == false) {
                    if (tmp_domain->level == 0 && rand_net == 10) {
                        valid = false;
                    } else if (tmp_domain->level == 1 && local_ref->client_ip_addr[0] == 172) {
                        if (rand_net >= 16 && rand_net < 32) {
                            valid = false;
                        }
                    } else if (tmp_domain->level == 1 && local_ref->client_ip_addr[0] == 192){
                        if (rand_net == 168 ) {
                            valid = false;
                        }
                    }
                }
            }
        }
    }

    if (tmp_domain->level == -2) {
        tmp_domain->subdomain[rand_net].level = (tmp_domain->level) + 2;
    } else tmp_domain->subdomain[rand_net].level = (tmp_domain->level) + 1;


    /**
     *  Allocate memory for the subdomain.
     */
    if (tmp_domain->subdomain[rand_net].subdomain == NULL && tmp_domain->level < 2) {
        tmp_domain->subdomain[rand_net].subdomain = (ListNode *) malloc(sizeof(ListNode)*256);
        tmp_domain->subdomain[rand_net].size = 0;
        tmp_domain->subdomain[rand_net].start = 0;
        tmp_domain->subdomain[rand_net].end = 255;
        for (i = tmp_domain->start; i < (tmp_domain->end +1); i++) {
            tmp_domain->subdomain[rand_net].subdomain[i].level = -1;
            tmp_domain->subdomain[rand_net].subdomain[i].size = -1;
            tmp_domain->subdomain[rand_net].subdomain[i].start = -1;
            tmp_domain->subdomain[rand_net].subdomain[i].end = -1;
        }
    }

    tmp_domain->subdomain[rand_net].size += 1;
    local_ref->client_ip_addr[tmp_domain->subdomain[rand_net].level] = rand_net;
    add_to_domain(local_ref, &(tmp_domain->subdomain[rand_net]), NULL);
}

/**
 *  Determines if subnets of the list element are full.
 */
int subnet_full(ListNode *list_in) {
    int i;
    if (list_in->level == -1) {
        return false;
    }
    if (list_in->level == 3) {
        return true;
    }
    if(list_in->subdomain==NULL){
        return false;
    }
    for (i = list_in->start; i < (list_in->end+1); i++) {
        if ( !subnet_full(&(list_in->subdomain[i])) ) {
            return false;
        }
    }
    return true;
}

uint8_t random_subnet(ListNode *list_in) {
    int i;
    uint8_t ret_val;
    int found;
    found = false;
    pthread_mutex_lock(&rand_lock);
    if (subnet_full(list_in)) {
        pthread_mutex_unlock(&rand_lock);
        return -1;
    }
    if (list_in->level == 2) {
        if (list_in->subdomain == NULL) {
            printf("subdomain not allocated\n");
            pthread_mutex_unlock(&rand_lock);
            return -1;
        }
        for (i = (list_in->start); i < (list_in->end +1); i++) {
            if (!subnet_full(&(list_in->subdomain[i]))) {
                pthread_mutex_unlock(&rand_lock);
                return i;
            }
        }
        return -1;
    }
    while (!found) {
        ret_val = rand() % ((list_in->end +1) - list_in->start);
        ret_val += list_in->start;
        if (!subnet_full(&(list_in->subdomain[ret_val]))) {
            found = true;
        }
    }
    pthread_mutex_unlock(&rand_lock);
    return ret_val;
}


/**
 *  determines the whether and if so the type of address, Internal/External.
 */
int is_internal_addr(uint8_t *ip_addr) {
    switch (ip_addr[0]) {
        case 10:
            if (((ip_addr[1] >= 0 && ip_addr[1] < 256) && (ip_addr[2] >= 0 && ip_addr[2] < 256)) && (ip_addr[3] > -1 && ip_addr[3] < 256)) {
                return 1;
            }
            break;
        case 172:
            if ( ip_addr[1] >= 16 && ip_addr[1] < 32 ) {
                if (((ip_addr[2] >= 0 && ip_addr[2] < 256) && (ip_addr[3] >= 0 && ip_addr[3] < 256))) {
                    return 2;
                }
            } else return false;
            break;
        case 192:
            if(ip_addr[1]==168){
                if ((ip_addr[2] >= 0 && ip_addr[2] < 256) && (ip_addr[3] >= 0 && ip_addr[3] < 256)) {
                    return 3;
                }
            } else return false;
            break;
    }
    return false;
}

uint8_t *address_format(const char *char_addr) {
    uint8_t *ret_addr, i;
    char *token, *addr_in;
    const char delim[2] = ".";
    ret_addr = (uint8_t *) malloc(sizeof(uint8_t)*4);
    addr_in = strdup(char_addr);
    token = strtok(addr_in, delim);
    ret_addr[0] = atoi(token);
    for(i = 1 ; i<4; i++){
        token = strtok(NULL, delim);
        if(token != NULL){
            ret_addr[i] = atoi(token);
        }
    }
    return ret_addr;
}

/**
 * generates a random mac address
 */
void mac_gen(uint8_t mac_ref[6]) {
    int i;
    for(i = 0; i< 6; i++){
        mac_ref[i] = (rand() % 255);
        mac_ref[i] += 1;
    }
}

/**
 * Compares two I.P addreses, returning a true if they are equal.
 */
int cmp(uint8_t a[4], uint8_t b[4]){
    int i;
    for (i = 0; i < 4; i++) {
        if (a[i] != b[i])
            return false;
    }
    return true;
}
