#include<stdio.h>
#include<string.h>
#include<regex.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h>   //threading
#include<errno.h>

/**
 *  Definitions
 */
typedef int bool;
#define true 1
#define false 0

#define Message_MAX 2048
#define Transfer_MAX 5
#define SOCKET_ERROR -1

/**
 *  Data Types
 */

typedef struct MessageType_t {
    int flag;
    unsigned char buffer[Message_MAX];
} MessageType;


int main(int argc, char **argv){
    int sockfd, connfd, n, is_paused, read_size, write_size;
    long long int file_size;
    struct sockaddr_in myaddr, destaddr;
    char *msg_helper;
    socklen_t destlen;
    char buffer[Message_MAX];
    FILE *src_file;

    sockfd = socket(AF_INET,SOCK_STREAM,0);
    printf("usage: ./send_exec <file path>\n");

    bzero(&myaddr,sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    myaddr.sin_port = htons(3001);
    if (bind(sockfd,(struct sockaddr *)&myaddr,sizeof(myaddr))!=0){
        printf("failed to bind\n");
        return;
    }
    printf("Bound!\n");
    listen(sockfd, 10);
    destlen = sizeof(struct sockaddr_in);
    connfd = accept(sockfd, (struct sockaddr *)&destaddr, (socklen_t *)&destlen);
    printf("accepted\n");

    if((src_file = fopen(argv[1], "r"))==NULL){
        printf("FAILED TO OPEN FILE\n");
    }

    bzero((void *)&buffer, sizeof(buffer));
    sprintf(buffer, "%s", argv[1]);
    printf("%s\n", buffer);
    read_size = write(connfd, (void *) &buffer, sizeof(buffer));
    if (read_size <= 0) {
        return;
    }
    bzero((void *)&buffer, sizeof(buffer));
    fseek(src_file, 0L, SEEK_END);
    file_size = htonl(ftell(src_file));
    sprintf(buffer, "%lld", file_size);
    printf("%s\n", buffer);
    fseek(src_file, 0L, SEEK_SET);
    read_size = write(connfd, (void *) &buffer, sizeof(buffer));
    if (read_size <= 0) {
        return;
    }
    while (!feof(src_file)) {
        bzero((void *)&buffer, sizeof(buffer));
        read_size = fread(buffer, 1, sizeof(buffer), src_file);
        if (read_size <= 0)
            error("ERROR reading file");


        msg_helper = buffer;
        while (read_size > 0)
        {
            write_size = write(connfd, (void *)&buffer, sizeof(buffer));
            if (write_size == SOCKET_ERROR)
            {
                if ( errno != EWOULDBLOCK){
                    printf("Strerror: %s\n", strerror(errno));
                    errno = 0;
                }

                fd_set writefd;
                FD_ZERO(&writefd);
                FD_SET(sockfd, &writefd);

                if (select(0, NULL, &writefd, NULL, NULL) != 1)
                    error("ERROR waiting to write to socket");

                continue;
            }
            if (write_size == 0)
                printf("DISCONNECTED writing to socket\n");

            msg_helper += write_size;
            read_size -= write_size;
        }
    }
    printf("DONE!\n");
    fclose(src_file);
    close(connfd);
    close (sockfd);
}

/*
 *
while (!feof(fp))
{
     int nRead = fread(bufferin, sizeof(char), 256, fp);
     if (nRead <= 0)
         error("ERROR reading file");

     char *pBuf = bufferin;
     while (nRead > 0)
     {
         int nSent = send(remsock, pBuf, nRead, 0);
         if (nSent == SOCKET_ERROR)
         {
             if (WSAGetLastError() != WSAEWOULDBLOCK)
                 error("ERROR writing to socket");

             fd_set writefd;
             FD_ZERO(&writefd);
             FD_SET(remsock, &writefd);

             if (select(0, NULL, &writefd, NULL, NULL) != 1)
                 error("ERROR waiting to write to socket");

             continue;
         }

         if (nSent == 0)
             error("DISCONNECTED writing to socket");

         pBuf += nSent;
         nRead -= nSent;
     }
     }

 closesocket(remsock);

 */
