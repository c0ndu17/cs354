#include <stdio.h>
#include <string.h>

#define MIN(a,b) (((a)<(b))?(a):(b))

int distance(char *a, char *b);

int distance(char *a, char *b) {
    //TODO: Implement Lowercase conversion
    int i, j, a_len, b_len, nw, cj;
    i = 0;
    a_len = strlen(a);
    b_len = strlen(b);
    int costs[b_len + 1];
    for (j = 0; j < b_len + 1; j++)
        costs[j] = j;
    for (i = 1; i <= a_len; i++) {
        costs[0] = i;
        nw = i - 1;
        for (j = 1; j <= b_len; j++) {
            cj = MIN(1 + MIN(costs[j], costs[j - 1]), a[i - 1] == b[j - 1] ? nw : nw + 1);
            nw = costs[j];
            costs[j] = cj;
        }
    }
    return costs[b_len];
}

int main(int argc, char **argv)
{
    int client_message_len, diff;
    int lev_distance;
    double max, min, threshold;
    if (argc<3) return 0;


    /*Threshold set at 1/3 or 0.3333333333332 */
    diff = (strlen(argv[1])-strlen(argv[2]));
    if (diff > 0) {
        max = strlen(argv[1]);
        min = strlen(argv[2]);
    } else {
        max = strlen(argv[2]);
        min = strlen(argv[1]);
    }
    if (diff < 0) diff = diff * -1;
    lev_distance = distance(argv[1], argv[2]);
    printf("distance between `%s' and `%s': %d\n", argv[1], argv[2], lev_distance);
    printf("After diff: %d\n", lev_distance-diff);
    printf("After division by %.4f: %.4f\n", min, (lev_distance-diff)/min);
    if(strlen(argv[1])<=2)
        threshold=0.0;
    else threshold = 0.4;
    if ((((lev_distance - diff)/min) <= threshold)) {
        printf("%s\n", argv[2]);
    }
    return 0;
}
