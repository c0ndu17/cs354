#include<stdio.h>
#include<string.h>
#include<regex.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h>   //threading
#include<errno.h>

/**
 *  Definitions
 */
typedef int bool;
#define true 1
#define false 0

#define Message_MAX 2048
#define Transfer_MAX 5
#define SOCKET_ERROR -1

/**
 *  Data Types
 */

typedef struct MessageType_t {
    int flag;
    unsigned char buffer[Message_MAX];
} MessageType;


int main(int argc, char **argv){
    int sockfd, n, is_paused, write_size, msg_size;
    long long int file_size;
    struct sockaddr_in myaddr, srcaddr;
    FILE *dest_file;
    char buffer[Message_MAX];
    char *msg_helper;
    socklen_t srclen;

    if ((sockfd=socket(AF_INET,SOCK_STREAM,0))<0){
        printf("Failed to open socket\n");
        return;
    }
    printf("usage: ./recv_exec <I.P>\n");

    bzero(&srcaddr,sizeof(srcaddr));
    srcaddr.sin_family = AF_INET;
    srcaddr.sin_addr.s_addr = inet_addr(argv[1]);
    srcaddr.sin_port = htons(3001);

    if ((connect(sockfd, (struct sockaddr *)&srcaddr, sizeof(srcaddr)))<0){
        printf("failed to connect\n");
        return;
    }
    printf("Connected.\n");

    bzero((void *) &buffer, sizeof(buffer));
    msg_size = read(sockfd, (void *)&buffer, sizeof(buffer));
    printf("File:\t%s\n", buffer);
    if((dest_file = fopen(buffer, "w")) == NULL){
        printf("FAILED TO OPEN FILE\n");
    }
    bzero((void *) &buffer, sizeof(buffer));
    msg_size = read(sockfd, (void *)&buffer, sizeof(buffer));
    file_size = atol(buffer);
    file_size = ntohl(file_size);
    printf("File size: %lld\n", file_size);
    is_paused = 0;
    while (file_size > 0) {
        bzero((void *) &buffer, sizeof(buffer));
        msg_size = recv(sockfd, (void *) &buffer, sizeof(buffer), 0);
        if (msg_size == SOCKET_ERROR) {
            if (errno != EWOULDBLOCK) {
                printf("Strerror: %s\n", strerror(errno));
                errno = 0;
            }
            fd_set readfd;
            FD_ZERO(&readfd);
            FD_SET(sockfd, &readfd);
            if (select(0, &readfd, NULL, NULL, NULL) != 1){
                printf("ERROR waiting for data from socket\n%s\n", strerror(errno));
            }
            continue;
        }
        if (msg_size == 0) {
            printf("Disconnected\n");
            break;
        }
        msg_helper = buffer;
        while (msg_size > 0 && file_size > 0)
        {
            if (msg_size > file_size)
                 write_size = fwrite(msg_helper, 1, file_size, dest_file);
            else write_size = fwrite(msg_helper, 1, msg_size, dest_file);
            if (write_size <= 0)
                printf("ERROR writing to file\n%s\n", strerror(errno));
            msg_helper += write_size;
            msg_size -= write_size;
            file_size-= write_size;
        }
    }
    printf("Exiting\n");
    fclose(dest_file);
}
