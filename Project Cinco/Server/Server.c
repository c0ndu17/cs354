/**
 *  @author George Phillips, 16512561
 *
 */
#include<stdio.h>
#include<string.h>
#include<regex.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h> //inet_addr
#include<unistd.h>    //write
#include<pthread.h>   //threading
#include<errno.h>

/**
 *  Definitions
 */
typedef int bool;
#define true 1
#define false 0

#define Message_MAX 2048
#define Transfer_MAX 5

/**
 *  Data Types
 */

typedef struct MessageType_t {
    int flag;
    char buffer[Message_MAX];
} MessageType;

typedef struct ClientThreadType_t {
    bool alive;
    pthread_mutex_t in_call;
    char *client_ip;
    int *sock_desc;
    pthread_t thread_id;
    char *username;
    int *file_req_socket;
    pthread_mutex_t req_lock;
    struct CommType_t **transfers;
    char *search_query;
    struct ClientThreadType_t *next;
} ClientThreadType;

typedef struct CommType_t {
    ClientThreadType *downloader_ref;
    ClientThreadType *uploader_ref;
    int port;
} CommType;


/**
 * Function Declarations
 */
int server_logic (int client_sock, int client_count);
void *client_thread_start (void *client_sock_desc);
CommType *create_comm (ClientThreadType *downloader, ClientThreadType *uploader);
void user_retrieve(char *ret_str, int placeholder);
void remove_user(int *socket_desc);
int recv_msg(int *socket_desc, MessageType *message);
int send_to_user (char *username, MessageType *message);
int send_msg(int *socket_desc, MessageType *message);
int broadcast_msg (MessageType *message_to_broadcast, int *local_desc);
ClientThreadType *add_user (int socket_desc);
void add_username (char *username, ClientThreadType *local_ref);
ClientThreadType *user_to_ref (char *username);
int user_check (char *username);

/**
 * Global Variables
 */
ClientThreadType *client_list;
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t user_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t recv_lock = PTHREAD_MUTEX_INITIALIZER;

int main(int argc , char *argv[])
{
    int tcp_sock_desc, client_sock, client_desc, client_count, backlog;
    struct sockaddr_in tcp_server, client;
    ClientThreadType *tmp_client;
    printf("Server starting\n");
    printf("Stimulating Transistors\n");


    if ((tcp_sock_desc = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("Failed to start server daemon socket\n");
        return EXIT_FAILURE;
    }
    printf("TCP server socket created\n" );

    //Prepare the sockaddr_in structure
    tcp_server.sin_family = AF_INET;
    tcp_server.sin_addr.s_addr = INADDR_ANY;
    tcp_server.sin_port = htons( 3000 );

    if( bind(tcp_sock_desc,(struct sockaddr *)&tcp_server , sizeof(tcp_server)) < 0)
    {
        //print the error message
        printf("Failed to bind to socket\n");
        return EXIT_FAILURE;
    }
    printf("Bound to TCP socket.\n");

    //Listen
    backlog = 10;
    listen(tcp_sock_desc, backlog);

    client_desc = sizeof(struct sockaddr_in);
    client_count =1;

    printf("Waiting for incoming connections...\n");
    while ( (client_sock = accept(tcp_sock_desc, (struct sockaddr *)&client, (socklen_t*)&client_desc))) {
        if (server_logic(client_sock, client_count++) <0) {
            client_sock=-2;
            break;
        }
    }
    switch (client_sock) {
        case -1:
            printf("accept failed");
            return EXIT_FAILURE;
        case -2:
            printf("Server Error... exiting");
            return EXIT_FAILURE;
        default:
            break;
    }
    while(client_list!=NULL){
        tmp_client = client_list;
        client_list = client_list->next;
        free(tmp_client->sock_desc);
        free(tmp_client->username);
        free(tmp_client);
    }
    return EXIT_SUCCESS;
}

/*
 * Creates and detaches a client specific thread
 */
int server_logic(int client_sock, int client_count) {
    ClientThreadType *tmp_client = add_user(client_sock);
    if (pthread_create(&(tmp_client->thread_id), NULL, client_thread_start, (void *)tmp_client) < 0) {
        printf("could not spawn child_thread for client %d\n", client_count);
        return -1;
    }
    pthread_detach(tmp_client->thread_id);
    printf("client %d's thread spawned\n", client_count);
    return 0;
}

/**
 *  Client thread handler: Spawned pthread process
 */
void *client_thread_start(void *client_sock_desc) {
    int client_message_len, i, j, int_helper;
    char *msg_token, *msg_helper;
    CommType *comm;
    MessageType message_from_client, message_to_client;
    ClientThreadType *local_ref, *tmp_ref;
    local_ref = (ClientThreadType *) client_sock_desc;

    if (local_ref == NULL) {
        printf("socket could not be found in list\n Exiting");
        return;
    }

    /* User Check */
    message_from_client.flag = -1;
    bzero(message_from_client.buffer, Message_MAX);
    while ((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client))) < 0);
    if (client_message_len == 0) {
        printf("Unauthenticated Client Disconnected\n");
        return;
    }
    while ((message_to_client.flag = user_check(message_from_client.buffer)) != 1) {
        if (!send_msg(local_ref->sock_desc, &(message_to_client))) {
            return;
        }
        message_to_client.flag = -1;
        bzero(message_from_client.buffer, Message_MAX);
        while((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client)))<0);
        if ( client_message_len == 0) {
            printf("Unauthenticated Client Disconnected\n");
            fflush(stdout);
            return;
        }
    }
    add_username(message_from_client.buffer, local_ref);
    if(!send_msg(local_ref->sock_desc, &(message_to_client))){
        remove_user(local_ref->sock_desc);
        return;
    }
    message_to_client.flag = -1;
    bzero(message_to_client.buffer, Message_MAX);

    while ((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client))) < 0) {
    }
    if (client_message_len == 0){
        printf("Failed to recieve I.P\n");
        remove_user(local_ref->sock_desc);
        return;
    }
    local_ref->client_ip = malloc(strlen(message_from_client.buffer)*sizeof(char));
    strcpy(local_ref->client_ip, message_from_client.buffer);

    /* Client Loop: While Thread is alive loop*/
    while (local_ref!=NULL) {
      message_from_client.flag = -1;
      bzero(message_from_client.buffer, Message_MAX);
      /*File List && Search Routines*/
      if (local_ref->file_req_socket != NULL) {
          printf("IN!!\n");
          message_from_client.flag = 2;
          if (local_ref->search_query == NULL) {
              printf("File List Request\n");
              bzero(message_from_client.buffer, Message_MAX);
          } else {
              printf("File Search Request\n");
              sprintf(message_from_client.buffer, "%s", local_ref->search_query);
              free(local_ref->search_query);
          }
          if (!send_msg(local_ref->sock_desc, &(message_from_client))){
              remove_user(local_ref->sock_desc);
              return;
          }
          bzero(message_to_client.buffer, Message_MAX);
          message_to_client.flag = 3;
          sprintf(message_to_client.buffer, "%s", local_ref->username);
          if (!send_msg(local_ref->file_req_socket, &message_to_client)) {
              remove_user(local_ref->sock_desc);
              return;
          }
          while (message_from_client.flag != 0) {
              message_from_client.flag = -1;
              //bzero(message_from_client.buffer, Message_MAX);
              while ((client_message_len = safe_recv(local_ref->sock_desc, &(message_from_client))) < 0);
              if (client_message_len == 0) {
                  printf("Failed to recieve\n");
                  remove_user(local_ref->sock_desc);
                  return;
              }
              //printf("FORWARDING TO SOCK:\t%d\tSIZE:%d\nData:\n\tFlag:\t%d\tBuffer:\t%s\n",*(local_ref->file_req_socket), client_message_len, message_from_client.flag, message_from_client.buffer);
              if ((client_message_len = send_msg(local_ref->file_req_socket, &message_from_client)) <= 0) {
                  remove_user(local_ref->sock_desc);
                  printf("Failed to recieve\n");
                  return;
              }
          }
          printf("List Sent\n");
          local_ref->file_req_socket = NULL;
      }
      if ((client_message_len = recv_msg(local_ref->sock_desc, &(message_from_client))) > 0) {
        printf("message from client: %s\t%d\t%s\n", local_ref->username, message_from_client.flag, message_from_client.buffer);
        switch (message_from_client.flag) {
          case 0:
            message_to_client.flag = 0;
            sprintf(message_to_client.buffer, "%s", message_from_client.buffer);
            broadcast_msg(&(message_to_client), local_ref->sock_desc);
            break;

          case 1:
            if(!send_msg(local_ref->sock_desc, &(message_to_client)))
              return;
            local_ref->alive = false;
            sprintf(message_to_client.buffer, "%s disconnected", local_ref->username);
            remove_user(local_ref->sock_desc);
            broadcast_msg(&(message_to_client), local_ref->sock_desc);
            printf("%s\n",message_to_client.buffer);
            message_to_client.flag = -1;
            bzero(message_to_client.buffer, Message_MAX);
            return;

          case 2:
            pthread_mutex_lock(&user_lock);
            tmp_ref = client_list;

            message_to_client.flag = 0;
            bzero(message_to_client.buffer, Message_MAX);
            sprintf(message_to_client.buffer, "|----------- Online Users -----------|");
            if (!send_msg(local_ref->sock_desc, &message_to_client)){
                remove_user(local_ref->sock_desc);
                return;
            }
            bzero(message_to_client.buffer, Message_MAX);
            while (tmp_ref != NULL) {
                if (tmp_ref->username != NULL) {
                    if (strcasecmp(local_ref->username, tmp_ref->username) != 0) {
                        sprintf(message_to_client.buffer, "%s", tmp_ref->username);
                        if (!send_msg(local_ref->sock_desc, &message_to_client)){
                            remove_user(local_ref->sock_desc);
                            return;
                        }
                        bzero(message_to_client.buffer, Message_MAX);
                    }
                }
                tmp_ref = tmp_ref->next;
            }
            pthread_mutex_unlock(&user_lock);
            sprintf(message_to_client.buffer, "|------------------------------------|");
            if (!send_msg(local_ref->sock_desc, &message_to_client)){
                remove_user(local_ref->sock_desc);
                return;
            }
            bzero(message_to_client.buffer, Message_MAX);
            break;

          case 3:
            tmp_ref = user_to_ref(message_from_client.buffer);
            if(tmp_ref != NULL) {
                if (*(tmp_ref->sock_desc) != *(local_ref->sock_desc)){
                    while (tmp_ref->file_req_socket != NULL);
                    tmp_ref->file_req_socket = local_ref->sock_desc;
                }
            }
            break;

          case 4:
            pthread_mutex_lock(&user_lock);
            tmp_ref = client_list;
            while (tmp_ref != NULL) {
                if (tmp_ref->username != NULL && (*(tmp_ref->sock_desc) != *(local_ref->sock_desc))) {
                    while (tmp_ref->file_req_socket != NULL);
                    tmp_ref->file_req_socket = local_ref->sock_desc;
                    tmp_ref->search_query = strdup(message_from_client.buffer);
                }
                tmp_ref = tmp_ref->next;
            }
            pthread_mutex_unlock(&user_lock);
            break;

          case 5:

            msg_token = strtok(message_from_client.buffer, " ");
            if (msg_token != NULL) {
                tmp_ref = user_to_ref(msg_token);
                //memmove(message_from_client.buffer, message_from_client.buffer+strlen(msg_token), strlen(message_from_client.buffer)-strlen(msg_token)+1);
                if (tmp_ref != NULL) {
                    msg_token = strtok(NULL, " ");
                    if (msg_token != NULL) {
                        /* TODO:
                         *  1. Create Comm
                         *  2. Send message to uploader FORMAT(Open port number, Key, ip)
                         */
                        comm = create_comm(local_ref, tmp_ref);
                        printf("Comm created on port (%d)\n", comm->port);
                        message_to_client.flag = 5;
                        sprintf(message_to_client.buffer, "%d %s %s", comm->port, msg_token, tmp_ref->client_ip);
                        if (!send_msg(tmp_ref->sock_desc, &message_to_client)) {
                            remove_user(tmp_ref->sock_desc);
                            return;
                        }
                    }
                } else printf("TOKEN NULL\n");
            }
            break;

          case 6:
            //DENY UPLOAD
            if (local_ref->transfers != NULL) {
                /* TODO:
                 * 1. Find Comm by port sent in packet buffer
                   2. Send message to downloader [UPLOAD DENIED/INVALID]
                   3. Remove Comm
                   */
                message_to_client.flag = 0;
                bzero(message_to_client.buffer, Message_MAX);
                if (!send_msg(local_ref->transfers[i]->downloader_ref->sock_desc, &message_to_client)) {
                    remove_comm(message_to_client.buffer);
                    remove_user(local_ref->transfers[i]->downloader_ref->sock_desc);
                    return;
                } else {
                    remove_comm(message_to_client.buffer);
                }
                bzero(message_to_client.buffer, Message_MAX);
            }
            break;

          case 7:
            //INIT_UPLOAD
            /* 1. Find Comm
               2. Send message to downloader FORMAT(Key, ip, Open port number)
            */
           // printf("Mesg to %d\n", *(local_ref->transfers[int_helper]->downloader_ref->sock_desc));
            message_to_client.flag = 4;
            bzero(message_to_client.buffer, Message_MAX);
            strcpy(message_to_client.buffer, message_from_client.buffer);
            strcat(message_to_client.buffer, " ");
            strcat(message_to_client.buffer, local_ref->client_ip);

            msg_helper = message_from_client.buffer;
            msg_token = strtok(msg_helper, " ");
            int_helper = atoi(msg_token);
            if (!send_msg(local_ref->transfers[int_helper]->downloader_ref->sock_desc, &message_to_client)) {
                local_ref->transfers[int_helper]->downloader_ref->sock_desc == NULL;
            } else printf("Success!\n");
            break;

          case 8:
            //pause/play download
            msg_token = message_from_client.buffer;
            int_helper = atoi(msg_token);
            if (local_ref->transfers[i]->downloader_ref->sock_desc != NULL) {
                if (!send_msg(local_ref->transfers[int_helper]->downloader_ref->sock_desc, &message_to_client)) {
                    local_ref->transfers[int_helper]->downloader_ref->sock_desc == NULL;
                }
            }
            if (local_ref->transfers[i]->uploader_ref->sock_desc != NULL) {
                if (!send_msg(local_ref->transfers[int_helper]->uploader_ref->sock_desc, &message_to_client)) {
                    local_ref->transfers[int_helper]->uploader_ref->sock_desc == NULL;
                }
            }
            break;
          case 9:
            //DOWNLOAD TERMINATION
            /*REMOVE CommType on Completion from Uploader and Downloader*/
            break;

          default:
            break;
        }
        message_to_client.flag = -1;
        bzero(message_to_client.buffer, Message_MAX);
      } else if (client_message_len == 0) {
        local_ref->alive=false;
        printf("Client Disconnected\n");
        fflush(stdout);
        remove_user(local_ref->sock_desc);
        return;
      } else if (client_message_len == -1) {
          if (errno != EWOULDBLOCK && errno != EAGAIN) {
              local_ref->alive = false;
              printf("Client Disconnected\n%s\n", strerror(errno));
              fflush(stdout);
              remove_user(local_ref->sock_desc);
              return;
          } else errno = 0;
      }
    }
    if (client_message_len == 0){
        local_ref->alive=false;
        printf("Client Disconnected\n");
        fflush(stdout);
        remove_user(local_ref->sock_desc);
        return;
    } else if (client_message_len == -1) {
        printf("recv failed\n");
        remove_user(local_ref->sock_desc);
        return;
    }
}

CommType *create_comm (ClientThreadType *downloader, ClientThreadType *uploader) {
    int free_port, down_loc, up_loc, i;
    CommType *ref_comm;
    if (uploader->transfers == NULL) {
        uploader->transfers = (CommType **) malloc(sizeof(CommType *) * Transfer_MAX);
    }
    if (downloader->transfers == NULL) {
        downloader->transfers = (CommType **) malloc(sizeof(CommType *) * Transfer_MAX);
    }
    if(downloader->transfers==NULL || uploader->transfers == NULL) {
        return NULL;
    }

    /* Free port determination*/
    free_port = -1;
    for (i = 0; i < 5 && free_port == -1; i++){
        if( downloader->transfers[i] == NULL && uploader->transfers[i] == NULL){
            free_port = i;
        }
    }
    if (free_port == -1)
        return NULL;

    /* Comm Initialisation */
    ref_comm = (CommType *) malloc(sizeof(CommType));
    ref_comm->downloader_ref = downloader;
    ref_comm->uploader_ref = uploader;
    ref_comm->port = free_port;

    /*Save to Client threads*/
    downloader->transfers[free_port] = ref_comm;
    uploader->transfers[free_port] = ref_comm;
    if (uploader->transfers[free_port] == NULL)
        printf("NULL UPLOADER\n");
    if (downloader->transfers[free_port] == NULL)
        printf("NULL DOWNLOADER\n");

    return ref_comm;
}

int remove_comm (ClientThreadType *comm_client, int loc) {
    int i;
    CommType *ref_comm;
    ref_comm = comm_client->transfers[i];
    if (ref_comm == NULL) return -1;
    i = ref_comm->port;
    ref_comm->uploader_ref->transfers[i] == NULL;
    ref_comm->downloader_ref->transfers[i] == NULL;
    free(ref_comm);
}

/**
 *  Retrieves a list of users, taking a malloced char array as input and a place holder for the last user removed
 */
void user_retrieve(char *ret_str, int placeholder) {
    int i =0;
    ClientThreadType *local_ref;
    local_ref = client_list;
    while (local_ref != NULL) {
        if (local_ref->username != NULL) {
            if (i >= placeholder) {
                strcpy(ret_str, local_ref->username);
                return;
            } else i++;
        }
        local_ref = local_ref->next;
    }
}


/**
 *  Non-Blocking read from the client
 */
int safe_recv(int *socket_desc, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&recv_lock);
    ret_val = recv(*(socket_desc), message, sizeof(MessageType), MSG_WAITALL);
    pthread_mutex_unlock(&recv_lock);
    return ret_val;
}

/**
 *  Non-Blocking read from the client
 */
int recv_msg(int *socket_desc, MessageType *message){
    int ret_val;
    pthread_mutex_lock(&recv_lock);
    ret_val = recv(*(socket_desc), message, sizeof(MessageType), MSG_DONTWAIT);
    pthread_mutex_unlock(&recv_lock);
    return ret_val;
}

/**
 *  Blocking write to the client
 */
int send_msg (int *socket_desc, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&write_lock);
    ret_val = write(*(socket_desc), (void *)message, sizeof(MessageType));
    pthread_mutex_unlock(&write_lock);
    return ret_val;
}

/**
 *
 */
int send_to_user (char *username, MessageType *message) {
    ClientThreadType *local_ref;
    pthread_mutex_lock(&write_lock);
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while (local_ref != NULL) {
        if (local_ref->username != NULL) {
            if (strcasecmp(username, local_ref->username) == 0) {
                write(*(local_ref->sock_desc), (void *)message, sizeof(MessageType));
                pthread_mutex_unlock(&user_lock);
                pthread_mutex_unlock(&write_lock);
                return;
            }
        }
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    pthread_mutex_unlock(&write_lock);
}

/*
 * Broadcast message to users.
 */
int broadcast_msg(MessageType *message_to_broadcast, int *local_desc){
    ClientThreadType *client_looper;
    int sock_holder;
    pthread_mutex_lock(&user_lock);
    client_looper = client_list;
    while (client_looper != NULL) {
        if (*(local_desc) != *(client_looper->sock_desc)) {
            send_msg(client_looper->sock_desc, message_to_broadcast);
        }
        client_looper = client_looper->next;
    }
    pthread_mutex_unlock(&user_lock);
}

/*
 *
 */
void add_username (char *username, ClientThreadType *local_ref) {
    int i;
    pthread_mutex_lock(&user_lock);
    local_ref->username = (char *) malloc(sizeof(char)*strlen(username));
    for ( i = 0; i< strlen(username); i++){
        local_ref->username[i] =username[i];
    }
    printf("user: %s added to database\n", local_ref->username);
    pthread_mutex_unlock(&user_lock);
}

/*
 * Called on the /exit to command to delete the user from the database
 */
void remove_user(int *socket_desc) {
    ClientThreadType *tmp_sock_thread, *remove_sock_thread;
    pthread_mutex_lock(&user_lock);
    tmp_sock_thread = client_list;
    if(tmp_sock_thread == NULL ){
        pthread_mutex_unlock(&user_lock);
        return;
    }
    if(*(tmp_sock_thread->sock_desc)==*(socket_desc)){
        remove_sock_thread = client_list;
        client_list = client_list->next;
        if(remove_sock_thread->username != NULL)free(remove_sock_thread->username);
        pthread_mutex_destroy(&tmp_sock_thread->req_lock);
        free(remove_sock_thread->sock_desc);
        free(remove_sock_thread);
        pthread_mutex_unlock(&user_lock);
        return;
    } else {
        while (tmp_sock_thread->next != NULL) {
            if(*(tmp_sock_thread->next->sock_desc)==*(socket_desc)){
                remove_sock_thread = tmp_sock_thread->next;
                tmp_sock_thread->next = remove_sock_thread->next;
                if(remove_sock_thread->username != NULL) free(remove_sock_thread->username);
                pthread_mutex_destroy(&tmp_sock_thread->req_lock);
                free(remove_sock_thread->sock_desc);
                free(remove_sock_thread);
                pthread_mutex_unlock(&user_lock);
                return;
            }
            tmp_sock_thread = tmp_sock_thread->next;
        }
    }
    pthread_mutex_unlock(&user_lock);
}

/*
 * Add User to list
 */
ClientThreadType *add_user (int socket_desc) {
    ClientThreadType *tmp_sock_thread;
    int *ret_val;
    tmp_sock_thread = (ClientThreadType *) malloc(sizeof(ClientThreadType));
    tmp_sock_thread->sock_desc = (int *)malloc(sizeof(int));
    *(tmp_sock_thread->sock_desc) = socket_desc;
    tmp_sock_thread->alive = true;
    pthread_mutex_init(&tmp_sock_thread->req_lock, NULL);
    pthread_mutex_lock(&user_lock);
    if (client_list==NULL) {
        client_list = tmp_sock_thread;
    } else {
        tmp_sock_thread->next = client_list;
        client_list = tmp_sock_thread;
    }
    pthread_mutex_unlock(&user_lock);
    return tmp_sock_thread;
}


/*
 * Returns a refence to client struct if the user is in the database.
 */
ClientThreadType *user_to_ref (char *username) {
    ClientThreadType *local_ref;
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while (local_ref!= NULL) {
        if (local_ref->username != NULL) {
            if (strcasecmp(username, local_ref->username) == 0) {
                pthread_mutex_unlock(&user_lock);
                return local_ref;
            }
        }
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    return NULL;
}

/*
 * Checks if the user is in the database
 */
int user_check (char username[Message_MAX]) {
    ClientThreadType *local_ref;
    if(strlen(username)>16) return 0;
    pthread_mutex_lock(&user_lock);
    local_ref = client_list;
    while (local_ref!= NULL) {
        if (local_ref->username != NULL) {
            if (strcasecmp(username, local_ref->username) == 0) {
                pthread_mutex_unlock(&user_lock);
                return 3;
            }
        }
        local_ref = local_ref->next;
    }
    pthread_mutex_unlock(&user_lock);
    return 1;
}
