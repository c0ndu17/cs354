/**
 *  @author George Phillips, 16512561
 *  @file Client.c
 */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/socket.h>
#include<sys/types.h>
#include<arpa/inet.h>
#include<ifaddrs.h>
#include<pthread.h>
#include<dirent.h>
#include<time.h>
#include<errno.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>


/**
 *  Definitions
 */
#define true 1
#define false 0
#define Message_MAX 2048
#define Transfer_MAX 5
#define SOCKET_ERROR -1

/* Minimisation definition */
#define MIN(a,b) (((a)<(b))?(a):(b))

/*
 *  Message Type:
 *      Flag System: (0:PRINT command) (1:STOP command) (2:LIST Routine) (3:FILELIST Routine)
 *
 */
typedef struct MessageType_t {
    int flag;
    char buffer[Message_MAX];
} MessageType;

typedef struct IKeyType_t {
    struct sockaddr_in client;
    char *Key;
    int is_paused;
    int *sock_desc;
} IKeyType;

typedef struct EntType_t{
    char *Key;
    char *dir_name;
    struct EntType_t *next;
} EntType;


/**
 * Function Declarations
 */
int write_msg(int sockfd, MessageType *message);
int read_msg(int sockfd, MessageType *message);
void safe_get(char *response);
void *server_send();
void *server_recv();
int send_list () ;
int lev_dist(char *a, char *b);
int send_search(char *search_string) ;
int message_handler(MessageType *mesg) ;
void switcher(char *ref_key);
//void close_windows();
int open_shared_dir(char *shared_location);
char *key_check(char *ref_key);
int key_gen();
void print_dirs();
void *init_upload(void *void_port) ;
void *init_download(void *void_port) ;
void initialize_ssl();
void destroy_ssl();
void shutdown_ssl();

/**
 * Global Variables
 */
int alive[] = {false, false, false};
int tcp_socket_comm;
int my_socket_comm;
struct sockaddr_in udp_server;
int *caller_sockfd;
DIR *shared_dir;
char *shared_str;
time_t time_seed;
EntType *file_key_pair;
IKeyType *transfer;
in_addr_t local_ip;
pthread_t thread[4];
pthread_mutex_t write_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t read_lock = PTHREAD_MUTEX_INITIALIZER;;
pthread_mutex_t thread_lock = PTHREAD_MUTEX_INITIALIZER;;
pthread_mutex_t server_send_lock = PTHREAD_MUTEX_INITIALIZER;;

int main(int argc, char *argv[])
{
    int alive_count, i, client_message_len;
    struct sockaddr_in server;
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;
    MessageType message_to_server, message_from_server;


    if (argc <2) {
        printf("<usage>: ./Client <ip_of_server> <optional: shared directory path>");
        return;
    }

    /* Opens the shared Directory*/
    if (argc == 3) open_shared_dir(argv[2]);
    else open_shared_dir(NULL);
    if(shared_dir == NULL){
        printf("Could not open Directory\n");
    }
    srand((unsigned) time(&time_seed));

    transfer = (IKeyType *) malloc(sizeof(IKeyType)*Transfer_MAX);
    /*File Key Generator*/
    key_gen();
    print_dirs();
    if(transfer==NULL){
        printf("Could not allocate memory for transfer data\n");
        return;
    }
    for (i = 0; i < Transfer_MAX; i++) {
        transfer[i].is_paused = -2;
    }

    if ((tcp_socket_comm = socket(AF_INET, SOCK_STREAM, 0))<0) {
        //close_windows();
        printf("failed to open a socket\n");
        return EXIT_FAILURE;
    }
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_family = AF_INET;
    server.sin_port = htons(3000);

    if (connect(tcp_socket_comm, (struct sockaddr *)&server, sizeof(server))<0) {
        //    close_windows();
        printf("Failed to connect to socket\n");
        return EXIT_FAILURE;
    }
    while (message_from_server.flag != 1) {
        message_to_server.flag = -1;
        bzero(message_to_server.buffer, Message_MAX);
        printf("Enter Username: ");
        safe_get(message_to_server.buffer);
        if (strlen(message_to_server.buffer) < 17) {
            if ((client_message_len = write_msg(tcp_socket_comm, &(message_to_server))) < 0) {
                printf("Send failed.\n");
                break;
            }
            if (client_message_len == 0) {
                printf("Disconnected from Server\n");
                return;
            }
            if ((client_message_len = read_msg(tcp_socket_comm, &(message_from_server))) < 0) {
                printf("Failed to receive from server.\n <---");
                break;
            }
            if (client_message_len == 0) {
                printf("Disconnected from Server\n");
                return;
            }
            printf("flag: %d\n", message_from_server.flag);
            switch (message_from_server.flag) {
                case 0:
                    printf("The maximum username allowed is 16 characters\n");
                    break;
                case 1:
                    printf("Logged In!\n");
                    break;
                case 2:
                    printf("The username did not consist of only alphanumeric characters\n");
                    break;
                case 3:
                    printf("The username was taken\n");
                    break;
            }
        }
    }

    /*Determine IP*/

    bzero(message_to_server.buffer, sizeof(message_to_server));

    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
        if (ifa->ifa_addr->sa_family==AF_INET) {
            if(strcasecmp(ifa->ifa_name, "eth0") == 0) {
                sa = (struct sockaddr_in *) ifa->ifa_addr;
                sprintf(message_to_server.buffer, "%s", inet_ntoa(sa->sin_addr));
                local_ip = inet_addr(message_to_server.buffer);
                printf("Interface: %s\tAddress: %s\n", ifa->ifa_name, inet_ntoa(sa->sin_addr));
                break;
            }
        }
    }

    if ((client_message_len = write_msg(tcp_socket_comm, &(message_to_server))) < 0) {
        printf("Send failed.\n");
        return;
    }
    freeifaddrs(ifap);

    alive[0] = true;
    if (pthread_create(&(thread[0]), NULL, server_send, NULL) < 0) {
        //close_windows();
        printf("could not spawn child_thread(send) for client\n");
        alive[0]=false;
        return -1;
    }
    alive[1]=true;
    if (pthread_create(&(thread[1]), NULL, server_recv, NULL) < 0) {
        alive[1] = false;
        alive[0] = false;
        //close_windows();
        printf("could not spawn child_thread(recv) for client\n");
        return -1;
    }
    do {
        alive_count = 0;
        for(i =0; i<2; i++){
            alive_count += alive[i];
        }
    } while(alive_count != 0);
    close(tcp_socket_comm);
    //close_windows();
    closedir (shared_dir);
    printf("Goodbye!\n");
    return EXIT_SUCCESS;
}


/**
 * Sends an message to a client
 */
int write_msg (int sockfd, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&write_lock);
    ret_val = send(sockfd, message, sizeof(MessageType), 0);
    pthread_mutex_unlock(&write_lock);
    return ret_val;
}

int read_msg (int sockfd, MessageType *message) {
    int ret_val;
    pthread_mutex_lock(&read_lock);
    ret_val = recv(sockfd, (void *)message, sizeof(MessageType), MSG_WAITALL);/**/
    pthread_mutex_unlock(&read_lock);
    return ret_val;
}

void safe_get(char *response) {
    gets(response);
}

void *server_send () {
    MessageType message_to_server;
    int client_message_len;
    message_to_server.flag = -1;
    while (alive[0]) {
        message_to_server.flag = -1;
        bzero(message_to_server.buffer, Message_MAX);
        safe_get(message_to_server.buffer);
        if (message_to_server.buffer[0] != 0) {
            if (strncasecmp(message_to_server.buffer, "/exit", 5) == 0){
                memmove(message_to_server.buffer, message_to_server.buffer+5, strlen(message_to_server.buffer)-5+1);
                message_to_server.flag = 1;
                printf("Exiting...\n");
                alive[0]=false;
            } else if (strncasecmp(message_to_server.buffer, "/list", 5) == 0) {
                memmove(message_to_server.buffer, message_to_server.buffer+5, strlen(message_to_server.buffer)-5+1);
                message_to_server.flag = 2;
            } else if (strncasecmp(message_to_server.buffer, "/filelist ", 10) == 0) {
                printf("usage: /filelist username\n");
                memmove(message_to_server.buffer, message_to_server.buffer + 10, strlen(message_to_server.buffer) - 10 + 1);
                message_to_server.flag = 3;
            }  else if (strncasecmp(message_to_server.buffer, "/search ", 8) == 0) {
                printf("usage: /search search_string\n");
                memmove(message_to_server.buffer, message_to_server.buffer + 8, strlen(message_to_server.buffer) - 8 + 1);
                message_to_server.flag = 4;
            } else if (strncasecmp(message_to_server.buffer, "/download ", 10) == 0) {
                printf("usage: /download username file_key\n");
                memmove(message_to_server.buffer, message_to_server.buffer + 10, strlen(message_to_server.buffer) - 10 + 1);
                message_to_server.flag = 5;
            }  else if (strncasecmp(message_to_server.buffer, "/switch ", 8) == 0) {
                printf("usage: /switch file_key\n");
                memmove(message_to_server.buffer, message_to_server.buffer + 8, strlen(message_to_server.buffer) - 8 + 1);
                message_to_server.flag = 8;
                switcher(message_to_server.buffer);
            } else message_to_server.flag = 0;
            pthread_mutex_lock(&server_send_lock);
            if ((client_message_len = write_msg(tcp_socket_comm, &(message_to_server))) < 0 ) {
                alive[0]=false;
            }
            pthread_mutex_unlock(&server_send_lock);
            if (client_message_len == 0) {
                printf("Disconnected from Server\n");
                break;
            }
        }
    }
    alive[1] = false;
}

void switcher(char *ref_key) {
    int found, i;
    for (i = 0; i < Transfer_MAX; i++){
        if (strcmp(transfer[i].Key, ref_key) == 0) {
            bzero(ref_key, Message_MAX);
            sprintf(ref_key, "%d", i);
            switch(transfer[i].is_paused){
            case 0:
                transfer[i].is_paused = 1;
                break;
            case 1:
                transfer[i].is_paused = 0;
                break;
            }
            return;
        }
    }
}


void *server_recv () {
    MessageType message_from_server, message_to_server;
    int i, client_message_len;
    i = 1;
    while (alive[1]) {
        bzero((void *)&message_from_server, sizeof(MessageType));
        if ((client_message_len = read_msg(tcp_socket_comm, &(message_from_server))) < 0) {
            alive[1] = false;
            printf("Disconnected from Server\n");
            break;
        }
        if (client_message_len == 0) {
            printf("Disconnected from Server\n");
            alive[1] = false;
            break;
        } else client_message_len = message_handler(&message_from_server);
        if (client_message_len < 1) {
            alive[1] = false;
        }
    }
    alive[0] = false;
}

int send_list () {
    EntType *local_dir_ent;
    MessageType mesg;
    int client_message_len;

    local_dir_ent = file_key_pair;
    mesg.flag = 3;
    bzero(mesg.buffer, Message_MAX);
    while (local_dir_ent != NULL) {
        sprintf(mesg.buffer, "%s", local_dir_ent->Key);
        client_message_len =  write_msg(tcp_socket_comm, &mesg);
        if(client_message_len < 1)
            return -1;
        bzero(mesg.buffer, Message_MAX);
        sprintf(mesg.buffer, "%s", local_dir_ent->dir_name);
        client_message_len =  write_msg(tcp_socket_comm, &mesg);
        if(client_message_len < 1)
            return -1;
        printf("Flag:\t%d\tLocal Dir:\t%s\t%s\n", mesg.flag, local_dir_ent->Key, mesg.buffer);
        local_dir_ent = local_dir_ent->next;
        bzero(mesg.buffer, Message_MAX);
    }
    mesg.flag = 0;
    client_message_len = write_msg(tcp_socket_comm, &mesg);
}

int lev_dist(char *a, char *b) {
    int i, j, a_len, b_len, nw, cj;
    char *a_lowered, *b_lowered;
    int *costs;
    i = 0;
    a_len = strlen(a);
    b_len = strlen(b);
    a_lowered = (char *)malloc(sizeof(char)*a_len);
    b_lowered = (char *)malloc(sizeof(char)*b_len);
    costs = (int *)malloc(sizeof(int)*(b_len+1));
    for (i =0; i < a_len; i++){
        a_lowered[i] = tolower(a[i]);
    }
    for (i =0; i < b_len; i++){
        b_lowered[i] = tolower(b[i]);
    }
    for (i = 0; i < b_len + 1; i++){
        costs[i] = i;
    }
    for (i = 1; i <= a_len; i++) {
        costs[0] = i;
        nw = i - 1;
        for (j = 1; j <= b_len; j++) {
            cj = MIN(1 + MIN(costs[j], costs[j - 1]), a_lowered[i - 1] == b_lowered[j - 1] ? nw : nw + 1);
            nw = costs[j];
            costs[j] = cj;
        }
    }
    return costs[b_len];
}

int send_search(char *search_string) {
    EntType *local_dir_ent;
    MessageType mesg;
    int client_message_len, diff, dist;
    double max, min, threshold;

    /*Threshold set 40% similarity*/
    local_dir_ent = file_key_pair;
    mesg.flag = 3;
    bzero(mesg.buffer, Message_MAX);
    while (local_dir_ent != NULL) {
        diff = strlen(search_string)-strlen(local_dir_ent->dir_name);
        if (diff > 0) {
            max = strlen(search_string);
            min = strlen(local_dir_ent->dir_name);
        } else{
            max = strlen(local_dir_ent->dir_name);
            min = strlen(search_string);
        }
        if (diff < 0) diff = diff * -1;
        dist = (lev_dist(search_string, local_dir_ent->dir_name)-diff);
        if (strlen(search_string) <= 2)
            threshold = 0.0;
        else threshold = 0.4;
        if (((dist/min) <= threshold)) {
            sprintf(mesg.buffer, "%s", local_dir_ent->Key);
            client_message_len =  write_msg(tcp_socket_comm, &mesg);
            if(client_message_len < 1)
                return -1;
            bzero(mesg.buffer, Message_MAX);
            sprintf(mesg.buffer, "%s", local_dir_ent->dir_name);
            client_message_len =  write_msg(tcp_socket_comm, &mesg);
            if(client_message_len < 1)
                return -1;
        }
        local_dir_ent = local_dir_ent->next;
        bzero(mesg.buffer, Message_MAX);
        printf("\n");
    }
    mesg.flag = 0;
    client_message_len = write_msg(tcp_socket_comm, &mesg);
}

int message_handler(MessageType *mesg) {
    char *msg_token;
    int client_message_len, count, *port_buff, i;
    pthread_t upload_thread, download_thread;
    if (mesg->flag == 0) { /* Print message */
        printf("%s\n", mesg->buffer);
    } else if (mesg->flag == 1) { /* Exit Routine */
        alive[1] = false;
    } else if (mesg->flag == 2) { /* Send File List Routine*/
        pthread_mutex_lock(&server_send_lock);
        if (mesg->buffer[0] != 0){
            client_message_len = send_search(mesg->buffer);
        } else {
            client_message_len = send_list();
        }
        if(client_message_len <0)
            return -1;
        pthread_mutex_unlock(&server_send_lock);
    } else if (mesg->flag == 3) { /* Recv File List Routine*/
        count = 0;
        printf("Files on [%s]\n", mesg->buffer);
        printf("Key:\t\tFile Name:\n");
        while (mesg->flag != 0) {
            bzero(mesg->buffer, Message_MAX);
            if ((client_message_len = read_msg(tcp_socket_comm, mesg)) <= 0) {
                printf("-1!\n");
                return -1;
            }
            if (count%2 == 0) {
                printf("%s\t", mesg->buffer);
            } else {
                printf("%s\n", mesg->buffer);
            }
            count++;
        }
        printf("\n");
    }  else if (mesg->flag == 4) { /* DOWNLOAD Routine */
        printf("----- DOWNLOAD ----- \n");
        printf("FLAG 6:\t%s\n", mesg->buffer);
        msg_token = strtok(mesg->buffer, " ");
        if ( msg_token != NULL ) {
            port_buff =  (int *) malloc(sizeof(int));
            *(port_buff) = atoi(msg_token);
            msg_token = strtok(NULL," ");
            if ( msg_token != NULL ) {
                transfer[*(port_buff)].Key = strdup(msg_token);
                msg_token = strtok(NULL, " ");
                if (msg_token != NULL) {
                    transfer[*(port_buff)].client.sin_port = htons((3001+*(port_buff)));
                    transfer[*(port_buff)].client.sin_addr.s_addr = inet_addr(msg_token);
                    transfer[*(port_buff)].client.sin_family = AF_INET;
                    transfer[*(port_buff)].is_paused = -1;
                    printf("Port Buff: %d\n", *(port_buff));
                    if (pthread_create(&(download_thread), NULL, init_download, (void *)port_buff) < 0) {
                        printf("could not spawn child_thread(send) for client\n");
                    }
                    pthread_detach(download_thread);
                } else {
                    free(transfer[*(port_buff)].Key);
                    free(port_buff);
                }
            } else {
                free(port_buff);
            }
        }
        printf("-------------------- \n");
    } else if (mesg->flag == 5) { /* UPLOAD Routine */
        printf("----- Upload Request ----- \n");
        msg_token = strtok(mesg->buffer, " ");
        if (msg_token != NULL) {
            port_buff =  (int *) malloc(sizeof(int));
            *(port_buff) = atoi(msg_token);
            msg_token = strtok(NULL," ");
            if ( msg_token != NULL ) {
                transfer[*(port_buff)].Key = strdup(msg_token);
                msg_token = strtok(NULL, " ");
                if (msg_token != NULL) {
                    transfer[*(port_buff)].client.sin_port = htons((3001+*(port_buff)));
                    transfer[*(port_buff)].client.sin_addr.s_addr = htonl(INADDR_ANY);
                    transfer[*(port_buff)].client.sin_family = AF_INET;
                    transfer[*(port_buff)].is_paused = -1;
                    if ((msg_token = key_check(transfer[*(port_buff)].Key)) != NULL) {
                        mesg->flag = 7;
                        bzero(mesg->buffer, Message_MAX);
                        sprintf(mesg->buffer, "%d %s", *(port_buff), transfer[*(port_buff)].Key);
                        printf("Port Buff: %d\n", *(port_buff));
                        if (!write_msg(tcp_socket_comm, mesg)) {
                            printf("failed to send\n");
                            return;
                        }
                        if (pthread_create(&(upload_thread), NULL, init_upload, (void *)port_buff) < 0) {
                            printf("could not spawn child_thread(send) for client\n");
                        }
                        pthread_detach(upload_thread);
                    } else {
                        mesg->flag = 6;
                        bzero(mesg->buffer, Message_MAX);
                        sprintf(mesg->buffer, "INVALID KEY", NULL);
                        if(!write_msg(tcp_socket_comm, mesg)){
                            printf("failed to send");
                            return 1;
                        }
                    }
                }
            }
        }
    } else if (mesg->flag == 8) {
        msg_token = mesg->buffer;
        switch (transfer[*(port_buff)].is_paused){
            case 0:
                transfer[*(port_buff)].is_paused=1;
                break;
            case 1:
                transfer[*(port_buff)].is_paused=0;
                break;
        }
    }
    return 1;
}

/**
 *  Opens the to directory to be shared.
 */
int open_shared_dir(char *shared_location){
    if (shared_location == NULL){
        printf("Opening ./shared\n");
        shared_dir = opendir ("shared");
        shared_str =  (char *)malloc(sizeof(char)*8);
        bzero(shared_str, sizeof(char)*8);
        sprintf(shared_str, "shared/");
    } else {
        printf("Opening %s\n", shared_location);
        shared_dir = opendir (shared_location);
        shared_str = strdup(shared_location);
    }
}

char *key_check(char *ref_key) {
    int i;
    struct dirent *curr_ent;
    EntType *build_list;
    if(file_key_pair==NULL){
        return NULL;
    }
    build_list = file_key_pair;
    while (build_list != NULL) {
        if (strcasecmp(build_list->Key, ref_key)==0){
            return build_list->dir_name;
        }
        build_list = build_list->next;
    }
    return NULL;
}

/**
 *  Generates keys for the files.
 */
int key_gen() {
    int i;
    struct dirent *curr_ent;
    EntType *build_list;
    if (shared_dir == NULL) {
        return -1;
    }
    ;
    if ((curr_ent = readdir (shared_dir)) !=NULL) {
        while ( (strcmp(curr_ent->d_name, ".")==0 )|| (strcmp(curr_ent->d_name, "..")==0)) {
            curr_ent = readdir (shared_dir);
        }
        file_key_pair = (EntType *) malloc(sizeof(EntType));
        file_key_pair->dir_name = strdup(curr_ent->d_name);
        file_key_pair->Key = (char *) malloc(sizeof(char)*8);
        for(i = 0; i < 8; i++) {
            file_key_pair->Key[i] = (char)((rand() % 75) + 48);
        }
        build_list = file_key_pair;
        while ((curr_ent = readdir (shared_dir))!=NULL) {
            if (!strcmp (curr_ent->d_name, "."))
                continue;
            if (!strcmp (curr_ent->d_name, ".."))
                continue;
            build_list->next = (EntType *) malloc(sizeof(EntType));
            build_list = build_list->next;
            build_list->dir_name = strdup(curr_ent->d_name);
            build_list->Key = (char *) malloc(sizeof(char) * 8);
            for(i = 0; i < 8; i++) {
                /* Range of the ASCII values to be used. */
                build_list->Key[i] = (char)((rand() % 75) + 48);
            }
        }
    }
    printf("Completed!\n");
    return 1;
};

void print_dirs() {
    EntType *tmp_ref;
    if (file_key_pair == NULL) {
        printf("Directory not open\n");
        return;
    }
    tmp_ref = file_key_pair;
    printf("----Files in dir----\n");
    printf("Key:\t\tFile Name:\n");
    while(tmp_ref != NULL){
        printf("%s\t%s\n", tmp_ref->Key, tmp_ref->dir_name);
        tmp_ref = tmp_ref->next;
    }
    printf("---------------------\n");
}

void *init_upload(void *void_port) {
    int sockfd, read_size, write_size, port, use_cert, use_prv, ssl_err;
    long long int file_size;
    char *msg_helper, *file_name;
    char buffer[Message_MAX];
    FILE *src_file;
    struct dirent *curr_ent;
    struct sockaddr_in myaddr, dest_addr;
    SSL_CTX *sslctx;
    SSL *cSSL;
    socklen_t destlen;
    EntType *list_check;

    printf("Init Upload\n");
    initialize_ssl();
    sslctx = SSL_CTX_new( SSLv23_server_method());
    SSL_CTX_set_options(sslctx, SSL_OP_SINGLE_DH_USE);
    use_cert = SSL_CTX_use_certificate_file(sslctx, "Certificates/cert.pem" , SSL_FILETYPE_PEM);
    use_prv = SSL_CTX_use_PrivateKey_file(sslctx, "Certificates/key.pem", SSL_FILETYPE_PEM);
    if ( !SSL_CTX_check_private_key(sslctx) )
    {
        printf("Private key does not match the public certificate\n");
        return;
    }

    if ((sockfd=socket(AF_INET, SOCK_STREAM, 0))<0) {
        printf("Failed to open socket\n");
        return;
    }
    port = *((int *)void_port);
    printf("Port in use: %d\n", port);

    bzero(&myaddr,sizeof(myaddr));
    myaddr = transfer[port].client;
    if (bind(sockfd,(struct sockaddr *)&myaddr,sizeof(myaddr))!=0){
        printf("failed to bind\n");
        return;
    }
    printf("Bound!\n");
    listen(sockfd, 10);
    destlen = sizeof(struct sockaddr_in);
    transfer[port].sock_desc = (int *) malloc(sizeof(int));
    *(transfer[port].sock_desc) = accept(sockfd, (struct sockaddr *) &dest_addr, (socklen_t *) &dest_addr);



    cSSL = SSL_new(sslctx);
    SSL_set_fd(cSSL, *(transfer[port].sock_desc));
    //Here is the SSL Accept portion.  Now all reads and writes must use SSL
    ssl_err = SSL_accept(cSSL);
    printf("Accepted %d\n", ssl_err);
    if(ssl_err <= 0)
    {
        //Error occured, log and close down ssl
        printf("Connect Failed\n");
        shutdown_ssl(cSSL);
        return;
    }
    printf("Accepted\n");

    if (file_key_pair == NULL){
        shutdown_ssl();
        return;
    }
    msg_helper = key_check(transfer[port].Key);
    rewinddir(shared_dir);
    while ((curr_ent = readdir(shared_dir))!=NULL){
        if(strcmp(curr_ent->d_name, msg_helper)==0){
            printf("Found!\n");
            break;
        }
    }
    if(curr_ent !=NULL)
        printf("%s\n", curr_ent->d_name);
    else{
        printf("Not found.\t%s\n", msg_helper);
        return;
    }
    bzero((void *)&buffer, sizeof(buffer));
    sprintf(buffer, "shared/%s", curr_ent->d_name);

    if((src_file = fopen(buffer, "r"))==NULL){
        printf("FAILED TO OPEN FILE\n");
    } else printf("Opened File!\n");

    bzero((void *)&buffer, sizeof(buffer));
    sprintf(buffer, "%s", msg_helper);
    printf("First Write:\n");
    write_size = SSL_write(cSSL, (void *)&buffer, sizeof(buffer));
    if (write_size <= 0) {
        printf("ERROR:\t%s\n", strerror(SSL_get_error(cSSL, write_size)));
        errno = 0;
        return;
    }
    printf("Sent!\n");
    bzero((void *)&buffer, sizeof(buffer));
    fseek(src_file, 0L, SEEK_END);
    file_size = htonl(ftell(src_file));
    sprintf(buffer, "%lld", file_size);
    printf("%s\n", buffer);
    fseek(src_file, 0L, SEEK_SET);
    write_size = SSL_write(cSSL, (void *)&buffer, sizeof(buffer));
    if (write_size <= 0) {
        return;
    }
    transfer[port].is_paused = 0;
    while (!feof(src_file)) {
        bzero((void *)&buffer, sizeof(buffer));
        read_size = fread(buffer, 1, sizeof(buffer), src_file);
        if (read_size <= 0)
            error("ERROR reading file");

        msg_helper = buffer;
        while (read_size > 0)
        {
            while(transfer[port].is_paused) printf("PAUSED!\n");
            write_size = SSL_write(cSSL, (void *)&buffer, sizeof(buffer));
            /*write_size = write(*(transfer[port].sock_desc), (void *)&buffer, sizeof(buffer));*/
            if (write_size == SOCKET_ERROR)
            {
                if ( errno != EWOULDBLOCK && errno != EAGAIN){
                    printf("Strerror: %s\n", strerror(errno));
                    errno = 0;
                }

                fd_set writefd;
                FD_ZERO(&writefd);
                FD_SET(*(transfer[port].sock_desc), &writefd);
                if (select(0, NULL, &writefd, NULL, NULL) != 1)
                    error("ERROR waiting to write to socket");
                continue;
            }
            if (write_size == 0)
                printf("DISCONNECTED\n");

            msg_helper += write_size;
            read_size -= write_size;
        }
    }
    printf("DONE!\n");
    fclose(src_file);
    transfer[port].is_paused = -2;
    shutdown_ssl(cSSL);
    close(*(transfer[port].sock_desc));
    close(sockfd);
    free(void_port);
}

void *init_download(void *void_port) {
    int sockfd, ssl_sockfd, write_size, msg_size, port, use_cert, use_prv, ssl_err;
    long long int file_size;
    struct sockaddr_in myaddr, cli_addr;
    FILE *dest_file;
    char buffer[Message_MAX];
    char *msg_helper, *file_loc;
    socklen_t srclen;
    SSL_CTX *sslctx;
    SSL *cSSL;

    printf("Init download\n");

    initialize_ssl();
    sslctx = SSL_CTX_new( SSLv23_client_method());
    if(sslctx == NULL){
        printf("Failed to create SSL Context.\n");
        return;
    }

    transfer[port].sock_desc = (int *) malloc(sizeof(int));
    if ((*(transfer[port].sock_desc)=socket(AF_INET, SOCK_STREAM, 0))<0) {
        printf("Failed to open socket\n");
        return;
    }

    port = *((int *) void_port);
    printf("Port in use: %d\n", port);
    if (transfer[port].client.sin_port != htons(3001+port)) {
        return;
    }

    if ((connect(*(transfer[port].sock_desc), (struct sockaddr *)&(transfer[port].client), sizeof(transfer[port].client)))<0){
        printf("failed to connect\n%s\n", strerror(errno));
        errno = 0;
        return;
    }



    cSSL = SSL_new(sslctx);
    SSL_set_fd(cSSL, *(transfer[port].sock_desc));
    //Here is the SSL Accept portion.  Now all reads and writes must use SSL
    ssl_err = SSL_connect(cSSL);
    if(ssl_err <0)
        ERR_print_errors_fp(stderr);
    if(ssl_err <= 0)
    {
        //Error occured, log and close down ssl
        printf("Connect Failed\n");
        shutdown_ssl(cSSL);
        return;
    }
    printf("Connected.\n");

    /*
    SSL_read(cSSL, (char *)charBuffer, nBytesToRead);
    SSL_write(ssl, "Hi :3\n", 6);
    */
    printf("FIRST READ:\n");
    bzero((void *) &buffer, sizeof(buffer));
    msg_size = SSL_read(cSSL, &buffer, sizeof(buffer));
    // = read(sockfd, (void *)&buffer, sizeof(buffer));
    printf("File:\t%s\n size of shared str: %d\n", buffer,(int)strlen(shared_str));
    file_loc = (char *) malloc(sizeof(char)*(strlen(buffer)+strlen(shared_str)));
    //bzero((void *) &file_loc, sizeof(char)*(strlen(buffer)+strlen(shared_str)));
    sprintf(file_loc, "%s%s", shared_str, buffer);

    printf("File Loc:\t%s\n", file_loc);
    if((dest_file = fopen(file_loc, "w")) == NULL){
        printf("FAILED TO OPEN FILE\n");
    }
    bzero((void *) &buffer, sizeof(buffer));
    msg_size = SSL_read(cSSL, (char *)&buffer, sizeof(buffer));
    file_size = atol(buffer);
    file_size = ntohl(file_size);
    printf("File size: %lld\n", file_size);
    transfer[port].is_paused = 0;
    while (file_size > 0) {
        bzero((void *) &buffer, sizeof(buffer));
        while(transfer[port].is_paused) printf("PAUSED!\n");
        SSL_read(cSSL, (char *)&buffer, sizeof(buffer));
       // msg_size = recv(sockfd, (void *) &buffer, sizeof(buffer), 0);
        if (msg_size == SOCKET_ERROR) {
            if (errno != EWOULDBLOCK) {
                printf("Strerror: %s\n", strerror(errno));
                errno = 0;
            }
            fd_set readfd;
            FD_ZERO(&readfd);
            FD_SET(*(transfer[port].sock_desc), &readfd);
            if (select(0, &readfd, NULL, NULL, NULL) != 1){
                printf("ERROR waiting for data from socket\n%s\n", strerror(errno));
            }
            continue;
        }
        if (msg_size == 0) {
            printf("Disconnected\n");
            fclose(dest_file);
            shutdown_ssl(cSSL);
            break;
        }
        msg_helper = buffer;
        while (msg_size > 0 && file_size > 0)
        {
            if (msg_size > file_size)
                 write_size = fwrite(msg_helper, 1, file_size, dest_file);
            else write_size = fwrite(msg_helper, 1, msg_size, dest_file);
            if (write_size <= 0)
                printf("ERROR writing to file\n%s\n", strerror(errno));
            msg_helper += write_size;
            msg_size -= write_size;
            file_size-= write_size;
        }
    }
    printf("Exiting\n");
    fclose(dest_file);
    free(void_port);
    shutdown_ssl(cSSL);
}
/***
 *  SSL
 */

void initialize_ssl(){
    SSL_load_error_strings();
    SSL_library_init();
    OpenSSL_add_all_algorithms();
}
void destroy_ssl() {
    ERR_free_strings();
    EVP_cleanup();
}
void shutdown_ssl(SSL *ssl_comm){
    SSL_shutdown(ssl_comm);
    SSL_free(ssl_comm);
}

